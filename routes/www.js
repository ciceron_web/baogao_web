var express = require('express');
var router = express.Router();
var proxy = require('express-http-proxy');
var request = require('request');
var fs = require('fs');
var urlencode = require('urlencode');
var mysql = require('mysql');
var multer = require('multer');
var url = require('url');


var nodemailer = require('nodemailer');


var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, __dirname + '/../public/uploadedFiles/')
	},
	filename: function (req, file, cb) {
		cb(null, + Date.now() + "." + file.originalname.split(".").pop())
	}
});




var uploading = multer({
	fileFilter: function (req, file, cb) {
		var UnacceptableMimeTypes = ["exe", "com", "scr", "sh", "ln"]
		if (UnacceptableMimeTypes.indexOf(file.originalname.split(".").pop()) > -1) {
			return cb(new Error('Unacceptable file type.'))
		}

		cb(null, true)
	},
	storage: storage,
	limits: { fileSize: 1000 * 1000 * 20, files: 5 }
})





var mysql_query = function (query, callback) {



	var connection = mysql.createConnection({
		host: global.certification["mysql-host"],
		user: global.certification["mysql-user"],
		password: global.certification["mysql-password"],
		port: global.certification["mysql-port"],
		database: global.certification["mysql-database"]
	});

	connection.connect();
	connection.query(query, callback);
	// {
	//   if (!err)
	//     console.log('The solution is: ', rows);
	//   else
	//     console.log('Error while performing Query.', err);
	// }
	connection.end();

}


var sendMail = function (to, subject, html) {
	var transporter = nodemailer.createTransport({
		service: 'Gmail',
		auth: {
			user: global.certification["gmail-user"],
			pass: global.certification["gmail-password"]
		}
	});

	var mailOptions = {
		from: 'baogao <no-reply@ciceron.me>',
		to: to,
		subject: subject,
		html: html
	};

	transporter.sendMail(mailOptions, function (error, response) {

		if (error) {
			console.log(error);
		} else {
			// console.log("Message sent : " + response.message);
			console.log(response);
		}
		transporter.close();
	});

}

// topik 채점기
/*
router.get('/topik_score', function (req, res, next) {
	viewFile(req, res, "topik_main1");
});
*/

router.get('/topik_tool', function(req, res, next){
	viewFile(req,res, "topik_main");
});

// korean 교열기
router.get('/tools/spell', global.loginRequired, function(req, res, next){
	viewFile(req,res, "kr_mark");
});

router.get('/request', function (req, res, next) {
	mysql_query("select * from school", function (err, rows, fields) {
		if (!err) {
			viewFile(req, res, 'form2', { school: rows });
		}
		else {
			console.log(err);
			next();
		}
	});
});

router.get('/img/school_background/*', function (req, res, next) {
	res.redirect("/img/school_background/etc.jpg");
});

router.get('/black', function (req, res, next) {
	viewFile(req, res, "black");
});
router.get('/black_app', function (req, res, next) {
	viewFile(req, res, "black2");
});
router.get('/black_job', function (req, res, next) {
	viewFile(req, res, "black1");
});

router.get('/black/frame', function (req, res, next) {
	res.redirect("https://goo.gl/forms/A3nPgVWil7MrEOC62");
});

router.get('/black_app/frame', function (req, res, next) {
	res.redirect("https://goo.gl/forms/j44ldlLSBufVeMFK2");
});

router.get('/black_job/frame', function (req, res, next) {
	res.redirect("https://goo.gl/forms/0fb7pP1Q9c0S3sij2");
});

// router.get('/black/frame', function(req, res, next){
// 	res.redirect("https://goo.gl/forms/YFj8ARa6EnanKrS83");
// });


router.get('/event', function (req, res, next) {
	viewFile(req, res, "event");
});

router.get('/admin/request', global.adminCheck, function (req, res, next) {
	// var tmpPage = req.query.page;
	// var urlWithoutPage = "";
	// delete req.query.page;
	// urlWithoutPage = req.url;
	// console.log(urlWithoutPage);
	// if(tmpPage)req.query.page = tmpPage;
	// console.log(req.query);
	console.log(req.protocol + '://' + req.get('host') + '/api/admin/getRequestList');
	// console.log(req.originalUrl.replace("&page=" + req.query.page, "").replace("page=" + req.query.page, ""));
	request({
		url: req.protocol + '://' + req.get('host') + '/api/admin/getRequestList',
		headers: req.headers,
		qs: req.query,
		rejectUnauthorized: false
	}, function (err, res2, body) {
		console.log(err);

		try {
			// console.log(JSON.parse(body));
			// res.render("www/" + filename, { title: 'CICERON', filename: filename, user: JSON.parse(body), url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });

			var u = url.parse(req.path);
			u.query = req.query;
			// console.log(u.query);
			// console.log(url.format(u));

			var pages = [];
			for (var i = 1; i <= JSON.parse(body).pageAmount; i++) {
				u.query.page = i;
				console.log(url.format(u));
				pages.push(url.format(u));
			}

			delete u.query.page;
			viewFile(req, res, 'adminRequest', { pageInfomation: pages, request: JSON.parse(body), urlWithoutPage: url.format(u) });

		} catch (error) {
		}
	});
});





/* GET home page. */
router.get('/', function (req, res, next) {
	mysql_query("select * from school", function (err, rows, fields) {
		if (!err) {
			// console.log(rows);
			viewFile(req, res, 'main', { school: rows });
		}
		else {
			console.log(err);
			next();
		}
	});
});

	router.get('/div/authStart', function (req, res, next) {
	viewFile(req, res, 'div/authStart');

});

router.get('/aboutus', function (req, res, next) {
	res.redirect('/about');
});
router.get('/about', function (req, res, next) {
	viewFile(req, res, 'aboutUs');
});

router.get('/account', global.loginRequired, function (req, res, next) {
	viewFile(req, res, 'myPage');
});

router.get('/form', function (req, res, next) {
	viewFile(req, res, 'form');
});
router.get('/checkout', function (req, res, next) {
	try {

		var request_id = mysql.escape(req.query.id);
		var request_email = mysql.escape(req.query.email);

		mysql_query("SELECT * FROM `request` WHERE `id` = " + request_id + " AND `email` = " + request_email, function (err, rows, fields) {
			if (err) {
				next(err);
				return;
			}
			if (rows.length < 1) {
				next();
				return;
			}
			var request = rows[0];
			try {
				mysql_query("SELECT * FROM `school` WHERE `id`=" + rows[0].school, function (err, rows2, fields) {
					var school = rows2[0];
					if (err) {
						next(err);
						return;
					}
					mysql_query("SELECT *, ROUND((`amount` / 1088), 2) AS `amount_dollar` FROM `payment` WHERE `request_id` = " + request_id, function (err, rows3, fields) {
						if (err) {
							next(err);
							return;
						}
						try {
							if (parseInt(request.status) >= 2) {
								res.redirect("/checkout/completed?id=" + request.id + "&email=" + request.email);
								return;
							}
							viewFile(req, res, 'checkout', { school: school, request: request, payment: rows3[0] });
						}
						catch (err) {
							next(err);
						}
					});

				});
			}
			catch (err) {
				next(err);
			}
		});
	}
	catch (err) {
		next(err);
	}
});


router.get('/checkout/card', function (req, res, next) {
	try {

		var request_id = mysql.escape(req.query.id);
		var request_email = mysql.escape(req.query.email);

		mysql_query("SELECT * FROM `request` WHERE `id` = " + request_id + " AND `email` = " + request_email, function (err, rows, fields) {
			if (err) {
				next(err);
				return;
			}
			if (rows.length < 1) {
				next();
				return;
			}
			var request1 = rows[0];
			try {
				mysql_query("SELECT * FROM `school` WHERE `id`=" + request1.school, function (err, rows2, fields) {
					var school = rows2[0];
					if (err) {
						next(err);
						return;
					}
					mysql_query("SELECT *, ROUND((`amount` / 1088), 2) AS `amount_dollar` FROM `payment` WHERE `request_id` = " + request_id, function (err, rows3, fields) {
						if (err) {
							next(err);
							return;
						}
						payment = rows3[0];
						try {
							if (payment.coupon_code.length > 0) {
								request.post({
									url: req.protocol + '://' + req.get('host') + '/api/admin/coupon/check',
									rejectUnauthorized: false,
									formData: {
										code: payment.coupon_code,
										request_id: payment.request_id
									}
								}, function (err, response, body) {
									if (!err) {
										console.log(body);
										viewFile(req, res, 'checkcard', { school: school, request: request1, payment: payment, coupon: JSON.parse(body) });
									}
								});
							}
							else {
								viewFile(req, res, 'checkcard', { school: school, request: request1, payment: payment, coupon: null });
							}
						}
						catch (err) {
							next(err);
						}
					});

				});
			}
			catch (err) {
				next(err);
			}
		});
	}
	catch (err) {
		next(err);
	}
});

router.get('/checkout/completed', function (req, res, next) {
	try {

		var request_id = mysql.escape(req.query.id);
		var request_email = mysql.escape(req.query.email);

		mysql_query("SELECT * FROM `request` WHERE `id` = " + request_id + " AND `email` = " + request_email, function (err, rows, fields) {
			if (err) {
				next(err);
				return;
			}
			if (rows.length < 1) {
				next();
				return;
			}
			var request = rows[0];
			try {
				mysql_query("SELECT * FROM `school` WHERE `id`=" + rows[0].school, function (err, rows2, fields) {
					var school = rows2[0];
					if (err) {
						next(err);
						return;
					}
					mysql_query("SELECT *, ROUND((`amount` / 1088), 2) AS `amount_dollar` FROM `payment` WHERE `request_id` = " + request_id, function (err, rows3, fields) {
						if (err) {
							next(err);
							return;
						}
						try {
							viewFile(req, res, 'checkcomplete', { school: school, request: request, payment: rows3[0] });
						}
						catch (err) {
							next(err);
						}
					});

				});
			}
			catch (err) {
				next(err);
			}
		});
	}
	catch (err) {
		next(err);
	}
});

router.get('/request/completed', function (req, res, next) {
	try {

		var request_id = mysql.escape(req.query.id);
		var request_email = mysql.escape(req.query.email);

		// mysql_query("select * from school where url=" + mysql.escape(req.params.url), function (err, rows, fields) {
		// 	if (err) {
		// 		next(err);
		// 		return;
		// 	}
		// 	try {
		// 		var school = rows[0];
		try {
			mysql_query("select * from `request` where `id` = " + request_id + " AND `email` = " + request_email, function (err, rows, fields) {
				if (err) {
					next(err);
					return;
				}

				try {
					viewFile(req, res, 'request', { request: rows[0] });
				}
				catch (err) {
					next(err);
				}
			});
		}
		catch (err) {
			next(err);
		}
		// });
	}
	catch (err) {
		next(err);
	}
});

// baogao 회원가입 양식
router.get('/signup', function (req, res, next) {
	viewFile(req, res, '/join_us');
});

// baogao 로그인
router.get('/signin', function (req, res, next) {
	viewFile(req, res, '/login');
});

// baogao 비밀번호 찾기
router.get('/find_login_info', function (req, res, next) {
	viewFile(req, res, '/find_login_info');
});

// baogao Quiz
router.get('/event/quiz', function (req, res, next) {
	viewFile(req, res, 'baogaoQ/index');
});

router.get('/event/quiz/questions', function (req, res, next) {
	viewFile(req, res, 'baogaoQ/questions');
});

router.get('/event/quiz/questions/answer', function (req, res, next) {
	viewFile(req, res, 'baogaoQ/answer');
});

router.get('/event/quiz/questions/errorsend', function (req, res, next) {
	viewFile(req, res, 'baogaoQ/errorsend');
});


// baogao 강의평가
router.get('/lecture', global.loginRequired, function (req, res, next) {
	viewFile(req, res, '/lecture_board');
});
router.post('/evaluation2', uploading.none(), function(req, res, next){
	res.redirect("/evaluation2/schools?school_id=" + req.body.school_id);
});
// router.get('/lecture', function (req, res, next) {
// 	viewFile(req, res, '/lecture_board');
// });
// router.post('/evaluation2*', uploading.none(), function(req, res, next){
// 	next();
// });
// router.post('/evaluation2', uploading.none(), function(req, res, next){
// 	res.redirect("/evaluation2/schools?school_id=" + req.body.school_id);
// });
// router.get('/lecture', function (req, res, next) {
// 	viewFile(req, res, 'lecture');
// });

router.get('/lecture/view', global.loginRequired, function (req, res, next) {
	viewFile(req, res, "lecture_view");
});

router.get('/request/:url', function (req, res, next) {
	
	if (req.baogaoUser && req.baogaoUser.logged_in) {
		res.redirect("/request");
	}
	else {
		mysql_query("select * from school where url=" + mysql.escape(req.params.url), function (err, rows, fields) {
			req.baogaoUser.school_id = rows[0].id;
			req.baogaoUser.school_name = rows[0].name;
			req.baogaoUser.school_domain = rows[0].email;
			req.baogaoUser.school_email = rows[0].email;
			req.baogaoUser.school_color = rows[0].color;
			req.baogaoUser.school_url = rows[0].url;
			viewFile(req, res, 'form2', {school: []});
		});
	}
});

// FAQ
router.get('/faq', global.loginRequired, function (req, res, next) {
	viewFile(req, res, '/faq');
});

router.get('/:url', function (req, res, next) {
	try {
		mysql_query("select * from school where url=" + mysql.escape(req.params.url), function (err, rows, fields) {
			try {
				var school = rows[0];
				mysql_query("select * from subject where school=" + mysql.escape(school.id), function (err, rows, fields) {
					try {

						if (req.url.substr(-1) != '/' && req.url.length > 1)
							res.redirect(301, req.url + "/");
						//  res.redirect(301, req.url.slice(0, -1));
						// else
						// 	next();
						viewFile(req, res, 'form2', { school: school, subject: rows });
					}
					catch (err) {
						next();
					}
				});
			}
			catch (err) {
				next();
			}
		});
	}
	catch (err) {
		next();
	}

});

router.get("/auth/:token", function (req, res, next) {


	request({
		url: req.protocol + '://' + req.get('host') + '/api/v1/user/auth/confirm?token=' + req.params.token,
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers,
		rejectUnauthorized: false
	}, function (err, res2, body) {
		// console.log(body);

		try {
			// console.log(JSON.parse(body));
			// res.send(res2);
			// console.log(body);
			res.redirect("/");

		} catch (error) {
			next(error);
		}
	});
})

router.post('/:url/upload', uploading.fields([{ name: "file", maxCount: 1 }, { name: "original", maxCount: 1 }]), function (req, res, next) {
	try {

		mysql_query("select * from school where url=" + mysql.escape(req.params.url), function (err, rows, fields) {
			if (err) {
				res.status(500).send(JSON.stringify({ code: 500, message: err }));
				return;

			}
			var school = rows[0];
			if (req.body.email.split("@").pop() != school.email) {
				res.status(401).send(JSON.stringify({ code: 401, message: "Unacceptable email address" }));
				return;
			}


			console.log(req.files);

			mysql_query("INSERT INTO `request` (`id`, `email`, `subject`, `file`, `context`, `file_language`, `school`, `due`, `original`, `time`, `original_language`, `name`) VALUES (NULL, " + mysql.escape(req.body.email) + ", " + mysql.escape(req.body.subject) + ", '/uploadedFiles/" + (req.files.file.length > 0 ? req.files.file[0].filename : "") + "', " + mysql.escape(req.body.context) + ", '1', " + mysql.escape(req.body.school) + ", CURRENT_TIMESTAMP, '/uploadedFiles/" + (typeof req.files.original != "undefined" ? req.files.original[0].filename : "") + "', CURRENT_TIMESTAMP, '4', " + mysql.escape(req.body.name) + ");", function (err, rows, fields) {
				if (err) {
					res.status(500).send(JSON.stringify({ code: 500, message: err }));
					return;
				}
				sendMail("hanseulmaro.kim@ciceron.me", "교열 요청 들어왔다", "확인해라");
				sendMail(req.body.email, "[baogao]교열 요청 완료",
					'<div style="width: 100%; height: 300px; text-align: center; position: relative; overflow: hidden; background-color: #efefef; margin-bottom: 40px;" >'
					+ '<img src="http://baogao.co/img/baogao.png" style="height: 150px; margin-top: 75px;"/>'
					+ '</div>'
					+ '<div style="width: 100%; height: auto; text-align: center; position: relative;">'
					+ '<div style="display: block; color: #adadad; font-size: 16px; font-style: italic;">알림 사항</div>'
					+ '<div style="display: block; font-size: 24px;">교열 요청이 완료되었습니다.</div>'
					+ '<div style="display: block; font-size: 14px; margin-top: 120px;">baogao 서비스를 이용해주셔서 감사합니다. 빠른 시일 내에 작업 결과를 드릴 수 있도록 노력하겠습니다.</div>'
					+ '</div>');
				res.send(JSON.stringify({ code: 200, message: "Success" }));
			});
		});
	}
	catch (err) {
		next(err);
	}

});

router.post('/user/sendBaogaoMail', uploading.none(), function (req, res, next) {
	try {
		// console.log(req);
		var formData = {
			name:req.body['name'],
			subject:req.body.subject,
			message:req.body.message,
			email: req.body.email
		};
		request.post({
			url: 'http://ciceron.xyz:5000/api/v2/user/sendBaogaoMail',
			// url: req.protocol + '://secure.c.i' + '/api/user/profile',
			//headers: req.headers,
			formData: formData,
			rejectUnauthorized: false
		}, function optionalCallback(err, httpResponse, body) {
			if (err) {
			  return console.error('upload failed:', err);
			}
			res.send(JSON.stringify({ code: 200, message: "Success" }));
			// console.log('Upload successful!  Server responded with:', body);
		  });
	}
	catch (err) {
		next(err);
	}

});

router.post('/user/receiveBaogaoMail', uploading.none(), function (req, res, next) {
	try {
		// console.log(req);
		var formData = {
			name:req.body['name'],
			subject:req.body.subject,
			message:req.body.message,
			email: req.body.email
		};
		request.post({
			url: 'http://ciceron.xyz:5000/api/v2/user/receiveBaogaoMail',
			// url: req.protocol + '://secure.c.i' + '/api/user/profile',
			//headers: req.headers,
			formData: formData,
			rejectUnauthorized: false
		}, function optionalCallback(err, httpResponse, body) {
			if (err) {
			  return console.error('upload failed:', err);
			}
			res.send(JSON.stringify({ code: 200, message: "Success" }));
			// console.log('Upload successful!  Server responded with:', body);
		  });
	}
	catch (err) {
		next(err);
	}

});

//SELECT * FROM `subject` WHERE school=(SELECT id FROM `school` WHERE url='ewha1')

router.get('/:url/search_subject', function (req, res, next) {

	try {
		mysql_query("select `name` from subject where `code` LIKE '%" + mysql.escape(req.query.term).substring(1, mysql.escape(req.query.term).length - 1) + "%' OR `name` LIKE '%" + mysql.escape(req.query.term).substring(1, mysql.escape(req.query.term).length - 1) + "%' AND school=(SELECT id FROM `school` WHERE url=" + mysql.escape(req.params.url) + ") order by `name`", function (err, rows, fields) {

			// console.log(rows)
			var result = [];
			for (var i in rows)
				result.push(rows[i].name);

			res.send(result);
			// viewFile(req, res, 'form2', {school: school, subject: rows});
		});
	}

	catch (err) {
		next(err);
	}

	// try{
	// 	mysql_query("select `name` from subject where `name` LIKE '%" + mysql.escape(req.query.term).substring(1,mysql.escape(req.query.term).length - 1) + "%' AND school=(SELECT id FROM `school` WHERE url="+ mysql.escape(req.params.url) +") order by `name`", function(err, rows, fields){

	// 		// console.log(rows)
	// 		var result = [];
	// 		for(var i in rows)
	// 			result.push(rows[i].name);

	// 		res.send(result);
	// 		// viewFile(req, res, 'form2', {school: school, subject: rows});
	// 	});
	// }

	// catch(err){
	// 	next(err);
	// }
});

router.get('/form2', function (req, res, next) {
	viewFile(req, res, 'form2');
});

// router.get('/request', function(req, res, next) {
//   viewFile(req, res, 'request');
// });

// router.get('/request/languages', function(req, res, next) {
//   viewFile(req, res, 'request-languages');
// });

// router.get('/', function (req, res, next) {
// 		res.end('index');
// });





router.get('/div/:id', function (req, res, next) {

	viewFile(req, res, 'div/' + req.params.id);

});

function adminCheck(req, res, next) {
	try {
		if (req.user) {
			var isAdmin = false;
			for (var i in req.user.emails) {
				console.log(req.user.emails[i].value);
				if (req.user.emails[i].value.split("@").pop() == "ciceron.me") {
					//console.log("jfghgfhgfhg");
					isAdmin = true;
					next();
					break;
				}

			}
			if (!isAdmin) res.redirect('/');

		}
		else {
			res.redirect('/');
		}
	}
	catch (e) {
		res.redirect('/');
	}
}

var viewFile = function (req, res, filename, param) {
	// console.log(param);
	var fullUrl = req.protocol + '://' + req.get('host') + req.path;
	res.render("www/" + filename, {
		title: 'baogao',
		filename: filename,
		url: fullUrl,
		encodedUrl: urlencode(fullUrl),
		req: req,
		param
	});

	// request({
	//     url: req.protocol + '://' + req.get('host') + '/api/v2/user/profile',
	//     // url: req.protocol + '://secure.c.i' + '/api/user/profile',
	//     headers: req.headers
	// }, function (err, res2, body) {
	//     // console.log(body);

	//     try {
	//         // console.log(JSON.parse(body));
	//         res.render("www/" + filename, { title: 'CICERON', filename: filename, user: JSON.parse(body), url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });

	//     } catch (error) {
	//     }
	// });
	//   // console.log(req);
	// console.log(req.headers);
}

module.exports = router;