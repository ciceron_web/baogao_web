

var multer = require('multer');
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		d = Date.now();
		fs.mkdirSync(__dirname + '/../public/uploadedFiles/' + d + '/');
		cb(null, __dirname + '/../public/uploadedFiles/' + d + '/')
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname.substring(0, file.originalname.lastIndexOf('.')).split(' ').join('') + "." + file.originalname.split(".").pop().split(' ').join(''));
		// cb(null, file.originalname.substring(0, file.originalname.lastIndexOf('.')).split(' ').join('') + Date.now() + "." + file.originalname.split(".").pop().split(' ').join(''));
		// cb(null,  Date.now() + "." + file.originalname.split(".").pop())
	}
});

var uploading = multer({
	fileFilter: function (req, file, cb) {
		var UnacceptableMimeTypes = ["exe", "com", "scr", "sh", "ln"]
		if (UnacceptableMimeTypes.indexOf(file.originalname.split(".").pop()) > -1) {
			return cb(new Error('Unacceptable file type.'))
		}

		cb(null, true)
	},
	storage: storage,
	limits: { fileSize: 1000 * 1000 * 20, files: 5 }
});

var express = require('express');
var router = express.Router();
var proxy = require('express-http-proxy');
var request = require('request');
var fs = require('fs');
var urlencode = require('urlencode');
var mysql = require('mysql');
var md5 = require('md5');

var nodemailer = require('nodemailer');


var Iamport = require('iamport');
var iamport = new Iamport({
	impKey: '2311212273535904',
	impSecret: 'jZM7opWBO5K2cZfVoMgYJhsnSw4TiSmBR8JgyGRnLCpYCFT0raZbsrylYDehvBSnKCDjivG4862KLWLd'
});







var mysql_query = function (query, callback) {

	var connection = mysql.createConnection({
		host: global.certification["mysql-host"],
		user: global.certification["mysql-user"],
		password: global.certification["mysql-password"],
		port: global.certification["mysql-port"],
		database: global.certification["mysql-database"],
		timezone: "utc"
	});

	connection.connect();
	if (callback == "sync") {

	}
	else {
		connection.query(query, callback);
		// {
		//   if (!err)
		//     console.log('The solution is: ', rows);
		//   else
		//     console.log('Error while performing Query.', err);
		// }
		connection.end();

	}
}


var sendMail = function (to, subject, html, attachments = []) {

	if (typeof html === 'string') {
		console.log("str")
		var temp = html;
		html = {
			mainTitle: "",
			subTitle: "",
			content: JSON.stringify([{
				type: "old",
				text: temp
			}])
		};
		console.log(html);
	}

	if (typeof html.content !== 'string') {
		html.content = JSON.stringify(html.content);
	}
	request.post({
		url: "https://localhost/api/v1/admin/generateMail",
		rejectUnauthorized: false,
		// headers: { Cookie: " _ga=GA1.1.1029730911.1491870523; _gid=GA1.1.170627832.1496132625; CiceronCookie=c5c8fd51-7832-410b-9b90-da3dca7c37a6; baogao=s%3AbK7_myRNe7Nlmx5tIPDeoswea7joHVkg.RWlRqF2%2BF%2FjnxI29JSzexS2jT4WOc1aV8oioj%2Fe0PRU" },
		formData: html
	}, function (err, response, body) {
		if (err) {
			console.log(err);
			return;
		}
		console.log(body);
		var transporter = nodemailer.createTransport({
			service: 'Gmail',
			auth: {
				user: global.certification["gmail-user"],
				pass: global.certification["gmail-password"]
			},
			tls: {
				rejectUnauthorized: false
			}
		});

		var mailOptions = {
			from: 'baogao <no-reply@ciceron.me>',
			to: to,
			subject: subject,
			html: body,
			attachments: attachments
		};

		console.log(mailOptions);

		transporter.sendMail(mailOptions, function (error, response) {

			if (error) {
				console.log(error);
			} else {
				// console.log("Message sent : " + response.message);
				console.log(response);
			}
			transporter.close();
		});
	});


}


var requestSendMail = function (id) {


	try {
		mysql_query("SELECT *, (SELECT `english_name` FROM `school` WHERE `id` = `request`.`school` LIMIT 1) AS `school_name`, (SELECT `school_email` FROM `auth` WHERE `auth`.`user_email` = `request`.`email` AND `auth`.`isAuthenticated` = 1 LIMIT 1) AS `school_email` FROM `request` WHERE `id` = " + id + ";", function (err, rows, fields) {

			if (err) {
				return err;
			}
			var request = rows[0];

			var mailFiles = [];
			mailFiles.push({ filename: "[한국어]" + request.file.split("/").pop(), content: fs.createReadStream(__dirname + "/../public" + request.file) });
			if (request.original.substr(-1) != '/') {
				mailFiles.push({ filename: "[원문]" + request.original.split("/").pop(), content: fs.createReadStream(__dirname + "/../public" + request.original) });
			}
			//
			// sendMail("hanseulmaro.kim@ciceron.me", "[baogao]교열 요청 알림",


			var mailContent = {
				"mainTitle": "교열 요청 알림",
				"subTitle": "관리자 메세지",
				"content": [
					{
						"type": "table",
						"table": [
							{
								"title": "이름",
								"text": request.name
							},
							{
								"title": "학교명",
								"text": request.school_name
							},
							{
								"title": "학교메일",
								"text": request.school_email
							},
							{
								"title": "이메일",
								"text": request.email
							},
							{
								"title": "강의명",
								"text": request.subject
							},
							{
								"title": "요구사항",
								"text": request.context
							}
						]
					},
					{
						"type": "p",
						"text": "교열 요청이 완료되었습니다. 확인 후 가격을 지정해주세요."
					}
				]
			};

			sendMail("betty@ciceron.me, hanseulmaro.kim@ciceron.me, baogao@ciceron.me", "[baogao]교열 요청 알림", mailContent, mailFiles
			);



			var mailContent2 = {
				"mainTitle": "通知",
				"subTitle": "您的校正请求已被接收。",
				"content": [
					{
						"type": "table",
						"table": [
							{
								"title": "이름",
								"text": request.name
							},
							{
								"title": "이메일",
								"text": request.email
							},
							{
								"title": "강의명",
								"text": request.subject
							},
							{
								"title": "요구사항",
								"text": request.context
							}
						]
					},
					{
						"type": "p",
						"text": "感谢您的使用，我们将继续努力，争取为您提供更优质的服务，谢谢！"
					}
				]
			};

			sendMail(request.email, "[baogao]您的校正请求已被接收。", mailContent2);

		});
	}
	catch (e) {
		next(e);
	}
}

// requestSendMail(193);

var requestStart = function (req, res, next) {
	try {

		// mysql_query("select * from school where url=" + mysql.escape(req.body.url), function (err, rows, fields) {
		// 	if (err) {
		// 		res.status(500).send(JSON.stringify({ code: 500, message: err }));
		// 		return;

		// 	}
		// 	var school = rows[0];
		// 	if (req.body.email.split("@").pop() != school.email) {
		// 		// res.status(401).send(JSON.stringify({ code: 401, message: "Unacceptable email address" }));
		// 		// return;
		// 	}


		console.log(req.files);
		console.log("/uploadedFiles" + req.files.file[0].path.split("uploadedFiles").pop().split("\\").join("/"))

		mysql_query("SELECT `id` FROM `auth` WHERE `user_email` = " + mysql.escape(req.body.email) + " AND `isAuthenticated` = 1", function (err, rows, fields) {
			if (err) {
				res.status(500).send(JSON.stringify({ code: 500, message: err }));
				return;
			}
			var isAuthenticated = rows.length > 0;

			mysql_query("INSERT INTO `request` (`id`, `email`, `subject`, `file`, `context`, `file_language`, `school`, `due`, `original`, `time`, `original_language`, `name`) VALUES (NULL, " + mysql.escape(req.body.email) + ", " + mysql.escape(req.body.subject) + ", '/uploadedFiles" + (typeof req.files.file != "undefined" ? req.files.file[0].path.split("uploadedFiles").pop().split("\\").join("/").split("//").join("/") : "/") + "', " + mysql.escape(req.body.context) + ", '1', " + mysql.escape(req.body.school) + ", CURRENT_TIMESTAMP, '/uploadedFiles" + (typeof req.files.original != "undefined" ? req.files.original[0].path.split("uploadedFiles").pop().split("\\").join("/").split("//").join("/") : "/") + "', CURRENT_TIMESTAMP, '4', " + mysql.escape(req.body.name) + ");", function (err, rows, fields) {


				if (err) {
					res.status(500).send(JSON.stringify({ code: 500, message: err }));
					return;
				}

				var insertId = rows.insertId;


				if (isAuthenticated) {
					requestSendMail(insertId);
				}


				res.send(JSON.stringify({ code: 200, message: "Success", auth: isAuthenticated, id: insertId, email: req.body.email }));
			});

		});
		// });
	}
	catch (err) {
		next(err);
	}
}

var paymentEnd = function (req, res, next) {

	var merchant = mysql.escape(req.query.ciceron_order_id);
	var request_id = mysql.escape(parseInt(req.query.request_id));
	var paidBy = mysql.escape(req.query.payment_platform);
	var status = mysql.escape(req.query.is_succeeded == "success" ? 1 : 0);
	var amount = mysql.escape(req.query.amount);
	var customer_uid = mysql.escape(req.query.PayerID);
	var status = mysql.escape(req.query.is_succeeded == "True" ? 1 : 0);

	console.log(request_id);

	mysql_query("SELECT *, ROUND((`amount` / 1088), 2) AS `amount_dollar`, (SELECT `email` FROM `request` WHERE `request`.`id` = " + request_id + ") AS `request_email` FROM `payment` WHERE `request_id` = " + request_id, function (err, rows, fields) {
		if (err) {
			next(err);
			return;
		}
		if (rows.length < 1) {
			res.status(403).send({
				code: 403,
				message: "This payment information does not exist."
			});
			return;
		}

		var coupon_code = rows[0].coupon_code ? rows[0].coupon_code : "";
		request.post({
			url: req.protocol + '://' + req.get('host') + '/api/admin/coupon/check',
			rejectUnauthorized: false,
			formData: {
				code: coupon_code
			}
		}, function (err, response, body) {
			console.log("debug")
			console.log(body);



			if (err) {
				next(err);
				return;
			}


			var coupon = {};

			body = JSON.parse(body);

			if (typeof body.id != "undefinded") {
				coupon = body;

			}
			else {
				coupon = {
					id: null,
					code: null
				};
			}


			var amount = rows[0].amount;
			var amount_dollar = rows[0].amount_dollar;

			if (coupon.id != null) {
				if (coupon.isPercent) {
					var discount = rows[0].amount * coupon.discount;
					if (discount > coupon.max_discount) {
						discount = coupon.max_discount;
					}
					amount_dollar = amount_dollar - (discount / 1088).toFixed(2);
					amount = amount - discount;
				}
				else {
					amount_dollar = amount_dollar - (coupon.discount / 1088).toFixed(2);
					amount = amount - coupon.discount;
				}
				if (amount < 0) {
					amount = 0;
				}
			}

			console.log(amount);
			console.log(amount_dollar);

			if (req.query.amount != amount && req.query.amount != amount_dollar) {
				res.status(403).send({
					code: 403,
					message: "The pricing information is incorrect."
				});
				return;
			}

			mysql_query("UPDATE `payment` SET `paidBy` = " + paidBy + ", `merchant` = " + merchant + ", `customer_uid` = " + customer_uid + ", `response` = '', `email` = '" + rows[0].request_email + "', `complete_time` = CURRENT_TIMESTAMP, `status` = " + status + " WHERE `payment`.`request_id` = " + request_id, function (err, rows3, fields) {
				if (err) {
					next(err);
					return;
				}

				mysql_query("UPDATE `request` SET `status` = 2 WHERE `id` = " + request_id + ";", function (err, rows2, fields) {
					if (err) {
						next(err);
						return;
					}



					try {
						mysql_query("SELECT *, (SELECT `english_name` FROM `school` WHERE `id` = `request`.`school` LIMIT 1) AS `school_name`, (SELECT `school_email` FROM `auth` WHERE `auth`.`user_email` = `request`.`email` AND `auth`.`isAuthenticated` = 1 LIMIT 1) AS `school_email`, (SELECT `payment`.`amount` FROM `payment` WHERE `request_id` = " + request_id + " ) AS `price` FROM `request` WHERE `id` = " + request_id + ";", function (err, rows, fields) {

							if (err) {
								return err;
							}
							var request = rows[0];

							var mailFiles = [];
							mailFiles.push({ filename: "[한국어]" + request.file.split("/").pop(), content: fs.createReadStream(__dirname + "/../public" + request.file) });
							if (request.original.substr(-1) != '/') {
								mailFiles.push({ filename: "[원문]" + request.original.split("/").pop(), content: fs.createReadStream(__dirname + "/../public" + request.original) });
							}
							//
							// sendMail("hanseulmaro.kim@ciceron.me", "[baogao]교열 요청 알림",


							var mailContent = {
								"mainTitle": "결제완료메일",
								"subTitle": "관리자 메세지",
								"content": [
									{
										"type": "table",
										"table": [
											{
												"title": "이름",
												"text": request.name
											},
											{
												"title": "학교명",
												"text": request.school_name
											},
											{
												"title": "학교메일",
												"text": request.school_email
											},
											{
												"title": "이메일",
												"text": request.email
											},
											{
												"title": "강의명",
												"text": request.subject
											},
											{
												"title": "요구사항",
												"text": request.context
											},
											{
												"title": "가격",
												"text": request.price
											}
										]
									},
									{
										"type": "p",
										"text": "결제가 완료되었습니다. 교열을 시작해주세요."
									}
								]
							};


							sendMail("betty@ciceron.me, hanseulmaro.kim@ciceron.me, baogao@ciceron.me", "[baogao]결제완료메일: 교열 시작해주세요", mailContent, mailFiles);



							var mailContent2 = {
								"mainTitle": "通知",
								"subTitle": "您的付款完成。",
								"content": [
									{
										"type": "table",
										"table": [
											{
												"title": "이름",
												"text": request.name
											},
											{
												"title": "학교명",
												"text": request.school_name
											},
											{
												"title": "학교메일",
												"text": request.school_email
											},
											{
												"title": "이메일",
												"text": request.email
											},
											{
												"title": "강의명",
												"text": request.subject
											},
											{
												"title": "요구사항",
												"text": request.context
											},
											{
												"title": "가격",
												"text": request.price
											}
										]
									},
									{
										"type": "p",
										"text": "工作完成后，我们会通过电子邮件通知您。"
									}
								]
							};

							sendMail(request.email, "[baogao]支付信息", mailContent2);

						});
					}
					catch (e) {
						next(e);
					}


					res.redirect("/checkout/completed?id=" + rows[0].request_id + "&email=" + rows[0].request_email);
				});
			});

		});
	});
}



var generateMail = function (req, res) {
	// [
	// 	{
	// 		"type": "table",
	// 		"table": [
	// 			{
	// 				title: "이메일",
	// 				text: "honeymaro@live.co.kr"
	// 			},
	// 			{
	// 				title: "이메일",
	// 				text: "honeymaro@live.co.kr"
	// 			},
	// 			{
	// 				title: "이메일",
	// 				text: "honeymaro@live.co.kr"
	// 			},
	// 			{
	// 				title: "이메일",
	// 				text: "honeymaro@live.co.kr"
	// 			}
	// 		]
	// 	},
	// 	{
	// 		"type": "p",
	// 		"text": "텍스트"
	// 	},
	// 	{
	// 		"type": "button",
	// 		"href": "http://naver.com",
	// 		"text": "텍스트"
	// 	}

	// ]

	var a = {
		"mainTitle": "교열요청완료",
		"subTitle": "알림",
		"content": [
			{
				"type": "table",
				"table": [
					{
						title: "이메일",
						text: "honeymaro@live.co.kr"
					},
					{
						title: "이메일",
						text: "honeymaro@live.co.kr"
					},
					{
						title: "이메일",
						text: "honeymaro@live.co.kr"
					},
					{
						title: "이메일",
						text: "honeymaro@live.co.kr"
					}
				]
			},
			{
				"type": "p",
				"text": "텍스트"
			},
			{
				"type": "button",
				"href": "http://naver.com",
				"text": "텍스트"
			}

		]
	};

	console.log("body");
	console.log(req.body);
	if (req.body.content[0].type == "old") {
		res.send("fdsa");
		// res.send(req.body.content[0].text);
	}
	else {
		res.render("mail/outline", {
			mainTitle: req.body.mainTitle,
			subTitle: req.body.subTitle,
			content: JSON.parse(req.body.content)
		});
	}
}

router.post('/admin/generateMail', uploading.none(), generateMail);

router.post('/admin/pay/setPrice', uploading.none(), global.adminCheck, function (req, res, next) {
	var request_id = mysql.escape(req.body.request_id);
	var amount = mysql.escape(req.body.amount);

	mysql_query("INSERT INTO `payment` (`id`, `request_id`, `paidBy`, `amount`, `merchant`, `customer_uid`, `response`, `time`, `status`, `email`, `complete_time`) VALUES (NULL, " + request_id + ", '', " + amount + ", '', '', '', CURRENT_TIMESTAMP, '0', '', NULL);", function (err, rows, fields) {
		if (err) {
			next(err);
			return;
		}
		mysql_query("UPDATE `request` SET `status` = 1 WHERE `id` = " + request_id + ";", function (err, rows2, fields) {
			if (err) {
				next(err);
				return;
			}
			try {
				mysql_query("SELECT *, (SELECT `english_name` FROM `school` WHERE `id` = `request`.`school` LIMIT 1) AS `school_name`, (SELECT `school_email` FROM `auth` WHERE `auth`.`user_email` = `request`.`email` AND `auth`.`isAuthenticated` = 1 LIMIT 1) AS `school_email`, (SELECT `payment`.`amount` FROM `payment` WHERE `request_id` = " + request_id + " ) AS `price` FROM `request` WHERE `id` = " + request_id + ";", function (err, rows, fields) {

					if (err) {
						return err;
					}
					var request = rows[0];

					var mailFiles = [];
					mailFiles.push({ filename: "[한국어]" + request.file.split("/").pop(), content: fs.createReadStream(__dirname + "/../public" + request.file) });
					if (request.original.substr(-1) != '/') {
						mailFiles.push({ filename: "[원문]" + request.original.split("/").pop(), content: fs.createReadStream(__dirname + "/../public" + request.original) });
					}



					var mailContent = {
						"mainTitle": "가격 지정 알림",
						"subTitle": "관리자 메세지",
						"content": [
							{
								"type": "table",
								"table": [
									{
										"title": "이름",
										"text": request.name
									},
									{
										"title": "학교명",
										"text": request.school_name
									},
									{
										"title": "학교메일",
										"text": request.school_email
									},
									{
										"title": "이메일",
										"text": request.email
									},
									{
										"title": "강의명",
										"text": request.subject
									},
									{
										"title": "요구사항",
										"text": request.context
									},
									{
										"title": "가격",
										"text": request.price
									}
								]
							},
							{
								"type": "p",
								"text": req.user.displayName + "님께서 가격 정보를 입력하였습니다."
							}
						]
					};

					sendMail("betty@ciceron.me, hanseulmaro.kim@ciceron.me, baogao@ciceron.me", "[baogao]" + req.user.displayName + "님께서 가격 정보를 입력하였습니다.", mailContent, mailFiles
					);


					var mailContent2 = {
						"mainTitle": "通知",
						"subTitle": "经理确认您的要求并设定价格。",
						"content": [
							{
								"type": "table",
								"table": [
									{
										"title": "이름",
										"text": request.name
									},
									{
										"title": "이메일",
										"text": request.email
									},
									{
										"title": "강의명",
										"text": request.subject
									},
									{
										"title": "요구사항",
										"text": request.context
									}
								]
							},
							{
								"type": "p",
								"text": "请点击下面的链接继续。"
							},
							{
								"type": "button",
								"href": "http://baogao.co/checkout?id=" + request.id + "&email=" + request.email,
								"text": "CHECKOUT"
							}

						]
					};

					sendMail(request.email, "[baogao]支付信息", mailContent2);

				});
			}
			catch (e) {
				next(e);
			}
			res.send(rows);
		});
	});

});


router.post('/user/pay/start', uploading.none(), function (req, res, next) {
	//INSERT INTO `payment` (`id`, `request_id`, `paidBy`, `amount`, `merchant`, `customer_uid`, `response`, `time`, `currency`, `status`) VALUES (NULL, '1', 'alipay', '30', 'baogao', 'baogao_1_30030303030', '', CURRENT_TIMESTAMP, 'USD', '0')
	var paidBy = req.body.payBy;
	var request_id = req.body.request_id;
	var coupon_code = req.body.coupon_code;
	console.log("start");
	request.post({
		url: req.protocol + '://' + req.get('host') + '/api/admin/coupon/check',
		rejectUnauthorized: false,
		formData: {
			code: coupon_code
		}
	}, function (err, response, body) {
		console.log(body);
		console.log("fdsafdsa");



		if (err) {
			next(err);
			return;
		}


		var coupon = {};

		body = JSON.parse(body);
		if (typeof body.id != "undefinded") {
			coupon = body;

		}
		else {
			coupon = {
				id: null,
				code: null
			};
		}

		mysql_query("SELECT *, ROUND((`amount` / 1088), 2) AS `amount_dollar`, (SELECT `email` FROM `request` WHERE `id` = " + request_id + ") AS `request_email` FROM `payment` WHERE `request_id` = " + mysql.escape(request_id), function (err, rows, fields) {

			var formData = {
				payment_platform: paidBy,
				request_id: rows[0].request_id,
				amount: 0,
				user_email: rows[0].request_email,
				product: "baogao"
			};

			var payment = rows[0];




			if (paidBy == "iamport") {
				formData.amount = payment.amount;

				mysql_query("UPDATE `payment` SET `coupon_code` = " + mysql.escape(coupon_code) + " WHERE `payment`.`id` = " + rows[0].id + ";", function (err, rows, fields) {
					res.send({
						link: "/checkout/card?id=" + request_id + "&email=" + payment.request_email
					});
				});
				return;
			}
			else {
				formData.amount = payment.amount_dollar;

				//쿠폰 할인 관련
				if (coupon.id != null) {
					if (coupon.isPercent) {
						var discount = payment.amount * coupon.discount;
						if (discount > coupon.max_discount) {
							discount = coupon.max_discount;
						}
						formData.amount = formData.amount - (discount / 1088).toFixed(2);
					}
					else {
						formData.amount = formData.amount - (coupon.discount / 1088).toFixed(2);
					}
				}
				request.post({
					url: global.api_url + "/api/v2/user/payment/start",
					rejectUnauthorized: false,
					// headers: { Cookie: " _ga=GA1.1.1029730911.1491870523; _gid=GA1.1.170627832.1496132625; CiceronCookie=c5c8fd51-7832-410b-9b90-da3dca7c37a6; baogao=s%3AbK7_myRNe7Nlmx5tIPDeoswea7joHVkg.RWlRqF2%2BF%2FjnxI29JSzexS2jT4WOc1aV8oioj%2Fe0PRU" },
					formData: formData
				}, function (err, response, body) {
					console.log(body);
					mysql_query("UPDATE `payment` SET `coupon_code` = " + mysql.escape(coupon_code) + " WHERE `payment`.`id` = " + payment.id + ";", function (err, rows, fields) {
						res.send(body);
					});
				});
			}

		});
	});



});





router.get('/user/pay/end', uploading.none(), paymentEnd);
router.get('/user/pay/end/iamport', uploading.none(), paymentEnd);
router.get('/user/pay/end/paypal', uploading.none(), paymentEnd);
router.get('/user/pay/end/alipay', uploading.none(), paymentEnd);


router.post('/user/pay/imp', uploading.none(), function (req, res, next) {
	console.log(req.body);
	// var merchant = mysql.escape(req.body.merchant);
	var request_id = mysql.escape(req.body.request_id);
	// var amount = mysql.escape(req.body.amount);
	// var uid = mysql.escape(req.body.uid);

	var card_number = req.body.card_number.join("-"); // XXXX-XXXX-XXXX-XXXX
	var expiry = "20" + req.body.expiry[1] + "-" + req.body.expiry[0]; // XXXX-XX
	var birth = req.body.birth; // XXXXXX or XXXXXXXXXX
	var pwd_2digit = req.body.pwd_2digit; // XX

	console.log(card_number);
	console.log(expiry);

	mysql_query("SELECT *, ROUND((`amount` / 1088), 2) AS `amount_dollar`, (SELECT `email` FROM `request` WHERE `id` = " + request_id + ") AS `request_email` FROM `payment` WHERE `request_id` = " + request_id, function (err, rows, fields) {
		if (err) {
			next(err);
			return;
		}

		var coupon_code = rows[0].coupon_code ? rows[0].coupon_code : "";

		request.post({
			url: req.protocol + '://' + req.get('host') + '/api/admin/coupon/check',
			rejectUnauthorized: false,
			formData: {
				code: coupon_code
			}
		}, function (err, response, body) {
			console.log(body);



			if (err) {
				next(err);
				return;
			}


			var coupon = {};

			body = JSON.parse(body);

			if (typeof body.id != "undefinded") {
				coupon = body;

			}
			else {
				coupon = {
					id: null,
					code: null
				};
			}



			var formData = {
				payment_platform: "iamport",
				request_id: rows[0].request_id,
				amount: rows[0].amount,
				product: "baogao",
				card_number: card_number,
				expiry: expiry,
				birth: birth,
				pwd_2digit: pwd_2digit,
				user_email: rows[0].request_email,

			}

			//쿠폰 할인 관련
			if (coupon.id != null) {
				if (coupon.isPercent) {
					var discount = rows[0].amount * coupon.discount;
					if (discount > coupon.max_discount) {
						discount = coupon.max_discount;
					}
					formData.amount = formData.amount - discount;
				}
				else {
					formData.amount = formData.amount - coupon.discount;
				}
				if (formData.amount < 0) {
					formData.amount = 0;
				}
			}


			console.log(formData);
			request.post({
				url: global.api_url + "/api/v2/user/payment/start",
				headers: { Cookie: " _ga=GA1.1.1029730911.1491870523; _gid=GA1.1.170627832.1496132625; CiceronCookie=c5c8fd51-7832-410b-9b90-da3dca7c37a6; baogao=s%3AbK7_myRNe7Nlmx5tIPDeoswea7joHVkg.RWlRqF2%2BF%2FjnxI29JSzexS2jT4WOc1aV8oioj%2Fe0PRU" },
				rejectUnauthorized: false,
				formData: formData
			}, function (err, response, body) {
				res.send(body);
			});

		});
	});




	// mysql_query("SELECT * FROM `payment` WHERE `merchant` = " + merchant + " AND `request_id` = " + request_id + " AND `amount` = " + amount + " AND `customer_uid` = " + uid + " LIMIT 1;", function (err, rows, fields) {
	// 	if (rows.length > 0) {

	// 		if (rows[0].status == 0) {

	// 			iamport.subscribe.onetime({
	// 				merchant_uid: req.body.merchant,
	// 				amount: req.body.amount,
	// 				card_number: card_number,
	// 				expiry: expiry,
	// 				birth: birth,
	// 				pwd_2digit: pwd_2digit,
	// 				name: "baogao"
	// 			}).then(function (result) {
	// 				// To do 
	// 				mysql_query("UPDATE `request` SET `status` = 1 WHERE `merchant` = " + merchant + " AND `request_id` = " + request_id + " AND `amount` = " + amount + " AND `customer_uid` = " + uid, function (err, rows, fields) {

	// 				})
	// 				console.log(result);
	// 			}).catch(function (error) {
	// 				// handle error 
	// 				console.log("실패?");
	// 				console.log(error);
	// 			});
	// 		}
	// 		else {
	// 			console.log("이미 결제 완료됨.");
	// 			res.send({
	// 				code: 403,
	// 				message: "You have already paid."
	// 			});
	// 			return;
	// 		}
	// 	}
	// 	else {
	// 		console.log("일치하는 결제 정보 없음.");
	// 		res.send({
	// 			code: 403,
	// 			message: "No matching payment information found."
	// 		});
	// 		return;
	// 	}
	// });

});

router.registerAdmin = function (req) {
	//
	// console.log("================================================")
	// console.log(req.user);
	mysql_query("REPLACE INTO `admin` (`id`, `name`, `email`, `picture`, `time`) VALUES(NULL, '" + req.user.displayName + "', '" + req.user.emails[0].value + "', '" + req.user.photos[0].value + "', CURRENT_TIMESTAMP)", function (err, rows, fields) {

	});
}

router.post('/user/quiz/coupon', uploading.none(), function (req, res, next) {
	console.log(req.body);

	var chars = "0123456789ACDEFGHIJKLMNOPRSTUVWXTZ";
	var string_length = 8;
	var randomstring = '';
	for (var i = 0; i < string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}

	var formData = {
		email: req.body.email,
		code: "BQ" + randomstring,
		isPercent: 1,
		discount: 0.2,
		max_discount: 2000,
		expiry: 14,
		note: "바오가오 퀴즈 상품"
	}

	request.post({
		url: "https://localhost/api/v1/admin/coupon/issue",
		// headers: { Cookie: " _ga=GA1.1.1029730911.1491870523; _gid=GA1.1.170627832.1496132625; CiceronCookie=c5c8fd51-7832-410b-9b90-da3dca7c37a6; baogao=s%3AbK7_myRNe7Nlmx5tIPDeoswea7joHVkg.RWlRqF2%2BF%2FjnxI29JSzexS2jT4WOc1aV8oioj%2Fe0PRU" },
		rejectUnauthorized: false,
		formData: formData
	}, function (err, response, body) {
		if (err) {
			res.send({
				code: 4001,
				message: "error"
			});
			return;
		}
		else {
			res.send({
				code: 200,
				message: "success"
			});
			return;
		}

	});
});

router.post('/user/contact', uploading.none(), function (req, res, next) {
	try {
		var mailContent = {
			"mainTitle": "관리자 메세지",
			"subTitle": "웹사이트에서 문의사항이 들어왔습니다.",
			"content": [
				{
					"type": "table",
					"table": [
						{
							"title": "이름",
							"text": req.body.name
						},
						{
							"title": "이메일",
							"text": req.body.email
						},
						{
							"title": "메세지",
							"text": req.body.message
						}
					]
				},
				{
					"type": "p",
					"text": "답장이 필요한 경우 " + req.body.email + " 로 회신하십시오."
				}
			]
		};

		sendMail("baogao@ciceron.me", "[baogao]문의사항 도착했습니다.", mailContent);
		res.send("");

	}
	catch (e) {
		res.status(500).send("");
	}
});

router.post('/user/request', uploading.fields([{ name: "file", maxCount: 1 }, { name: "original", maxCount: 1 }]), requestStart);


router.post('/user/request/start', uploading.fields([{ name: "file", maxCount: 1 }, { name: "original", maxCount: 1 }]), requestStart);



router.post('/admin/request/reject', uploading.none(), function (req, res, next) {
	var request_id = mysql.escape(req.body.request_id);
	var message = req.body.message;
	mysql_query("UPDATE `request` SET `status` = '-1' WHERE `request`.`id` = " + request_id, function (err, rows, fields) {
		if (err) {
			console.log(err);
			next();
			return;
		}
		console.log(rows);


		var mailContent2 = {
			"mainTitle": "通知",
			"subTitle": "교열의뢰가 거절되었습니다.",
			"content": [
				{
					"type": "table",
					"table": [
						{
							"title": "이름",
							"text": request.name
						},
						{
							"title": "이메일",
							"text": request.email
						},
						{
							"title": "강의명",
							"text": request.subject
						},
						{
							"title": "요구사항",
							"text": request.context
						},
						{
							"title": "교열 불가 사유",
							"text": message
						}
					]
				},
				{
					"type": "p",
					"text": "조건을 충족시킨 후 다시 의뢰해주세요."
				}
			]
		};

		sendMail(request.email, "[baogao]교열불가안내", mailContent2);





		res.send(rows);


	});
});

router.get('/admin/user/getReviewerList', uploading.none(), global.adminCheck, function (req, res, next) {
	mysql_query("SELECT * FROM `admin`;", function (err, rows, fields) {
		if (err) {
			res.send(err);
			return;
		}
		res.send(rows);
	});
});

router.post('/admin/request/ignore', uploading.none(), global.adminCheck, function (req, res, next) {
	var request_id = mysql.escape(req.body.request_id);

	mysql_query("UPDATE `request` SET `status` = '-2' WHERE `request`.`id` = " + request_id, function (err, rows, fields) {
		if (err) {
			console.log(err);
			next();
			return;
		}
		console.log(rows);
		res.send(rows);


	});
});



router.post('/user/auth/start', uploading.none(), function (req, res, next) {
	console.log(req.body);
	var school_id = mysql.escape(req.body.school_id);
	var school_email = mysql.escape(req.body.school_email);
	var user_email = mysql.escape(req.body.user_email);
	var token = md5(req.body.school_email + Date.now());


	if (school_email != user_email) {
		res.status(401).send({ code: 401, message: "Unacceptable email address" });
		return;
	}

	try {

		mysql_query("SELECT `email` FROM `school` WHERE `id` = " + school_id, function (err, rows, fields) {
			if (err) {
				res.send(err);
				return;
			}


			// if (req.body.school_email.split("@").pop() != rows[0].email) {
			// 	res.status(401).send({ code: 401, message: "Unacceptable email address" });
			// 	return;
			// }

			mysql_query("INSERT INTO `auth` (`id`, `school_id`, `school_email`, `user_email`, `time`, `token`, `isAuthenticated`) VALUES (NULL, " + school_id + ", " + school_email + ", " + user_email + ", CURRENT_TIMESTAMP, '" + token + "', '0');", function (err, rows, fields) {
				if (err) {
					res.send(err);
					return;
				}


				var mailContent2 = {
					"mainTitle": "通知",
					"subTitle": "교열 요청을 위해 계정을 인증해주세요.",
					"content": [
						{
							"type": "p",
							"text": "아래 링크를 눌러 완료합니다."
						},
						{
							"type": "button",
							"href": "http://baogao.co/auth/" + token,
							"text": "http://baogao.co/auth/" + token
						}

					]
				};

				sendMail(req.body.school_email, "[baogao]계정 인증", mailContent2);

				mysql_query("SELECT `token` FROM `auth` WHERE `id` = " + rows.insertId, function (err, rows, fields) {

					if (err) {
						next(err);
						return;
					}
					res.send(rows[0]);
				})
			});

		});

	} catch (error) {
		console.log(error);
		next(error)
	}

});

router.get('/user/auth/check', function (req, res, next) {
	var token = mysql.escape(req.query.token);
	mysql_query("SELECT `token`, `isAuthenticated` FROM `auth` WHERE `token` = " + token, function (err, rows, fields) {
		if (err) {
			next(err);
			return;
		}

		res.send(rows[0]);
	})
});

router.get('/user/auth/confirm', function (req, res, next) {
	var token = mysql.escape(req.query.token);
	try {
		mysql_query("UPDATE `auth` SET `isAuthenticated` = 1 WHERE `auth`.`token` = " + token, function (err, rows, fields) {
			if (err) {
				next(err);
				return;
			}
			console.log(rows);
			if (rows.changedRows > 0) {
				mysql_query("SELECT `id` FROM `request` WHERE `request`.`email` = (SELECT `auth`.`user_email` FROM `auth` WHERE `auth`.`token` = " + token + ") ORDER BY `id` DESC", function (err, rows, fields) {
					requestSendMail(rows[0].id);
				});
			}
			res.send(JSON.stringify(rows));
		});
	}
	catch (e) {
		next(e);
	}
});

console.log(new Date());
mysql_query("SELECT now();", function (err, rows, fields) {
	console.log(rows);
})
router.post('/admin/writeComment', global.adminCheck, uploading.fields([{ name: "file", maxCount: 1 }]), function (req, res, next) {
	//req.files.file[0].filename
	var request_id = mysql.escape(req.body.request_id);
	var content = mysql.escape(req.body.content);


	try {
		mysql_query("INSERT INTO `request_comment` (`id`, `request_id`, `admin_email`, `content`, `file`, `time`) VALUES (NULL, " + request_id + ", '" + req.user.emails[0].value + "', " + content + ", " + (typeof req.files.file != "undefined" ? "'/uploadedFiles/" + req.files.file[0].filename + "'" : "NULL") + ", CURRENT_TIMESTAMP);", function (err, rows, fields) {
			if (err) {
				next(err);
				return;
			}
			res.send({
				id: rows.insertId,
				request_id: req.body.request_id,
				admin_email: req.user.emails[0].value,
				content: req.body.content,
				time: new Date(),
				file: typeof req.files.file != "undefined" ? "/uploadedFiles/" + req.files.file[0].filename : null
			});
		});

	}
	catch (err) {
		next(err);
	}

});


router.put('/admin/updateComment', global.adminCheck, uploading.fields([{ name: "file", maxCount: 1 }]), function (req, res, next) {
	//req.files.file[0].filename

	var content = mysql.escape(req.body.content);
	var id = mysql.escape(req.body.request_id);

	try {
		// mysql_query("INSERT INTO `request_comment` (`id`, `request_id`, `admin_email`, `content`, `file`, `time`) VALUES (NULL, '"+req.body.request_id+"', '"+req.user.emails[0].value+"', '"+req.body.content+"', "+(typeof req.files.file != "undefined" ? "'/uploadedFiles/"+req.files.file[0].filename+"'": "NULL")+", CURRENT_TIMESTAMP);", function(err, rows, fields){
		mysql_query("UPDATE `request_comment` SET `content` = " + content + (typeof req.files.file != "undefined" ? ", `file` = '/uploadedFiles/" + req.files.file[0].filename + "'" : "") + " WHERE `id` = " + id + "", function (err, rows, fields) {
			if (err) {
				next(err);
				return;
			}
			res.send(rows);
		});

	}
	catch (err) {
		next(err);
	}

});

router.post('/admin/deleteComment', global.adminCheck, uploading.none(), function (req, res, next) {
	//req.files.file[0].filename

	var id = mysql.escape(req.body.request_id);

	try {
		mysql_query("DELETE FROM `request_comment` WHERE `id` = " + id, function (err, rows, fields) {
			if (err) {
				next(err);
				return;
			}
			res.send(rows);
		});

	}
	catch (err) {
		next(err);
	}

});


router.get('/user/:url/search_subject', function (req, res, next) {
	try {
		mysql_query("select `name` from subject where `code` LIKE '%" + mysql.escape(req.query.term).substring(1, mysql.escape(req.query.term).length - 1) + "%' OR `name` LIKE '%" + mysql.escape(req.query.term).substring(1, mysql.escape(req.query.term).length - 1) + "%' AND school=(SELECT id FROM `school` WHERE url=" + mysql.escape(req.params.url) + ") order by `name`", function (err, rows, fields) {

			// console.log(rows)
			var result = [];
			for (var i in rows)
				result.push(rows[i].name);

			res.send(result);
			// viewFile(req, res, 'form2', {school: school, subject: rows});
		});
	}

	catch (err) {
		next(err);
	}
});


router.get('/admin/getRequest', global.adminCheck, function (req, res, next) {
	var id = mysql.escape(req.query.id);
	mysql_query("SELECT *, (SELECT url FROM school WHERE school.id = request.school) AS `school_url` FROM `request` WHERE `id` = " + id, function (err, rows, fields) {
		if (err) next();
		var request = rows[0];
		mysql_query("SELECT * FROM `request_comment` WHERE `request_id` = " + id, function (err, rows2, fields) {
			if (err) next();
			request.comment = rows2;
			mysql_query("SELECT * FROM `payment` WHERE `request_id` = " + id + ";", function (err, rows, fields) {
				if (err) next();
				request.payment = rows.length > 0 ? rows[0] : null;
				res.send(request);
			})
		})

	});
});

router.get('/admin/getRequestList', global.adminCheck, function (req, res, next) {
	var showNumber = req.query.num ? req.query.num : 12;
	var page = req.query.page ? req.query.page : 1;
	var orderBy = req.query.orderBy ? req.query.orderBy : "id";
	var sort = req.query.sort ? req.query.sort : "DESC";
	var viewHidden = req.query.viewHidden == 1 || req.query.viewHidden == 'true' ? true : false;

	var status = viewHidden ? -10 : 0;

	if (isNaN(showNumber)) showNumber = 12;
	if (isNaN(page)) page = 1;

	if (sort.toUpperCase() == "ASC") {
		sort = "ASC";
	}
	else {
		sort = "DESC";
	}


	mysql_query("SELECT *, (SELECT url FROM school WHERE school.id = request.school) as `school_url`, (SELECT EXISTS(SELECT 1 FROM `auth` WHERE `auth`.`user_email` = `request`.`email`)) AS `isAuthenticated`, (SELECT EXISTS(SELECT 1 FROM `payment` WHERE `payment`.`request_id` = `request`.`id` AND `payment`.`status` = 1)) AS `isPaid` FROM `request` WHERE `request`.`status` >= " + status + " ORDER BY `request`.`" + orderBy + "` " + sort + " LIMIT " + ((page - 1) * showNumber) + ", " + showNumber, function (err, rows, fields) {
		if (!err) {
			var c = 0;
			var comment = [];

			console.log(rows);



			mysql_query("SELECT COUNT(*) as `count` FROM `request`", function (err2, rows2, fields2) {
				if (!err2) {

					var getComment = function (item) {
						mysql_query("SELECT * FROM `request_comment` WHERE `request_id` = '" + rows[item].id + "'", function (err3, rows3, fields3, index = item) {
							if (!err3) {
								rows[index].comment = rows3;
								c++;

								// rows[item].comment.push(rows3);
							}
							else {
								console.log(err3);
								next();
							}
							if (c >= rows.length) {
								console.log("done");
								res.send({ nowPage: page, totalNumber: rows2[0].count, pageAmount: parseInt((parseInt(rows2[0].count) / showNumber + 1)), data: rows });
							}
						});
					}

					for (item in rows) {
						getComment(item);
					}


				}
				else {
					console.log(err2);
					next();
				}
			});
			// next();
			// viewFile(req, res, 'main', {school: rows});
		}
		else {
			console.log(err);
			next();
		}
	});
	// next();
});



router.get('/admin/getSchoolList', global.adminCheck, function (req, res, next) {


	mysql_query("SELECT * FROM `school`", function (err, rows, fields) {
		if (!err) {
			console.log(rows);
			res.send(rows);
			// next();
			// viewFile(req, res, 'main', {school: rows});
		}
		else {
			console.log(err);
			next();
		}
	});
	// next();
});



router.get('/admin/getLanguageList', global.adminCheck, function (req, res, next) {


	mysql_query("SELECT * FROM `language`", function (err, rows, fields) {
		if (!err) {
			console.log(rows);
			res.send(rows);
			// next();
			// viewFile(req, res, 'main', {school: rows});
		}
		else {
			console.log(err);
			next();
		}
	});
	// next();
});

router.post('/admin/coupon/issue', uploading.none(), function (req, res, next) {
	var email = mysql.escape(req.body.email);
	var code = mysql.escape(req.body.code);
	var isPercent = mysql.escape(req.body.isPercent);
	var discount = mysql.escape(req.body.discount);
	var max_discount = req.body.isPercent == 1 ? mysql.escape(req.body.max_discount) : discount;
	var status = mysql.escape(0);
	var expiry = parseInt(req.body.expiry);
	var note = mysql.escape(req.body.note);

	console.log(req.body);

	// console.log("INSERT INTO `coupon` (`id`, `email`, `code`, `isPercent`, `discount`, `max_discount`, `status`, `expiry`, `time`, `used_time`, `note`) VALUES (NULL, " + email + ", " + code + ", " + isPercent + ", " + discount + ", " + max_discount + ", " + status + ", NOW() + INTERVAL " + expiry + " DAY, CURRENT_TIMESTAMP, NULL, " + note + ");");
	// res.send("INSERT INTO `coupon` (`id`, `email`, `code`, `isPercent`, `discount`, `max_discount`, `status`, `expiry`, `time`, `used_time`, `note`) VALUES (NULL, " + email + ", " + code + ", " + isPercent + ", " + discount + ", " + max_discount + ", " + status + ", (NOW() + INTERVAL " + expiry + " DAY), CURRENT_TIMESTAMP, NULL, " + note + ");")

	mysql_query("INSERT INTO `coupon` (`id`, `email`, `code`, `isPercent`, `discount`, `max_discount`, `status`, `expiry`, `time`, `used_time`, `note`) VALUES (NULL, " + email + ", " + code + ", " + isPercent + ", " + discount + ", " + max_discount + ", " + status + ", (NOW() + INTERVAL " + expiry + " DAY), CURRENT_TIMESTAMP, NULL, " + note + ");", function (err, rows, fields) {
		if (err) {
			console.log(err);
			next(err);
			return;
		}
		if (req.body.email.length > 0) {
			var mailContent = {
				"mainTitle": "쿠폰이 발급되었습니다.",
				"subTitle": "알림",
				"content": [
					{
						"type": "table",
						"table": [
							{
								"title": "쿠폰번호",
								"text": req.body.code
							},
							{
								"title": "할인",
								"text": (req.body.isPercent == 1 ? parseInt(parseFloat(req.body.discount) * 100) + "%, 최대 " + req.body.max_discount + "원" : req.body.discount + "원")
							},
							{
								"title": "사용기한",
								"text": "발급일로부터 " + req.body.expiry + "일간"
							},
							{
								"title": "쿠폰 내용",
								"text": req.body.note
							}
						]
					},
					{
						"type": "p",
						"text": "쿠폰이 발급되었습니다."
					}
				]
			};
			sendMail(req.body.email, "[baogao]쿠폰이 발급되었습니다.", mailContent);
		}



		res.send(rows);
	});
});

var checkCoupon = function (req, res, next) {
	console.log(req.body);

	var code = mysql.escape(req.body.code);
	var request_id = parseInt(req.body.request_id);

	mysql_query("SELECT COUNT(*) AS `count` FROM `payment` WHERE `status` = 1 AND `coupon_code` = " + code + ";", function (err, rows, fields) {

		if (rows[0].count > 0) {
			res.send(JSON.stringify(
				{
					code: 4001,
					message: "already used."
				}
			));
			return;
		}
		else {
			mysql_query("SELECT * FROM `coupon` WHERE `code` = " + code, function (err, rows, fields) {
				if (err) {
					next(err);
					return;
				}
				var coupon = {};
				if (rows.length > 0) {
					var notMine = true;
					for (i = 0; i < rows.length; i++) {
						if (rows[i].email == req.query.email || rows[i].email == null || rows[i].email == "") {
							notMine = false;
							coupon = rows[i];
							break;
						}
					}

					if (notMine) {
						res.send(JSON.stringify(
							{
								code: 4002,
								message: "not exist."
							}
						));
						return;
					}
				}
				else if (rows.length < 1) {
					res.send(JSON.stringify(
						{
							code: 4002,
							message: "not exist."
						}
					));
					return;
				}

				console.log(coupon);

				if (request_id > 0) {
					mysql_query("SELECT * FROM `payment` WHERE `request_id` = '" + request_id + "';", function (err, rows, fields) {
						console.log(err);
						if (!err && rows.length > 0) {
							coupon.request = rows[0];

							var amount = rows[0].amount;
							if (coupon.isPercent) {
								var discount = rows[0].amount * coupon.discount;
								if (discount > coupon.max_discount) {
									discount = coupon.max_discount;
								}
								amount = amount - discount;
								coupon.request.discounted = discount;
							}
							else {
								amount = amount - coupon.discount;
								coupon.request.discounted = coupon.discount;
							}

							if (amount < 0) {
								amount = 0;
							}

							coupon.request.amount = amount;
						}

						res.send(coupon);
					});
				}
				else {
					res.send(coupon);
				}


				// if (coupon.status == 1) {

				// 	res.send(JSON.stringify(
				// 		{
				// 			code: 410,
				// 			message: "This coupon has already been redeemed."
				// 		}
				// 	));
				// 	return;
				// }


			});

		}
	});



}

router.post('/admin/coupon/check', uploading.none(), checkCoupon);
router.post('/user/coupon/check', uploading.none(), checkCoupon);


router.post('/admin/coupon/use', uploading.none(), function (req, res, next) {
	var code = mysql.escape(req.body.code);
	var email = mysql.escape(req.body.email);

	mysql_query("SELECT * FROM `coupon` WHERE `code` = " + code + " AND (`email` = " + email + " OR `email` = NULL OR `email` = '');", function (err, rows, fields) {
		// res.send("SELECT * FROM `coupon` WHERE `code` = " + code + " AND (`email` = "+email+" OR `email` = NULL OR `email` = ''); ");
		// return;
		if (err) {
			res.send(
				JSON.stringify({
					code: 403,
					message: "failed"
				})
			);
			return;
		}

		if (rows.length < 1) {
			res.send(
				JSON.stringify({
					code: 403,
					message: "coupon does not exist."
				})
			)
		}

		mysql_query("UPDATE `coupon` SET `status` = '1', `used_time` = CURRENT_TIME() WHERE `coupon`.`id` = " + rows[0].id + " ;", function (err, rows, fields) {
			if (err) {
				res.send(
					JSON.stringify({
						code: 403,
						message: "failed"
					})
				);
				return;
			}
			res.send(rows);
		});
	});
});



var GoogleTokenProvider = require('refresh-token').GoogleTokenProvider;
var tokenProvider = new GoogleTokenProvider({
	refresh_token: '1/NPGPGmh5N7wO0aw2fhHrQ2zsa9Je7jhhHZqbJl75bPQ',
	client_id: global.googleClientId,
	client_secret: global.googleClientSecret
});

//baogao ciceron
//1/NPGPGmh5N7wO0aw2fhHrQ2zsa9Je7jhhHZqbJl75bPQ

//1/LHdwRlZFpKGMIsLGKiyjAvMg7S_JflfAJDhIwiFakRI
router.get('/googleDrive/:filename', function (req, res, next) {
	var fileName = req.params.filename;
	tokenProvider.getToken(function (err, token) {
		console.log(token);
		var googleDrive = require('google-drive');
		//0B2x3EfgjIFDYTE5Pc1B6SVpWaVU
		// googleDrive(token).files("0B2x3EfgjIFDYTE5Pc1B6SVpWaVU").permissions({ q: "title = '" + fileName + "'" }, function (data, data2, data3) {
		googleDrive(token).files().list({ q: "title = '" + fileName + "'" }, function (data, data2, data3) {
			console.log(data);
			console.log(data2);
			res.send(JSON.stringify(JSON.parse(data3), null, '\t'));
		});
	});
});




module.exports = router;
