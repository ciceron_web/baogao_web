
// global.api_url = "http://ciceron.xyz:5000";
global.api_url = "http://ciceron.xyz";
global.quiz_api_url = "http://test.baogao.co:5000";
global.secure_url = "https://secure.localhost:4430";

global.googleClientId = "793272019863-d5k34k247iqdd5unc4krth0sk14ef8km.apps.googleusercontent.com";
global.googleClientSecret = "BnuLxmkV-uwegj0raKLmIj5e";


var subdomain = require('express-subdomain');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');

var bodyParser = require('body-parser');

var session = require('express-session');
var swaggerJSDoc = require('swagger-jsdoc');
var request = require('request');

var fs = require('fs');

global.certification = JSON.parse(fs.readFileSync('certification.json'));


// var google = require('googleapis');
// var googleAuth = require('google-auth-library');

// auth = new googleAuth();
// console.log(auth);

global.adminCheck = function (req, res, next) {
  try {
    // if(req.get('host').indexOf("localhost") >= 0) {
    //   next();
    //   return;
    // }
    if (req.user) {
      var isAdmin = false;
      for (var i in req.user.emails) {
        console.log(req.user.emails[i].value);
        if (req.user.emails[i].value.split("@").pop() == "ciceron.me") {
          //console.log("jfghgfhgfhg");
          isAdmin = true;
          next();
          break;
        }

      }
      if (!isAdmin) {
        req.session.redirectTo = req.url;
        res.redirect('/auth/google');
      }
    }
    else {
      req.session.redirectTo = req.url;
      res.redirect('/auth/google');
    }
  }
  catch (e) {
    req.session.redirectTo = req.url;
    res.redirect('/auth/google');
  }
}


global.loginRequired = function (req, res, next) {
  try{
    if (req.baogaoUser.logged_in) {
      next();
    }
    else {
      res.redirect("/signin");
    }
  }
  catch(e){
    res.redirect("/signin");
  }
}


var routeWww = require('./routes/www');
var routeApi = require('./routes/api');



var app = express();
app.all('/*', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

// // Swagger definition
// // You can set every attribute except paths and swagger
// // https://github.com/swagger-api/swagger-spec/blob/master/versions/2.0.md
// var swaggerDefinition = {
//   info: { // API informations (required)
//     title: 'baogao', // Title (required)
//     version: '1.0.0', // Version (required)
//     description: 'baogao API', // Description (optional)
//   },
//   host: 'localhost', // Host (optional)
//   basePath: '/api/v1/', // Base path (optional)
// };

// // Options for the swagger docs
// var options = {
//   // Import swaggerDefinitions
//   swaggerDefinition: swaggerDefinition,
//   // Path to the API docs
//   apis: ['./routes/api.js'],
// };

// // Initialize swagger-jsdoc -> returns validated swagger spec in json format
// var swaggerSpec = swaggerJSDoc(options);

// // Serve swagger docs the way you like (Recommendation: swagger-tools)
// app.get('/api-docs.json', function (req, res) {
//   res.setHeader('Content-Type', 'application/json');
//   res.send(swaggerSpec);
// });

try {
  var nodeadmin = require('nodeadmin');
  app.use(nodeadmin(app));

}
catch (e) {

}



// app.use(session({
//   key: 'baogao', // 세션키
//   secret: 'baogaossss', // 비밀키
//   cookie: {
//     maxAge: 1000 * 60 * 60 // 쿠키 유효기간 1시간
//   }
// }));


// var proxy = require('express-http-proxy');
var httpProxy = require('http-proxy');
var proxy = httpProxy.createProxyServer({});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

// app.use('/api', proxy(global.api_url, {
//   preserveHostHdr: true,
//   forwardPath: function(req, res) {
//     // console.log(require('url').parse(req.url).path);
//     return "/api" + require('url').parse(req.url).path;
//   },
//   decorateRequest: function(proxyReq, originalReq) {
//     // console.log(originalReq.url);
//     var ip = (originalReq.headers['x-forwarded-for'] || '').split(',')[0] 
//                    || originalReq.connection.remoteAddress;

//     // you can update headers 
//     // proxyReq.url = "/api" + originalReq.url;
//     // if (originalReq.body) {
//     //   proxyReq.bodyContent = JSON.stringify(originalReq.body);
//     // }




//     proxyReq.headers['x-forwarded-for-client-ip'] = ip;
//     // you can change the method 
//     // proxyReq.method = 'GET';
//     // you can munge the bodyContent. 
//     // proxyReq.bodyContent = proxyReq.bodyContent.replace(/losing/, 'winning!');
//     return proxyReq;
//   }
// }));





app.use(cookieParser());
app.use(bodyParser());


app.use(session({
  secret: 'baogaossss',
  name: 'baogao',
  // store:  new RedisStore({
  // 	host: '127.0.0.1',
  // 	port: 6379
  // }),
  proxy: true,
  resave: true,
  saveUninitialized: true
}));
var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;




app.use(passport.initialize());
app.use(passport.session());


var clientId = "793272019863-d5k34k247iqdd5unc4krth0sk14ef8km.apps.googleusercontent.com";
var clientSecret = "BnuLxmkV-uwegj0raKLmIj5e";



passport.use(new GoogleStrategy({
  clientID: global.certification["google-client-id"],
  clientSecret: global.certification["google-client-secret"],
  callbackURL: global.certification["google-client-callbackURL"]
},
  function (accessToken, refreshToken, profile, done) {
    console.log("accessToken: " + accessToken);
    console.log("refreshToken: " + refreshToken);



    if (profile) {
      var user = profile;
      return done(null, user);
    }
    else {
      return done(null, false);
    }
    //  User.findOrCreate({ googleId: profile.id }, function (err, user) {
    //    return done(err, user);
    //  });
  }
));


passport.serializeUser(function (user, done) {
  console.log(user);
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});


app.get('/auth/google',
  passport.authenticate('google', {
    scope: ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/drive'],
    accessType: 'offline',
    prompt: 'consent'
  }));
// passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));


app.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/auth/google' }),
  function (req, res) {

    if (req.user) {
      var isAdmin = false;
      for (var i in req.user.emails) {
        console.log(req.user.emails[i].value);
        if (req.user.emails[i].value.split("@").pop() == "ciceron.me") {
          //console.log("jfghgfhgfhg");
          isAdmin = true;

          routeApi.registerAdmin(req);

          res.redirect(req.session.redirectTo);
          break;
        }

      }
      if (!isAdmin) res.redirect('/');

    }
    else {
      res.redirect('/');
    }

  });





// app.use('/api', function(req, res){
//   var ip = (req.headers['x-forwarded-for'] || '').split(',')[0] 
//               || req.connection.remoteAddress;
//   req.headers['x-forwarded-for-client-ip'] = ip;
//   // console.log('API REQUEST');
//   // console.log(ip);
//   proxy.web(req, res, {
//       target: global.api_url + "/api"
//   }); 
//   proxy.on('error', function(e) {
//       console.log(e);    
//   }); 
// })

// app.use('/', routes);



// var secure_router = express.Router();
// var ciceron_landing_router = express.Router();

// secure_router.get('/', function(req, res) {
//     res.send('Welcome to our API!');
// } );

// ciceron_landing_router.get('/', function(req, res) {
//     res.send('home!');
//     console.log(req);
// } );

app.all("/swagger*", global.adminCheck);
app.all("/api-docs*", global.adminCheck);



app.use(express.static(path.join(__dirname, 'public')));



app.use('/api2', function (req, res) {
  var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
    || req.connection.remoteAddress;
  req.headers['x-forwarded-for-client-ip'] = ip;
  // console.log('API REQUEST');
  // console.log(ip);
  proxy.web(req, res, {
    target: global.certification["flask-server"] + "/api2"
  });
  proxy.on('error', function (e) {
    console.log(e);
  });
});



app.use(function (req, res, next) {
  req.baogaoUser = {};
  request({
    url: global.certification["flask-server"] + '/api2/v2',
    headers: { "cookie": req.headers.cookie },
    rejectUnauthorized: false//,
    // qs: req.query
  }, function (err, res2, body) {
    console.log(req.protocol + '://' + req.get('host') + '/api2/v2');
    try {
      body = JSON.parse(body);
      req.baogaoUser = body;
      req.baogaoUser.school_email = body.school_domain;
      if(!req.baogaoUser.school_color){
        req.baogaoUser.school_color = "#168046";
      }
      console.log(body);
    } catch (error) {

    } finally {
      next();
    }
  });
});



app.use(routeWww);


// app.use(subdomain('secure', routeSecure));
// app.use(subdomain('www', routeWww));
app.use("/ko", routeWww);
app.use("/cn", routeWww);
app.use("/api", routeApi);
app.use("/api/v1", routeApi);

app.use('/api', function (req, res) {
  var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
    || req.connection.remoteAddress;
  req.headers['x-forwarded-for-client-ip'] = ip;
  // console.log('API REQUEST');
  // console.log(ip);
  proxy.web(req, res, {
    target: global.certification["topik-server"] + "/api"
  });
  proxy.on('error', function (e) {
    console.log(e);
  });
});

app.use(function (req, res, next) {
  var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
    || req.connection.remoteAddress;
  req.headers['x-forwarded-for-client-ip'] = ip;
  // console.log('API REQUEST');
  // console.log(ip);
  proxy.web(req, res, {
    target: global.certification['flask-server']
  });
  proxy.on('error', function (e) {
    console.log("error");
    console.log(e);
  });
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {

  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error.pug', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error.pug', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
