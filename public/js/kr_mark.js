var content_id = '';
var content = '';
var corrections;
var corrections_item;
var sentence_num_arr = [];
var result_arr = [];
var mark_length;
var new_help_index = 0;
var doubleSubmitFlag = false;

var loading = function (isStart) {
    if (isStart == false) {
        $(".global_loading").remove();
        // $("#floatingCirclesG").remove();
    } else {

        $('body').prepend('<div class="global_loading">' +
            '<div class="mask-loading">' +
            '<div class="spinner">' +
            '<div class="double-bounce1"></div>' +
            '<div class="double-bounce2"></div>' +
            '</div>' +
            '</div>' +
            '</div>'
        );
    }
}

var random_ad = function () {

    // console.log(ranNum);
    setInterval(function () {

        var ranNum = Math.floor(Math.random() * 3);
        $(".ad_section a").css("background-image", "url(/img/ciceron_ad" + ranNum + ".jpg)");
    }, 5000);
}

$(document).ready(function () {

    var helper = new Helper();
    helper.Init();

    // 의견 popup-open
    var targeted_popup = $(".kr_mark_popup");

    $("body").on("click", ".popup_btn", function (e) {
        e.preventDefault();

        targeted_popup.removeClass("invisible");
        targeted_popup.find("textarea").focus();
    });

    // popup-close
    $("body").on("click", ".popup-close", function (e) {
        e.preventDefault();
        // var targeted_popup = $(".popup");
        targeted_popup.addClass("invisible");
        targeted_popup.find("textarea").val("");

    });

    // popup-submit
    $("body").on("submit", ".form_send_mail", function (e) {
        e.preventDefault();

        helper.send_mail(this);
        $(".popup-close").click();

        return false;
    });

    // help_content msg 
    $("body").on("click", ".user_btn", function (e) {
        e.preventDefault();

        this_wrong_mark = $(this).attr("wrong_text_mark");

        var err_msg = '';
        $("#help_content" + this_wrong_mark + " ul>li").each(function () {
            var f_span = $(this).find("span:eq(0)").text();
            var s_span_elem = $(this).find(".modify_form");
            var s_span;

            if (s_span_elem.is("input, textarea")) {
                s_span = s_span_elem.val();
            }

            if (($(this).find("span")).length < 1) {
                return;
            }
            // console.log(s_span);
            err_msg += f_span + ": " + s_span + "\n";
        });

        err_msg += "\n\n의견: ";
        // console.log($("#help_content"+this_wrong_mark).text());
        targeted_popup.find("textarea").val(err_msg);

        $(".popup_btn").click();
        // helper.send_mail(this);

        return false;
    });

    // text counter
    // $("body").on("change", "#form_type", helper.setStanardScore);

    $("body").on("keyup", "#form_text", function (e) {

        e.preventDefault();

        var total = $(".total_score");
        var len = $(this).val().length;

        total.text(len);

        if (parseInt(total.text()) > 0) {

            $("#content button").addClass("red_btn");
        } else {
            $("#content button").removeClass("red_btn");
        }
    });

    // 중복 제출 방지
    var doubleSubmitCheck = function () {
        if (doubleSubmitFlag) {
            return doubleSubmitFlag;
        } else {
            doubleSubmitFlag = true;
            return false;
        }
    }

    // 채점하기 버튼
    $("body").on("click", "#content button", function (e) {

        e.preventDefault();
        // $(this).prop("disabled", true);
        var text_content = $.trim($("#form_text").val());
        // console.log(typeof(text_content));

        if (text_content === "") {
            alert("문장을 입력하세요.");
            $("#form_text").focus();
        } else {

            var formData = new FormData();
            // formData.append("question_type", 100);
            formData.append("content", text_content);
            // if (doubleSubmitCheck()) return;
            helper.spellcheck(formData);

        }

        return false;
    });

    // 2번째 페이지 form_text 영역 .red_font click시 help_content 찾기
    $("body").on("click", "#content2 #form_text .red_font, #content2 #form_text .blue_font", function (e) {

        e.preventDefault();

        var red_font_mark = $(this).attr("wrong_text_mark");
        var help_content_length = $(".help_content").length;

        $(".help_content [name='sentence_num']").each(function (k, elem) {
            var help_wrong_start = parseInt($("#help_content" + red_font_mark + " input[name='wrong_start']").val()) + 1;
            var help_wrong_end = parseInt($("#help_content" + red_font_mark + " input[name='wrong_end']").val());

            var this_helpContent_pos = $("#help_content" + red_font_mark).offset().top;
            var help_prevAll = $("#help_content" + red_font_mark).prevAll();
            var amount_outer_height = 0;

            help_prevAll.each(function (index, elem2) {
                amount_outer_height += $(elem2).outerHeight();
            });

            // console.log(amount_outer_height);
            // var this_helpContent_pos = $(this_help_content).outerHeight();
            var style2_pos = $("#tbox1").scrollTop();

            $("#tbox1").stop().animate({
                scrollTop: amount_outer_height - 200
            });

            // console.log("style: ",$(window).scrollTop());
            // console.log("amount: ",amount_outer_height);
        });

        // highlight event
        $("#help_content" + red_font_mark).addClass("highlight_div").delay(1500).queue(function (next) {
            $(this).removeClass("highlight_div");
            next();
        });
    });

    // 적용 버튼 이벤트
    $("body").on("click", ".replace_btn", function (e) {

        e.preventDefault();

        var this_btn_mark = $(this).attr("wrong_text_mark");

        if ($("i[wrong_text_mark='" + this_btn_mark + "']").text() != "") {
            alert("추천어가 없습니다. 직접 입력해주세요.");

            $("#form_text span[wrong_text_mark='" + this_btn_mark + "']").click();

            return;
        } else {
            $("i[wrong_text_mark='" + this_btn_mark + "']").remove();
        }

        this_text = $(this).prev("span").text();

        $("#form_text span[wrong_text_mark = '" + this_btn_mark + "']").text(this_text).removeClass("red_font").addClass("blue_font");
    });

    $("body").on("click", ".user_text_btn", function (e) {
        e.preventDefault();

        var this_btn_mark = $(this).attr("wrong_text_mark");

        this_text = $(this).prev("input[name='user_text']").val();

        $("#form_text span[wrong_text_mark = '" + this_btn_mark + "']").text(this_text).removeClass("red_font").addClass("blue_font");
    });

    // help_content input click select event
    $("body").on("click", ".modify_form", function (e) {
        e.preventDefault();

        $(this).select();
    });

    $("body").on("click", ".copy_all_btn", function (e) {

        e.preventDefault();

        copy_form_text("#content2 #form_text");
    });

    $("body").on("click", ".change_all_btn", function (e) {

        e.preventDefault();

        var q = confirm("모두 바꾸기를 진행하시면 틀린 단어들이 대체어로 일괄 변경됩니다.\n진행 후 반드시 의미를 확인하시기 바랍니다.");
        if (q) {

            if ($("#form_text .red_font").length < 1) {
                alert("더 이상 수정할 내용이 없습니다.");
            } else {

                $(".help_content").each(function () {
                    $(this).find(".replace_btn:eq(0)").click();
                });
            }

        }

        return false;
    });

    // 파일 다운로드 이벤트
    var textbox = $("#content2 #form_text");
    $("body").on("click", ".down_btn", function (e) {

        e.preventDefault();

        yyyymmdd = x => (f = x => (x < 10 && '0') + x, x.getFullYear() + f(x.getMonth() + 1) + f(x.getDate()));
        var date = new Date();

        downloadURI(makeTextFile(textbox.text()), yyyymmdd(date) + "_spellCheck");

        alert("파일이 다운로드 되었습니다.\n교열 페이지에서 첨부하여 주십시오.");

        return;
    });

    // 10초에 한 번씩 갱신
    $("body").on("submit", ".result_content", function (e) {
        e.preventDefault();

        corrections = [];
        $(".help_content").each(function (i, item) {
            corrections_item = {};
            $(item).find('input, textarea, select').each(function (j, item2) {
                // if($(item2).prop("name")=='wrong_start'){

                // }
                if ($(item2).attr("name") == 'wrong_start' || $(item2).attr("name") == 'wrong_end' || $(item2).attr("name") == 'sentence_id' || $(item2).attr("name") == 'subtract_point') {

                    corrections_item[$(item2).attr('name')] = parseInt($(item2).val());
                } else {

                    corrections_item[$(item2).attr('name')] = $.trim($(item2).val());
                }

            });
            corrections.push(corrections_item);
        });

        var data = {
            content_id: content_id,
            content: content,
            corrections: corrections
        }

        spellcheckstore(data);
    });

});


function Helper() {
    this.form_textarea = null;
}

// 초기화
Helper.prototype.Init = function () {
    this.form_textarea = $("#form_text");
    this.setStanardScore();
    random_ad();
}

// option별 기본점수 set
Helper.prototype.setStanardScore = function () {

    var total = $(".total_score");
    var form_textarea = $("#form_text");

    // form_textarea.val("");

    total.removeClass("red_font").text(0);
    form_textarea.keyup();
}

// 채점결과 페이지 보이기
Helper.prototype.showResultPage = function () {

    $("#content2_wrap").removeClass("invisible");
    $("#content").addClass("invisible");
}

// side 오답체크부분
var help_content_maker = function (key, val, style_parent, is_append) {
    // console.log(val);

    // (val.wrong_str).replace(/&nbsp;/g, " ");
    // (val.replace_str).replace(/&nbsp;/g, " ");

    var html = "<div class='help_content' id='help_content" + key + "'>" +
        "<ul>" +
        "<li>" +
        "<span>입력내용</span>" +
        "<span>" +
        "<span>" + val.wrong_str + "</span>" +
        "<input type='text' name='wrong_str' value='" + val.wrong_str + "' class='invisible modify_form'>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<span>대체어</span>" +
        "<span class='replace_wrap'>" +
        // 여기;
        "<input type='text' name='replace_str' value='" + val.replace_str + "' class='invisible modify_form' placeholder='추천어 없음'>" +
        // "<select class = 'invisible modify_form' name = 'replace_str'>" +
        // "</select>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<span>직접입력</span>" +
        "<span>" +
        "<input type='text' name='user_text' class='modify_form' style='width:80%;'>" +
        "<button type='button' class='user_text_btn' wrong_text_mark='" + key + "'>적용</button>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<span>도움말</span>" +
        "<span>" +
        "<span class='help_text_wrap'>" + val.help_text + "</span>" +
        "<textarea class='invisible modify_form' name='help_text' rows='6'>" + val.help_text + "</textarea>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<button class='user_btn' type='button' wrong_text_mark='" + key + "'>의견 보내기" +
        "</button>" +
        "</li>" +
        "</ul>" +
        "<input type='hidden' name='sentence_num' value='" + val.sentence_num + "'>" +
        "<input type='hidden' name='wrong_start' value='" + parseInt(val.wrong_start) + "'>" +
        "<input type='hidden' name='wrong_end' value='" + parseInt(val.wrong_end) + "'>" +
        "</div>";

    if (is_append) {

        $(style_parent + " .style2").append(html);
    } else {
        $(style_parent + " .style2").prepend(html);
    }


    var replace_str_type = '';

    if (typeof (val.replace_str) == "string") {

        replace_str_type += "<span>" + val.replace_str +
            "<i class='no_replace' wrong_text_mark='" + key + "'>" + val.no_replace +
            "</i>" +
            "</span>" +
            "<button type='button' wrong_text_mark='" + key + "' class='replace_btn'>적용</button>";

    } else {
        for (var e = 0; e < (val.replace_str).length; e++) {
            replace_str_type += "<span>" + (val.replace_str)[e] +
                "<i class='no_replace' wrong_text_mark='" + key + "'>" + val.no_replace +
                "</i>" +
                "</span>" +
                "<button type='button' wrong_text_mark='" + key + "' class='replace_btn'>적용</button><br>";
        }
    }

    $("#help_content" + key + " .replace_wrap").append(replace_str_type);

    $(style_parent + " .style2").html($(style_parent + " .help_content").sort(function (a, b) {

        return $(a).find('input[name=sentence_num]').val() == $(b).find('input[name=sentence_num]').val() ? $(a).find('input[name=wrong_start]').val() - $(b).find('input[name=wrong_start]').val() : $(a).find('input[name=sentence_num]').val() - $(b).find('input[name=sentence_num]').val();
    }));
};

// back_arrow event
var back_arrow_evt = function () {
    if (!$("#content2_wrap").hasClass("invisible") && $(".content3_inner_wrap").hasClass("invisible")) {
        location.href = '/kr_mark';
    } else if ($(".content2_inner_wrap").hasClass("invisible")) {
        $(".content3_inner_wrap").addClass("invisible");
        $(".content2_inner_wrap").removeClass("invisible");

        $(".content3_inner_wrap .style2, .content3_table .content3 #form_text").empty();
    }
}

// form_textarea에 span 뿌리기
var append_span_func = function (append_parent, data) {
    // var form_textarea = append_parent;
    var charArray = [];
    var i = 0;

    for (i = 0; i < (data.content).length; i++) {
        // console.log(data.content[i]);
        var a = data.content[i].replace(/\n/g, "<br>");
        charArray.push(a.split(""));
    }

    $.each(data.corrections, function (k, val) {
        charArray[val.sentence_num][parseInt(val.wrong_start)] = "<span class='red_font' wrong_text_mark='" + k + "'>" + charArray[val.sentence_num][parseInt(val.wrong_start)];
        charArray[val.sentence_num][parseInt(val.wrong_end) - 1] = charArray[val.sentence_num][parseInt(val.wrong_end) - 1] + "</span>";
        // form_textarea.append("<span sentence_num='" + k + "'>"+val+"</span>");

        /*
        var split_sentence = val.split("");

        for (var i = 0; i < split_sentence.length; i++) {
            split_sentence[i] = split_sentence[i] == ' ' ? "&nbsp;" : split_sentence[i];
            $("span[sentence_num='" + k + "']").append("<span sentence_index='" + i + "'>" + split_sentence[i] + "</span>");
        }
        */
    });

    // console.log(charArray);
    
    for (i = 0; i < charArray.length; i++) {
        
        append_parent.append("<span sentence_num='" + i + "'>" + charArray[i].join("") + " </span>");
    };

};

// 틀린 위치 addClass = "red_font"
Helper.prototype.wrong_str_addClass = function (key, elem, is_wrong_end) {

    var start_num;
    var end_num;

    if (is_wrong_end) {
        start_num = key.wrong_start;
        end_num = key.wrong_end;
    } else {
        start_num = parseInt(key.revise_start);
        end_num = key.revise_end;
    }

    if (key.revise_start != null && key.revise_end == null) {
        key.revise_end = key.revise_start;

        // console.log(key.revise_end);
    }

    for (var j = parseInt(start_num); j < parseInt(end_num) + 1; j++) {
        // console.log(i);
        elem.eq(j).addClass("red_font");
    }

    $("#form_text span[sentence_num='" + key.sentence_num + "'] .red_font").wrapAll("<span class='wrong_word' />");

}
// 취소 버튼
var cancel_all = function () {
    var cancel_chk = confirm("맞춤법검사를 취소하면 편집모드로 돌아갑니다.\n적용한 내용은 유지/저장되지 않습니다. \n취소하시겠습니까?");

    if (cancel_chk) {
        $(document).scrollTop(0);
        // location.reload();

        $("#content2_wrap").addClass("invisible");

        $("#content2 #form_text, .style2").empty();
        $("#content").removeClass("invisible");
        $("#content #form_text").focus();

        new_help_index = 0;
        doubleSubmitFlag = false;

    } else {
        return false;
    }
}

// 전체 복사
var copy_form_text = function (elem) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(elem).text()).select();
    document.execCommand("copy");

    alert("입력하신 내용이 복사되었습니다.\n\nCtrl + v 키를 사용하여, 붙여 넣기를 사용하실 수 있습니다.");
    $temp.remove();
}

// txt 파일 다운
var textFile = null,
    makeTextFile = function (text) {
        var data = new Blob([text], {
            type: 'text/plain'
        });

        // If we are replacing a previously generated file we need to
        // manually revoke the object URL to avoid memory leaks.
        if (textFile !== null) {
            window.URL.revokeObjectURL(textFile);
        }

        textFile = window.URL.createObjectURL(data);

        // console.log(textFile);
        return textFile;
    };

function downloadURI(uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}


// spellcheck api 연결

var xhrSpell;

Helper.prototype.spellcheck = function (val) {

    xhrSpell && xhrSpell.abort();
    xhrSpell = $.ajax({
        // url: "/api/v1/topik/mark",
        url: "/api/v1/spellcheck/",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        beforeSend: function () {
            loading(true);
        },
        data: val
    }).done(function (data) {

        content_id = data.content_id;
        content = data.content;

        $.each(data.corrections, function (x, y) {

            y.no_replace = "";

            if (y.replace_str == '추천어 없음') {
                y.replace_str = "";
                y.no_replace += "추천어 없음";
            }
            // else {
            //     y.no_replace = "";
            // }
        });

        mark_length = data.corrections.length;

        // quiz type 결과페이지 넘기기
        Helper.prototype.showResultPage();
        $(".quiz_type_val").attr("value", 100);

        var form_textarea = $("#content2 #form_text");

        // 문장 1개씩 나눠서 span 처리
        append_span_func(form_textarea, data);
        // console.log(data.content);

        // help content 채우기
        if (data.corrections.length <= 0) {
            $(".style2").text("오류가 없습니다.");
        } else {

            $.each(data.corrections, function (key, val) {

                help_content_maker(new_help_index, val, ".sidebar2");
                var help_text_split = (val.help_text).replace(/\n/g, "<br>");
                $("#help_content" + new_help_index + " textarea").prev("span").html(help_text_split);

                new_help_index++;
                var sentence_num_text = $.trim($("#form_text span[sentence_num='" + val.sentence_num + "']").text());
                var sentence_num_span = $("#form_text span[sentence_num='" + val.sentence_num + "'] span");

                // Helper.prototype.wrong_str_addClass(val, sentence_num_span, true);
                // console.log(sentence_num_text);
                // console.log(sentence_num_span);

            });
        };

        var content_form_heigth = $("#content2 .myform2").outerHeight(true);
        $("#sidebar #tbox1").css("max-height", content_form_heigth);

        // console.log(data);

        submit_store(".result_content"); // 10초 단위 갱신

    }).fail(function (err) {

        // console.log(err);
        alert(err);
    }).always(function () {
        loading(false);

    });
}

var submit_store = function (elem) {
    setInterval(function () {
        $(elem).submit();
    }, 10000);
};

// 실시간 업데이트 보낼 데이터 api
var spellcheckstore = function (val) {

    var json_val = JSON.stringify(val);

    $.ajax({
        // url: "/api/v1/topik/mark",
        url: "/api/v1/spellcheck/store",
        type: "post",
        // cache: false,
        dataType: "json",
        contentType: false,
        processData: false,
        data: json_val
    }).done(function (data) {

        // console.log(data);

    }).fail(function (request, status, error) {
        // alert("에러!!");

        console.log("code: " + request.status + "\n" + "msg: " + request.responseText + "\n" + "error: " + error);
    });

}


// topik mail send
Helper.prototype.send_mail = function (msg) {
    var formData = new FormData($(msg)[0]);
    // formData.append('name','test');

    $.ajax({
        url: "/user/receiveBaogaoMail",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        beforeSend: function () {
            loading(true);
        },
        data: formData
    }).done(function (data) {

        alert("전송 성공");
        // console.log(data);
    }).fail(function (err) {

        console.log(err)
    }).always(function () {
        loading(false);

    });
}