$(document).ready(function () {
    // console.log(location.queryString);
    var lecture_comment;

    // lecture_id 받아오기
    $.ajax({
        url: "/api2/v2/school/" + location.queryString["school_id"] + "/lecture/" + location.queryString["lecture_id"],
        type: "get",
        contentType: false,
        processData: false
    }).done(function (data) {
        // console.log(data.data.length);

        var total = 0;
        // var rate_text = ["", "최악", "별로", "그냥그래", "좋아", "최고"];
        var rate_text = ["", "糟透了", "不咋的", "一般般", "好", "强推"];

        if (data.data.length == 0) {

            $(".lecture_wrap .lecture_name").html(data.common.lecture_name);
            $(".tutor_name").html(data.common.tutor_name);
            // $(".lecture_view_text .star-ratings-css").remove();
            // $(".lecture_view_text .star_point").remove();
            // $(".star_wrap").html("<span class='star-ratings-css' title='0'></span><span class='star_point'>0 / 5</span></div>");

            $(".comment_wrap").html("<li class='comments no-comments'>尚且没有这门课的评价<br>您想成为第一个评价的人吗？</li>");

        } else {

            $.each(data.data, function (key, val) {
                // var RegExp = /,|，|。/
                // var bold_text = val.comment.split(RegExp);
                // console.log(bold_text[0]);
                function user_email(strCar) {
                    if (strCar == undefined || strCar === '') {
                        return '';
                    }
                    var pattern = /.{3}$/; // 정규식
                    return strCar.replace(pattern, "***");
                }
                var email_info = userInfomation.email.split("@");
                // console.log(user_email(email_info[0]));

                lecture_comment = "<li class='comments' value='" + val.article_id + "'>"
                    + (val.is_yours ? "<div class='modify'>"
                        + "<i class='fa fa-pencil-square-o btnModify' aria-hidden='true' value='" + val.article_id + "'></i>"
                        + "<i class='fa fa-trash-o btnDelete' aria-hidden='true' value='" + val.article_id + "'></i>"
                        + "</div>" : "")
                    + "<div class='text'>"
                    + "<span class='bold_text'>" + rate_text[parseInt(val.rate)] + "</span>"
                    + "<div class='text_info'>"
                    + "<div class='star_wrap'>"
                    + "<span class='star-ratings-css' title='" + val.rate + "'></span>"
                    + "<span class='star_point'>" + val.rate + " / 5</span>"
                    + "</div>"
                    + "<span class='writer'>&nbsp;" + val.masked_email + "</span>"
                    + "</div>"
                    + "<span>" + val.comment + "</span>"
                    + "</div>"
                    + (val.is_yours ? "<form class='modify_frm' value='" + val.article_id + "'><ul><li class='drag_star'><ul class='stars'><li class='star' title='1'><i class='fa fa-star fa-fw'></i></li><li class='star' title='2'><i class='fa fa-star fa-fw'></i></li><li class='star' title='3'><i class='fa fa-star fa-fw'></i></li><li class='star' title='4'><i class='fa fa-star fa-fw'></i></li><li class='star' title='5'><i class='fa fa-star fa-fw'></i></li><li class='star_point'>" + val.rate + "</li><input type='hidden' name='rate' id='rate' value='" + val.rate + "'></ul></li>"+
                    "<li class='semester'><label>수강년도</label><select name='year' required><option value=''>선택해주세요.</option><option value='2017'>2017</option><option value='2016'>2016</option><option value='2015'>2015</option><option value='2014'>2014</option><option value='2013'>2014 이전</option></select></li>"
                    + "<li class='semester'><label>수강학기</label><select name='semester' required><option value=''>선택해주세요.</option><option value='1'>1학기</option><option value='2'>2학기</option><option value='S'>여름계절학기</option><option value='W'>겨울계절학기</option></select></li>"
                    + "<li class='input_textarea'><textarea type='text' name='comment' required>" + val.comment + "</textarea></li><li><button class='comment_btn' type='submit'>comment</button></li></ul></form>" : "") + "</li>";

                // console.log(val);
                $(".comment_wrap").append(lecture_comment);
                total += parseInt(val.rate);
            });

            $(".lecture_wrap .lecture_name").html(data.common.lecture_name);
            $(".tutor_name").html(data.common.tutor_name);
        }

        $(".drag_star").mouseleave();

        var modify_val = $(".comments").find(".modify").attr("value");

        // console.log(data.data);

        var Ave_star = parseInt(Math.round(total / data.data.length));
        if (isNaN(Ave_star)) {
            Ave_star = 0;
        }
        // var Ave_point = '<span class="star-ratings-css" title="' + Ave_star + '"></span><span class="star_point">' + Ave_star + ' / 5</span>'
        // $(".lecture_view_text > .star_wrap").append(Ave_point);
        $(".lecture_view_text > .star_wrap .star-ratings-css").attr("title", Ave_star);
        $(".lecture_view_text > .star_wrap .star_point").text(Ave_star + " /  5");

        $(".total-comment").text(data.data.length);

        var search_text;
        var search_lecture = $(".lecture_name").text().substring(0, 2);
        var search_tuter = $(".tutor_name").text();


        $.ajax({
            url: "/api2/v2/school/" + location.queryString["school_id"] + "/lectureFind?keyword=" + search_lecture,
            type: "get",
            contentType: false,
            processData: false,
            beforeSend: function () {
                loading(true);
            }
        }).done(function (data) {

            // console.log(data);
            $.each(data.data, function (key, val) {
                lecture_id = val.lecture_id;
                if (val.article_id == null) {
                    val.rate = 0;
                }
                var lecture_li = '<li class="div4 lecture"><div class="padding_div"><a href="/lecture/view?lecture_id=' + val.lecture_id + '&school_id=' + val.school_id + '"><div class="school_title" style="background:' + val.school_color + '" value="' + val.school_id + '">' + val.school_name + '</div><div class="lecture_content"><span class="lecture_title" value="' + val.lecture_id + '">' + val.lecture_name + '</span><span class="professor">&nbsp;-' + val.tutor_name + '</span><div class="star_wrap"><span class="star-ratings-css" title="' + val.rate + '"></span><span class="star_point">' + val.rate + '/ 5</span></div><div class="text">' + val.text + '</div></div></a></div></li>'

                $(".view_lecture_list").append(lecture_li);

                if (val.school_name == null) {
                    $(this).parents(".lecture").remove();
                }

            });

            var slider = $('.view_lecture_list').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 2,
                responsive: [
                    {
                        breakpoint: 960,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 640,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

        }).fail(function (err) {
            console.log("실패");
        }).always(function () {
            loading(false);
        });

    }).fail(function (err) {
        console.log("실패")
    });


    
    // $("body").on("click", "select[name='semester']", function(){
    //     console.log($(this).val());
    // });


    // 강의평 추가
    var AddComment = function (e) {
        e.preventDefault();

        var minLength = 10;
        var text_count = $(this).find(".input_textarea textarea").val();
        var text_length = text_count.length;
        // return;
        if (text_length < minLength) {
            alert("10자 이상이어야합니다.");
            return;

        } else {
            var formData = new FormData($(this)[0]);
            /*formData.append("year", $(this).find("input[name='year']").val());
            formData.append("semester", $(this).find("select[name='semester']").val());
            formData.append("rate", $(this).find("#rate").attr("value"));
            formData.append("comment", $(this).find(".input_textarea textarea").val());*/

            $.ajax({
                url: "/api2/v2/school/" + location.queryString["school_id"] + "/lecture/" + location.queryString["lecture_id"],
                type: "post",
                contentType: false,
                processData: false,
                data: formData
            }).done(function (data) {
                console.log("성공");
                location.reload();
                // return false;
                // $("ul.comment_wrap").append();
            }).fail(function (err) {
                console.log("실패");
            });
        }


        /*console.log($("#rate").attr("value") +", "+ $("input[comment]").val());*/
    };

    // 강의평 수정
    var modifyComment = function (e) {
        e.preventDefault();
        var formData = new FormData();
        formData.append("rate", $(this).find("#rate").attr("value"));
        formData.append("text", $(this).find(".input_textarea textarea").val());

        $.ajax({
            url: "/api2/v2/school/" + location.queryString["school_id"] + "/lecture/" + location.queryString["lecture_id"] + "/article/" + $(this).attr("value"),
            type: "PUT",
            contentType: false,
            processData: false,
            data: formData
        }).done(function (data) {
            console.log("성공");
            location.reload();
            // return false;
            // $("ul.comment_wrap").append();
        }).fail(function (err) {
            console.log("실패");
        });

        /*console.log($("#rate").attr("value") +", "+ $("input[comment]").val());*/
    };

    $("body").on("submit", ".comment_submit", AddComment);
    $("body").on("submit", ".modify_frm", modifyComment);

    // 강의평 삭제
    $("body").on("click", ".btnDelete", function () {
        var article_id = $(this).attr("value");
        // var r = confirm("삭제하시겠습니까?");
        var r = confirm("确认删除？");
        if (r == true) {
            $.ajax({
                url: "/api2/v2/school/" + location.queryString["school_id"] + "/lecture/" + location.queryString["lecture_id"] + "/article/" + article_id,
                type: "DELETE",
                processData: false,
                contentType: false
            }).done(function (data) {
                console.log("성공");
                // $(this).parents("li.comments").remove();
                location.reload();
            }).fail(function (err) {
                console.log("실패");
            });
        } else {
            // console.log(r);
        }
    });

    // 강의평 수정
    $("body").on("click", ".btnModify", function () {
        // $(".comment_btn").text("modify");
        $(".modify_frm[value=" + $(this).attr("value") + "]").toggleClass("selected");
        $(".modify_frm textarea").select();
    });


    $("body").on("mouseover", ".stars li", function () {
        var onStar = parseInt($(this).attr("title"), 10);

        $(this).parent().children("li.star").each(function (e) {
            // console.log(onStar);
            if (e < onStar) {
                $(this).addClass("selected");
            }
            else {
                $(this).removeClass("selected");
            }
        });

    });
    $("body").on("mouseleave", ".drag_star", function () {

        var index = $(".drag_star").index($(this));

        var onStar = parseInt($(".drag_star:eq(" + index + ") .star_point").text(), 10);
        $(".drag_star:eq(" + index + ") li.star").removeClass("selected");
        for (i = 0; i < onStar; i++) {
            $(".drag_star:eq(" + index + ") li.star:eq(" + i + ")").addClass("selected");
        }
    });


    $("body").on("click", ".stars li", function () {
        var index = $(".drag_star").index($(this).parents(".drag_star"));
        console.log(index);
        var onStar = parseInt($(this).attr("title"), 10);
        var stars = $(this).parent().children("li.star");

        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass("selected");
        }

        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass("selected");
        }

        $(".drag_star:eq(" + index + ") .star_point").text(onStar);
        $(".drag_star:eq(" + index + ") #rate").val(onStar);

        /*console.log($("#rate").attr("value") +", "+$(".input_textarea textarea").val());*/

    });

});