$(document).ready(function () {

	if ($.cookie("pop-day") != "Y") {
		// 견적 비용 변경 안내
		// popup("change_price");

		// 사용안내
		// popup("howToUse");

		// 이벤트 당첨자 리스트
		// popup("event_list");	


		// $("body").css("overflow","hidden");	
	}

	if ($.cookie("alert-day") == "Y") {
		$(".alertWrap").hide();
	};

	$("body").on("click", ".intro-logo img", function () {
		location.href = "/";
	});

	$("body").on("click", "#gotoSchool", function (e) {
		e.preventDefault();
		var targetOffset = $(".school2").offset().top - 100;
		$('html,body').stop().animate({ scrollTop: targetOffset }, 500);
		$(".school2").effect("highlight", {}, 850);
	});

	$("body").on("click", "a", function (e) {
		if ($(this).attr("href").substr(0, 1) == "#") {
			e.preventDefault();
			var targetOffset = $($(this).attr("href")).offset().top - 100;
			$('html,body').stop().animate({ scrollTop: targetOffset }, 500);
		}
	});

	$("body").on("click", "li.menu2, .gotoSchool", function (e) {
		e.preventDefault();
		$("#gotoSchool").click();
	});	
	
	// new_about scroll event
	$(document).scroll(function(){
		$(".progress").each(function(){
			if($(this).offset().top-$(document).scrollTop() <= 537){
				$(this).stop().fadeTo(0, 100).addClass('scroll_ani');
				// $(this).stop().fadeTo(0, 100);
			} else {
				// $(this).stop().fadeTo('fast',0).removeClass('scroll_ani');
				// $(this).stop().fadeTo('fast',0);
			}

			if($(this).hasClass('scroll_ani')){
				return;
			}
		});
	});

	// aside_menu scroll event
	$(window).scroll(function () {
		var height = $(document).scrollTop();
		if ($(".hide_aside").hasClass("invisible"))
			log(height);
	});

	function log(height) {
		if (height >= 300) {
			// console.log(height);
			$(".aside_menu").removeClass("invisible");
		} else {
			$(".aside_menu").addClass("invisible");
			$(".hide_aside").addClass("invisible");
		}
	}

	$(document).scroll();

	$("body").on("click", ".request_btn", function () {
		location.href = "/request";
	});
	$("body").on("click", ".black_btn", function () {
		location.href = "/black";
	});

	$("body").on("click", ".aside_menu .x-icon, .hide_aside", function () {
		$(".aside_menu").toggleClass("invisible");
		$(".hide_aside").toggleClass("invisible");
	});


	// alert 창
	$("body").on("click", "#alert-day", function () {
		// if ($("#alert-day").is(":checked")) {
		$.cookie("alert-day", "Y", {
			expires: 1
		});
		$(".alertWrap").css("height", "0%");
		// } else {
		// 	$(this).css("height", "0%");
		// }
	});

	// 팝업 창 닫기
	$("body").on("click", ".popup-button", function (e) {
		e.preventDefault();

		if ($("#pop-day").is(":checked")) {
			// 쿠키값을 "Y"로 하여 하루 저장
			$.cookie("pop-day", "Y", {
				expires: 1
			});
		}
		$(".popup").remove();
		// $("body").css("overflow","visible");			
	});

	$("body").on("click", ".close", function (e) {
		e.preventDefault();
		$(".popup-button").click();
		// $("body").css("overflow","visible");
	});

	// about us alert
	/*$("body").on("click", ".section_us a", function(){
		alert("준비 중입니다.");
	});
	
	$("body").on("click", ".section_writer a", function(){
		alert("준비 중입니다.");
	});*/

	
	/*var sub_menu = "<i class='ion-chevron-down'></i>"
	if($(".header-menu").find("ul>ul>a")){
		console.log($(this).text());
	}*/

	$.ajax({
		url: "/api2/v2/recommendedLecture",
		type: "get",
		contentType: false,
		processData: false
	}).done(function (data) {

		if(data.data.length <= 0) {
			var update_alert = '<div class="update_alert_wrap"><div class="alert_text">아직 강의평을 남길 수업이 없습니다. <br/><br/>업데이트 후 강의를 공유해주세요!</div></div>'
			$(".recommendedLecture_list").append(update_alert);
			
		} else {

			$.each(data.data, function (key, val) {
				var newLectureComments = '<div onclick="location.href=\'/lecture/view?lecture_id=' + val.lecture_id + '&school_id=' + val.school_id + '\'" class="lecture div4" value = "' + val.school_id + '" style="border-bottom-color:' + val.school_color + '">'
					+ '<div class="lecture_content">'
					+ '<h3 value = "' + val.lecture_id + '">' + val.lecture_name + '</h3>'
					+ '<span class="tutor_name">&nbsp;- ' + val.tutor_name + '</span>'
					+ '<div class="star_wrap">'
					+ '<span class="star-ratings-css" title="' + val.rate + '"></span>'
					+ '<span class="star_point">' + val.rate + '/ 5</span>'
					+ '</div>'
					+ '<div class="text">' + val.comment + '</div>'
					+ '</div>'
					+ '<span class="arrow">▶</span>'
					+ '<span class="school_text">' + val.school_name + '</span>'
					+ '</div>'
	
				$(".recommendedLecture_list").append(newLectureComments);
			});

			var slider = $('.recommendedLecture_list').slick({
				// dots: true,
				/*customPaging: function (slider, i) {
					var thumb = $(slider.$slides[i]).data();
					return '<a>' + (i + 1) + '</a>';
				},*/
				infinite: true,
				slidesToShow: 3,
				slidesToScroll: 2,
				responsive: [
					{
						breakpoint: 960,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					},
					{
						breakpoint: 640,
						settings: {
							slidesToShow: 1,
							slidesToScroll: 1
						}
					}
				]
			});
		}


	}).fail(function (err) {
		console.log(err);
	});
	/*var school_text = $(".school_text");

	$("body").on("mousemove", ".lecture", function (e) {
		x = e.clientX;
		y = e.clientY;

		school_text.css({"top":y+20, "left":x+20});
	});*/
});

