$(document).ready(function () {
	
	location.queryString = {};
	location.search.substr(1).split("&").forEach(function (pair) {
		if (pair === "") return;
		var parts = pair.split("=");
		location.queryString[parts[0]] = parts[1] &&
			decodeURIComponent(parts[1].replace(/\+/g, " "));
	});
	
	$("a[href='" + location.pathname + "']").addClass("selected");

	$('body').on('click', '.popup', function (e) {
		if ($(e.target).hasClass('popup')) {
			$(".popup-button").click();
			$(this).remove();
		}
	});

	$('body').on('click', '.popup-next', function () {
		$(".step-now").addClass("step-left").removeClass("step-now");
		$($(".step-right").first()).addClass("step-now").removeClass("step-right");
	});
	$('body').on('click', '.popup-prev', function () {
		$(".step-now").addClass("step-right").removeClass("step-now");
		$($(".step-left").last()).addClass("step-now").removeClass("step-left");
	});

	$("body").on("click", ".header-side", function () {
		if ($("body").hasClass("show-menu")) {
			$("body").removeClass("show-menu");
		}
		else {
			$("body").addClass("show-menu");
		}
	});

	// var lecture_write = '<div class="lecture_writeBtn"><button type="button" onclick="location.href=\'/lecture\'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></div>';

	// 강의평 쓰기  &gt; 
	// $("body").append(lecture_write);

	/* $("body").on("mouseover", ".lecture_writeBtn", function(){
		$(this).addClass("hover");
		$(this).find("button").html("강의평 쓰기&nbsp;&gt;&nbsp;");
	}).on("mouseleave", function(){
		$(this).removeClass("hover");
		$(this).find("button").html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>');
	}); */

	var touchForMenu = false;
	var startCoords = 0;
	var endCoords = 0;
	var preCoords = 0;

	$("body").on("touchstart", ".wrapper", function (e) {
		if ($(document).width() > 640) {
			touchForMenu = false;
			return;
		}
		startCoords = endCoords = preCoords = e.originalEvent.targetTouches[0].pageX;
		$(".header-menu").css("transition", "none");
		$(".wrapper").css("transition", "none");
		if ($("body").hasClass("show-menu")) {
			touchForMenu = true;
		}
		else {
			if (startCoords < 100) {
				touchForMenu = true;
			}
			else {
				touchForMenu = false;
			}
		}
	});

	$("body").on("touchmove", ".wrapper", function (e) {
		if ($(document).width() > 640) {
			touchForMenu = false;
			return;
		}
		if (!touchForMenu) {
			return;
		}
		endCoords = e.originalEvent.targetTouches[0].pageX;
		var x = $(".header-menu").position().left - preCoords + endCoords;
		if (x > 0) {
			x = 0;
		}
		if (x < -200) {
			x = -200;
		}
		$(".header-menu").css("left", x);
		x = x + $(".header-menu").width();
		if (x < 0) {
			x = 0;
		}
		$(".wrapper").css("left", x / 2);
		preCoords = e.originalEvent.targetTouches[0].pageX;
	});

	$("body").on("touchend", ".wrapper", function (e) {
		if ($(document).width() > 640) {
			touchForMenu = false;
			return;
		}
		$(".header-menu").attr("style", "");
		$(".wrapper").attr("style", "");
		var x = $(".header-menu").position().left;
		if (x > -100) {
			$("body").addClass("show-menu");
		}
		else {
			$("body").removeClass("show-menu");
		}
	});

	$("body").on("click", ".wrapper", function () {
		$("body").removeClass("show-menu");
	});

	/*
	$(document).on('scroll', function () {
		if (window.pageYOffset > 80) {
			$(".fixed_header").removeClass("invisible");
		}
		else {
			$(".fixed_header").addClass("invisible");
		}
	});
	*/
	$(document).scroll();
});

var is_popup_clicked = false;
var timer_popup;
var popup = function (filename, callback) {

	if (is_popup_clicked) return;
	var p = $('<div class="popup" id="popup_' + filename + '"></div>');

	p.load('/div/' + filename, function () {
		$('body').prepend(p);
		if (callback) callback();
	});
	is_popup_clicked = true;

	timer_popup = setTimeout(function () {
		is_popup_clicked = false;
		clearTimeout(timer_popup);
	}, 1000);
}

var loading = function (isStart) {
	if (isStart == false) {
		$(".global_loading").remove();
	}
	else {
		$('body').prepend('<div class="global_loading">'
			+ '<div class="mask-loading">'
			+ '<div class="spinner">'
			+ '<div class="double-bounce1"></div>'
			+ '<div class="double-bounce2"></div>'
			+ '</div>'
			+ '</div>'
			+ '</div>'
		);
	}
}

// QnA 메일 전송
function send_QnA(e) {
	e.preventDefault();
	var formData = new FormData($(this)[0]);

	$.ajax({
		url: "/api/v1/user/contact",
		type: "post",
		data: formData,
		contentType: false,
		processData: false
	}).done(function (data) {
		console.log("성공");
		// alert("메일이 성공적으로 전송되었습니다.");
		alert("您的邮件已传送成功");
		location.reload();
	}).fail(function (err) {
		console.log("실패");
	});
}

$("body").on("submit", ".footer_mail_form .QnA_mail", send_QnA);

