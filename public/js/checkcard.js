$(document).ready(function () {
	$(".iamport").submit(function (e) {
		e.preventDefault();
		if ($("#agree").prop('checked') != true) {
			alert("请接受条款。");
			return;
		}

		loading(true);
		var formData = new FormData($(this)[0]);

		$.ajax({
			url: '/api/user/pay/imp',
			data: formData,
			processData: false,
			contentType: false,
			dataType: "JSON",
			type: 'POST'
		}).done(function (data) {
			try {
				if (data.link) {
					location.href = data.link;
				}
				else {
					alert("支付失败。 请检查您再次输入的信息。");
				}
			}
			catch (err) {
				alert("支付失败。 请检查您再次输入的信息。");
			}
		}).fail(function (xhr, ajaxOptions, thrownError) {
			alert("支付失败。 请检查您再次输入的信息。");
		}).always(function () {
			loading(false);
		});
	})
});