$(document).ready(function () {
    var lecture_id;
    // console.log(userInfomation.school_id);

    $(".search_text").focus();

    $("body").on("click", ".intro-logo img, .form-header-logo img, .form-header-back", function () {
        location.href = "/";
    });

    $("body").on("click", ".lecture", function () {
		lecture_id = $(this).find(".lecture_title").attr("value");
		// console.log(lecture_id);
    });

    $("body").on("click",".lecture_writeBtn button", function(e){
        e.preventDefault();
        var targetOffset = $(".search_box").offset().top - 100;
        $("html, body").animate({scrollTop:targetOffset}, 300);
        $(".search_text").focus();
    });


    var login_check = function () {
        /*$.ajax({
            url: "/api2/v2",
            type: "get",
            contentType: false,
            processData: false
        }).done(function (data) {
            // console.log(data);*/
            if (userInfomation.logged_in == true) {
                // location.href="/api2/v2/school/"+userInfomation.school_id+"/lecture/"+lecture_id;
                location.href="lecture/view";
            } else {
                alert("로그인 후 열람 가능합니다.");
                // location.reload();
            }
        /*}).fail(function (err) {
            console.log(err);
        });*/
    };

    $("body").on("click", ".lecture", function () {
        login_check();
        var lecture_id = $(this).find(".lecture_title").attr("value");

    });

    $.ajax({
        url: "/api2/v2/newLectureComments",
        type: "get",
        contentType: false,
        processData: false
    }).done(function (data) {

        // console.log(data);
        $.each(data.data, function (key, val) {
            lecture_id = val.lecture_id;
            var lecture_li = '<li class="div4 lecture"><div class="padding_div"><a href="/lecture/view?lecture_id='+val.lecture_id+'&school_id='+val.school_id+'"><div class="school_title" style="background:' + val.school_color + '" value="' + val.school_id + '">' + val.school_name + '</div><div class="lecture_content"><span class="lecture_title" value="' + val.lecture_id + '">' + val.lecture_name + '</span><span class="professor">&nbsp;-' + val.tutor_name + '</span><div class="star_wrap"><span class="star-ratings-css" title="' + val.rate + '"></span><span class="star_point">' + val.rate + '/ 5</span></div><div class="text">' + val.text + '</div></div></a></div></li>'

            if (val.school_name == null) {
                $(this).parents(".lecture").remove();
            } else {
                $(".lecture_list").append(lecture_li);
            };

        });

        var slider = $('.lecture_list').slick({
            dots: false,
            /*customPaging: function (slider, i) {
                var thumb = $(slider.$slides[i]).data();
                return '<a>' + (i + 1) + '</a>';
            },*/
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 2,
            rows: 2,
            responsive: [
                {
                  breakpoint: 960,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                  }
                },
                {
                  breakpoint: 640,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                  }
                }
              ]
        });

        // hide button
        /*$(".slick-prev").hide();
        slider.on("afterChange", function (e, slick, currentSlide) {
            // console.log(slick.slideCount);
            if (currentSlide === 0) {
                $(".slick-prev").hide();
                $(".slick-next").show();
            } else {
                $(".slick-prev").show();
            }

            if (slick.slideCount === currentSlide + 3) {
                $(".slick-next").hide();
            }
        });*/


    }).fail(function (err) {
        console.log("실패");
    });

});