$(document).ready(function(){

	$("#file_share-link").val(location.href);

	$("body").on('click', '.file_download-close, .file_share-close, .wrap', function(){
		if($(".wrap").hasClass("menu") || $(".wrap").hasClass("share")){
			$(".wrap").removeClass("menu");
			$(".wrap").removeClass("share");
			// $("body").css("overflow-y", "auto");
		}
	});
	$("body").on('click', '.btnDownload', function(){
		
		setTimeout(function() {
			$(".wrap").addClass("menu");
			// $("body").css("overflow-y", "hidden");
		}, 10);
		
	});

	$("body").on('click', '.btnShare', function(){

		setTimeout(function() {
			$(".wrap").addClass("share");
			// $("body").css("overflow-y", "hidden");
		}, 10);
		
		$("#file_share-link").select();
	});

	$("body").on('click', '#file_share-link', function(){
		$(this).select();
	});

	$("body").on('click', '#file_share-copy', function(){
		if(copyToClipboard($('#file_share-link')[0])){
			$(this).addClass('success');
			setTimeout(function() {
				$('#file_share-copy').removeClass('success');
			}, 1000);
		}
		else{
			alert("");
		}
	});

    $("#frmDownload").submit(function(e){
        e.preventDefault();

        var form = $(this)[0];
        var formData = new FormData(form);

        $.ajax({
            url: '/api/v2/user/pretranslated/project/'+ project_id +'/resource/'+ language_id +'/request',
            processData: false,
            contentType: false,
            data: formData,
            cache : false,
            // dataType: "JSON",
            type: 'POST'
        }).done(function(data){
            console.log(data);
            alert("성공");
            location.reload();
        }).fail(function(xhr, ajaxOptions, thrownErr7or){
            // if (xhr.status == 417) {
            //     alert("올바르지 않은 이메일 주소입니다.");
            // }
            // else if(xhr.status == 412){
            //     alert("이미 존재하는 이메일 주소입니다.");
            // }
            alert("Error!\n" + JSON.parse(xhr.responseText).message);
        }).always(function(data){
            $.ajax({
                url: '/api/v2/user/pretranslated/project/'+ project_id +'/resource/'+ language_id +'/sendMail',
                processData: false,
                contentType: false,
                data: formData,
                cache : false,
                // dataType: "JSON",
                type: 'POST'
            })
        });

    });
});

function copyToClipboard(elem) {
	  // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    // copy the selection
    var succeed;
    try {
    	  succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}