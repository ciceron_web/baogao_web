$(document).ready(function () {
	var check_email, password, school_id, major, name, enter_year, phone, wechat_id, referred_by, birth;

	var regExp_mail = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; // mail check
	var regExp_phone = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/; // phone check
	var regExp_pass = /^[a-z0-9_]{4,20}$/; // pass check

	$("body").on("click", ".intro-logo img, .form-header-logo img, .form-header-back", function () {
		location.href = "/";
	});

	$("body").on("keyup", "#join-pass, #user_passwd_confirm", function () {
		if ($("#join-pass").val() == $("#user_passwd_confirm").val()) {
			$(".check_password").html("Matching").css("color", "green");
		} else {
			$(".check_password").html("Not Matching").css("color", "red");
		}
	});

	$("body").on("keyup", "#join-email", function () {
		// console.log($(this).val());
		if ($(this).val() == "") {
			$(".possible_mail").html("");
		}
	});

	$("body").on("click", ".check_email", function (e) {
		e.preventDefault();
		check_email = $("#join-email").val();

		if (check_email == "") {
			// alert("이메일을 확인해주세요.");
			alert("请确认您的邮箱");
		} else if (!regExp_mail.test($("#join-email").val())) {
			// console.log("메일 아냐");
			$(".possible_mail").html("");
			// alert("이메일 형식이 맞지 않습니다.");
			alert("您输入的邮箱形式有误");
			return false;
		} else {
			$.ajax({
				url: "/api2/v2/idUniqueCheck?email=" + check_email,
				type: "get",
				contentType: false,
				processData: false,
			}).done(function (data) {
				// console.log(data);
				// alert("사용할 수 있습니다.");
				// $(".possible_mail").html("사용 가능한 이메일입니다.");
				$(".possible_mail").html("邮箱可用");
			}).fail(function (err) {
				console.log(err);
				// alert("사용 중인 이메일입니다.");
				alert("邮箱已注册过用户，请直接登录");

			});

		}

	});


	function checkPassword(email, password) {
		password = $("#join-pass").val();
		var checkNumber = password.search(/[0-9]/g);
		var checkEnglish = password.search(/[a-z]/ig);
		var checkSpe = password.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

		/*
		if (!/^[a-zA-Z0-9]{4,16}$/.test(password)) {
			// alert('숫자와 영문자 조합으로 4~16자리를 사용해야 합니다.');
			alert('请使用数字和英文组合的4~16位字符');
			return false;
		};
		*/

		if (checkNumber < 0 || checkEnglish < 0 || checkSpe < 0) {
			alert("영문, 숫자, 특수문자를 혼합하여 입력해주세요.");
			// alert("请混合使用数字和英文字母");
			return false;
		};

		if (/(\w)\1\1\1/.test(password)) {
			// alert('같은 문자를 4번 이상 사용하실 수 없습니다.');
			alert('同样的字符不可输入4次');
			return false;
		};

		if (password.search(/\s/) != -1) {
			alert('비밀번호는 공백 없이 입력해주세요.');
			return false;
		};

		/*if (password.search(email) > -1) {
			alert("비밀번호에 아이디가 포함되었습니다.");
			return false;
		}*/
		return true;
	}


	$("#joinForm").on("submit", function (e) {
		// alert("jjjj");

		e.preventDefault();

		var formData = new FormData($(this)[0]);
		formData.append("phone", $("#mobile1").val() + $("#mobile2").val() + $("#mobile3").val());

		formData.append("password", $.sha256($("#join-pass").val()));
		// console.log($(this).val());
		check_email = $("#join-email").val();
		if (checkPassword(check_email, password)) {
			console.log($("#agree_service_check0")[0].checked);
			if ($("#agree_service_check0")[0].checked != true) {
				// alert("개인정보취급방침 및 이용약관을 동의해주세요.");
				alert("同意‘个人信息获取协议’及‘服务使用协议’");
				return;
			} else {

				$.ajax({
					url: "/api2/v2/signUp",
					type: "POST",
					dataType: "text",
					contentType: false,
					processData: false,
					data: formData
				}).done(function (data) {
					console.log("성공");
					// alert("가입되었습니다. 환영합니다~");
					alert("恭喜您成功申请会员~");
					location.href = "/";
				}).fail(function (err) {
					console.log("실패");
				});
			}
		} else {
			return;
		}

	});

	// console.log($("#joinForm"));

});