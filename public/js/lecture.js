var isFirstTime = true;

$(document).ready(function () {
	location.queryString = {};
	location.search.substr(1).split("&").forEach(function (pair) {
		if (pair === "") return;
		var parts = pair.split("=");
		location.queryString[parts[0]] = parts[1] &&
			decodeURIComponent(parts[1].replace(/\+/g, " "));
	});
});

function autoResize(i) {

	$(i).height(0);
	setTimeout(function () {
		if (isFirstTime && location.queryString.search && userInfomation.logged_in) {
			$('iframe').contents().find('input[name="search"]').val(location.queryString.search);
			$('iframe').contents().find('input[name="search"]').parent("form").submit();
			isFirstTime = false;
			return;
		}
		var iframeHeight =
			(i).contentWindow.document.body.scrollHeight;
		$(i).height(iframeHeight + 20);
	}, 100);
}