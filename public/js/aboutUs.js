

var speed = "200";

$(document).ready(function () {
    // about us
    $("body").on("click", ".form-header-logo img, .form-header-back", function () {
        location.href = "/";
    });

    $("body").on("click", ".question", function(){
        $(this).stop().toggleClass("select");
        $(this).next().stop().slideToggle(speed).siblings(".answer").stop().slideUp();
        $(this).siblings(".question").removeClass("select");
        
    });
    
});