var requestedData = {};
var isAuthInProgress = false;
var token = "";

$(document).ready(function () {

	$("#form-request-subject").autocomplete({
		source: "/api/user/" + userInfomation.school_url + "/search_subject",
		minLength: 0,
		select: function (event, ui) {
			// log( "Selected: " + ui.item.value + " aka " + ui.item.id );
		},
		focus: function (e, ui) {
			console.log(this);
			return false;
		},
		open: function () {
			$(".ui-autocomplete").css("max-width", $("#form-request-subject").outerWidth());
		}
	});
	$(document).on("scroll", function (e) {
		$(".form-background").css("opacity", ($(window).height() - $(this).scrollTop() * 1) / $(window).height());
		$(".form-header-top span").css("opacity", ($(window).height() - $(this).scrollTop() * 2) / $(window).height());
		var top = $(this).scrollTop() * 0.6 - 50;
		// if (top > 500) {
		// 	top = 500;
		// }
		$(".form-background").css("top", top);

	});

	$("body").on("click", ".form-header-logo img, .form-header-back", function () {
		location.href = "/";
	});

	$("body").on("focus", ".form-request-inside input, .form-request-inside button, .form-request-inside textarea", function(e){
		if(!userInfomation.school_url){
			e.preventDefault();
			$(".form-request-inside").addClass("active");
			$(this).blur();
		}
	});

	$(".form-request-inside").on("click", function(e){
		if($(e.target).hasClass("form-request-inside") || $(e.target).hasClass("needSignup-close")){
			$(".form-request-inside").removeClass("active");
		}
	});

	$("body").on("focus", "#form-request-subject", function () {
		if(userInfomation.school_url){
			$(this).keydown();
		}
	});

	$("body").on("submit", "#form-request", function (e) {
		e.preventDefault();
		var formData = new FormData($(this)[0]);

		$.ajax({
			url: '/api/user/request',
			data: formData,
			processData: false,
			contentType: false,
			dataType: "JSON",
			type: 'POST',
			success: function (data) {
				requestedData = data;
				if (data.auth) {

					// alert("恭喜您成功申请了校正服务，我们将通过邮件通知您进行状况。");
					// location.reload();
					location.href = "/request/completed?id=" + requestedData.id + "&email=" + requestedData.email;
				}
				else {
					popup("authStart", function () {
						isAuthInProgress = false;
						$(".auth-school_id").val($("#form-request input[name='school']").val());
						$(".auth-school_email").attr("placeholder", $("#form-request input[name='email']").val());
						$(".auth-user_email").val($("#form-request input[name='email']").val());
						$(".popup-button").css("background-color", school_color);
						$(".popup-title").css("background-color", school_color);


						$(".form-auth").submit(function (e) {
							e.preventDefault();
							$(".auth-user_email").val($("#form-request input[name='email']").val());
							$(".auth-user_email").attr("disabled", false);
							var formData = new FormData($(this)[0]);
							$(".auth-user_email").attr("disabled", true);

							$.ajax({
								url: '/api/user/auth/start',
								data: formData,
								processData: false,
								contentType: false,
								dataType: "JSON",
								type: 'POST',
								success: function (data2) {
									token = data2.token;

									$(".step-now").addClass("step-left").removeClass("step-now");
									$($(".step-right").first()).addClass("step-now").removeClass("step-right");
									isAuthInProgress = true;

									$("#popup_authStart popup-button popup-prev").click(function () {
										isAuthInProgress = false;
									});

									checkAuth();
									// alert("입력하신 학교 이메일로 인증 링크를 발송하였습니다. 인증 완료 후 교열이 시작됩니다.");
								},
								error: function (xhr, ajaxOptions, thrownError) {
									if (xhr.status == 401) {
										alert("이메일이 잘못되었습니다.");
										// alert("학교에서 발급받은 이메일만 사용할 수 있습니다.");
									}
								}
							});
						})
					});
				}
				// alert("교열 요청을 성공하였습니다. 진행 상황을 이메일로 알려드립니다.");
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.status == 401) {
					alert("이메일이 잘못되었습니다.");
					// alert("학교에서 발급받은 이메일만 사용할 수 있습니다.");
				}
				else if (xhr.status == 500) {
					alert("由于网页故障，暂时不能传送。");
					// alert("일시적인 사이트 오류로 전송할 수 없습니다.");
				}
				// alert("작성 에러!");
			}
		});
	});

	$("body").on("click", ".fa-times", function (e) {
		e.preventDefault();
		$(".popup").remove();
	});

	$("body").on("keydown", function (e) {
		if (e.keyCode == 27) {
			$(".fa-times").click();
		}
	});

	var checkAuth = function () {
		if (!isAuthInProgress) {
			return;
		}
		if ($(".popup#popup_authStart").length < 1) {
			console.log("ff");
			return;
		}

		$.ajax({
			url: '/api/user/auth/check?token=' + token,
			processData: false,
			contentType: false,
			dataType: "JSON",
			type: 'GET',
			success: function (data) {
				if (data.isAuthenticated == 1) {
					location.href = "/request/completed?id=" + requestedData.id + "&email=" + requestedData.email;
				}
				else {
					setTimeout(function () {
						checkAuth();
					}, 2000);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				setTimeout(function () {
					checkAuth();
				}, 2000);
			}
		});
	}
});