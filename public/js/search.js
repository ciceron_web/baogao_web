$(document).ready(function () {

    var search_text;

    // $.ajax({
    //     url: "/api2/v2",
    //     type: "get",
    //     // dataType: "JSON",
    //     contentType: false,
    //     processData: false,
    // }).done(function (data) {
    // console.log(data.school_id);

    $(".search_text").autocomplete({
        source: "/api2/v2/lecturename?school_id=" + userInfomation.school_id,
        minLength: 0,
        select: function (event, ui) {
            // log( "Selected: " + ui.item.value + " aka " + ui.item.id );
        },
        focus: function (e, ui) {
            // console.log(this);
            return false;
        },
        open: function () {
            $(".ui-autocomplete").css("max-width", $(".search_text").outerWidth());
        }
    });

    $("body").on("focus", ".search_text", function () {
        $(this).keydown();
    });

    $("body").on("click", "input[type='text']", function () {
        $(this).select();
    });

    var ajax;

    $("body").on("submit", "form.search", function (e) {
        e.preventDefault();

        search_text = $(".search_text").val();
        ajax && ajax.abort();
        ajax = $.ajax({
            url: "/api2/v2/school/" + userInfomation.school_id + "/lectureFind?keyword=" + search_text,
            type: "get",
            contentType: false,
            processData: false,
        }).done(function (data) {
            // console.log(data);
            if (search_text == '') {
                location.reload();
            } else if (data.data.length == 0) {
                // console.log("ekhgd");
                alert("찾으시는 강의가 없습니다.");
            } else {
                // console.log(data.data);
                // $(".lecture_list").remove();
                $(".lecture_wrap").html('<ul class="lecture_list"></ul>');
                $.each(data.data, function (k, val) {
                    // console.log(val);
                    if (val.article_id == null){
                        val.rate = 0;
                    }
                    var lecture_li = '<li class="div4 lecture"><div class="padding_div"><a href="/lecture/view?lecture_id=' + val.lecture_id + '&school_id=' + val.school_id + '"><div class="school_title" style="background:' + userInfomation.school_color + '" value="' + val.school_id + '">' + val.school_name + '</div><div class="lecture_content"><span class="lecture_title" value="' + val.lecture_id + '">' + val.lecture_name + '</span><span class="professor">&nbsp;-' + val.tutor_name + '</span><div class="star_wrap"><span class="star-ratings-css" title="' + val.rate + '"></span><span class="star_point">' + val.rate + '/ 5</span></div><div class="text">' + val.text + '</div></div></a></div></li>'

                    $(".lecture_list").append(lecture_li);

                });


                var slider = $('.lecture_list').slick({
                    dots: false,
                    /*customPaging: function (slider, i) {
                        var thumb = $(slider.$slides[i]).data();
                        return '<a>' + (i + 1) + '</a>';
                    },*/
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    rows: 2,
                    responsive: [
                        {
                          breakpoint: 960,
                          settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                          }
                        },
                        {
                          breakpoint: 640,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                          }
                        }
                      ]
                });
            }

        }).fail(function (err) {
            console.log("실패");
        });

    });


    // }).fail(function (err) {
    //     console.log(err);
    // });


});