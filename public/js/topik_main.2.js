var keyArray = ["선택안함", "논술문", "설명문", "단답형"];
var valueArray = [0, 50, 30, 20];
var limitArray = [0, 700, 300, 0];
var err_type = [
    "에러가 없을 때", "형태소 분석이 안 될 때", "오용어로 분석될 때", "다수어절 오류", "의미 문체 오류", "문장 부호 오류", "통계정보를 이용한 붙여쓰기", "영어 오용어로 분석될 때", "태깅 오류", "복합명사 언더바 오류", "오류 형태에 따라 붙여쓰기"
];
var question_type;
var content = [];
var corrections;
var corrections_item;
var sentence_id_arr = [];
var result_arr = [];
var mark_length;
var content_form_text_index;
var new_help_index = 0;

// topik select text popup event
if (!window.x) {
    x = {};
}

x.Selector = {};
x.Selector.getSelected = function () {
    var t = '';
    if (window.getSelection) {
        t = window.getSelection().toString();
    } else if (document.getSelection) {
        t = document.getSelection().toString();
    } else if (document.selection) {
        t = document.selection.createRange().text;
    }
    return t;
}

var pageX;
var pageY;

var point_func = function () {

    // 현 감점 상황
    $(".minus_point_span").text("");
    var minus_point_result = Helper.prototype.minus_point_add($(".help_content input[name='subtract_point']"));
    $(".minus_point_span").text(minus_point_result);

    // 현 점수
    $(".now_score").text("");
    var result_score = now_point_check();
    $(".now_score").text(result_score);
}

$(document).ready(function () {

    var helper = new Helper();
    helper.Init();

    // popup-open
    $("body").on("click", ".popup_btn", function (e) {
        var targeted_popup = $(".popup");
        targeted_popup.removeClass("invisible");

        e.preventDefault();
    });

    // popup-close
    $("body").on("click", ".popup-close", function (e) {
        var targeted_popup = $(".popup");
        targeted_popup.addClass("invisible");

        e.preventDefault();
    });

    // popup-submit
    $("body").on("submit", ".form_send_mail", function (e) {
        e.preventDefault();

        helper.send_mail(this);

        return false;
    });


    // text counter
    $("body").on("change", "#form_type", helper.setStanardScore);

    $("body").on("keyup", "#form_text", function () {

        var selected_opt = helper.get_select_val($("#form_type input[type='radio']:checked"));
        var total = $(".total_score");
        var len = $(this).val().length;

        for (var i = 0; i < valueArray.length; i++) {
            if (selected_opt === valueArray[i]) {
                if (len > limitArray[i] && limitArray[i] != 0) {
                    total.addClass("red_font");
                } else {
                    total.removeClass("red_font");
                }
            }
        }
        total.text(len);

        if (parseInt(total.text()) > 0) {

            $("#content button").addClass("red_btn");
        } else {
            $("#content button").removeClass("red_btn");
        }
    });

    // 채점하기 버튼
    $("body").on("click", "#content button", function () {

        question_type = parseInt($("#form_type input[type='radio']:checked").index() / 2);
        var text_content = $.trim($("#form_text").val());

        if (text_content === "") {
            alert("문장을 입력하세요.");
        } else {

            var formData = new FormData();
            formData.append("question_type", question_type);
            formData.append("content", text_content);

            if ($(".total_score").hasClass("red_font")) {
                var r = confirm("최대 글자수를 넘었습니다. 진행하시겠습니까?");
                if (r) {
                    helper.topik_mark(formData);
                    // helper.showResultPage(question_type);
                }
            } else {

                helper.topik_mark(formData);
                // helper.showResultPage(question_type);
            }
        }
        return false;
    });

    // 2번째 페이지 form_text 영역 .red_font click시 help_content 찾기
    $("body").on("click", "#form_text .red_font", function (e) {

        e.preventDefault();

        var red_font_index = $(this).attr("sentence_index");
        var red_font_id = $(this).attr("sentence_id");
        var help_content_length = $(".help_content").length;
        var help_parent_mark;
        // console.log(red_font_index);
        // console.log(red_font_id);
        $(".help_content [name='sentence_id']").each(function (k, elem) {
            if ($(elem).val() == red_font_id) {
                help_parent_mark = $(this).parents(".help_content").find("button").attr("wrong_text_mark");
                var help_wrong_start = parseInt($("#help_content" + help_parent_mark + " input[name='wrong_start']").val()) + 1;
                var help_wrong_end = parseInt($("#help_content" + help_parent_mark + " input[name='wrong_end']").val());
                if (red_font_index >= help_wrong_start && red_font_index <= help_wrong_end) {

                    var this_helpContent_pos = $("#help_content" + help_parent_mark).offset().top;
                    var help_prevAll = $("#help_content" + help_parent_mark).prevAll();
                    var amount_outer_height = 0;

                    help_prevAll.each(function (index, elem2) {
                        amount_outer_height += $(elem2).outerHeight();
                    });

                    // console.log(amount_outer_height);

                    // var this_helpContent_pos = $(this_help_content).outerHeight();
                    var style2_pos = $(".style2").scrollTop();

                    $(".style2").stop().animate({
                        scrollTop: amount_outer_height
                    });

                    // console.log("style: ",style2_pos);
                    // console.log("amount: ",amount_outer_height);

                }
            }
        });
    });

    // side_help delete
    $("body").on("click", ".help_delete_btn", function () {

        var this_content = $("#help_content" + $(this).attr("wrong_text_mark"));
        var this_sentence_id = this_content.find("input[name='sentence_id']").val();
        var this_wrong_start = parseInt(this_content.find("input[name='wrong_start']").val());
        var this_wrong_end = parseInt(this_content.find("input[name='wrong_end']").val());

        for (var i = this_wrong_start; i <= this_wrong_end; i++) {
            $("#form_text span[sentence_id='" + this_sentence_id + "'][sentence_index='" + i + "']").removeClass("red_font").find("div").remove();
        }

        $(this).parents(".help_content").remove();

        point_func();

        if ($(".style2 .help_content").length <= 0) {
            $(".style2").text("오류가 없습니다.");
        }

    });

    // side_help modify
    $("body").on("click", ".help_modify_btn", function (e) {

        e.preventDefault();

        helper.modifyAndCancel(this);
        var this_help_content = "#help_content" + $(this).attr("wrong_text_mark");

        $(this_help_content).siblings(".help_content").find(".modify_complete_btn").click();

        $(this_help_content + " .modify_form").removeClass("invisible");
        $(this_help_content + " .modify_form").prev().addClass("invisible");
        $(this_help_content + " input").eq(1).select();

        // var this_helpContent_pos = $("#help_content" +(parseInt($(this).attr("wrong_text_mark"))-1)).height();
        var this_helpContent_pos = $(this_help_content).offset().top;
        // var this_helpContent_pos = $(this_help_content).outerHeight();
        var style2_pos = $(".style2").scrollTop();

        $(".style2").stop().animate({
            scrollTop: style2_pos - ($(this_help_content).outerHeight() - this_helpContent_pos) + 40
        });

        // console.log($(this_help_content).outerHeight());
        // console.log(this_helpContent_pos);
        // console.log((parseInt($(this).attr("wrong_text_mark"))+1)*305);
    });

    // side_help cancle
    $("body").on("click", ".modify_cancel_btn", function () {
        helper.modifyAndCancel(this);
        $("#help_content" + $(this).attr("wrong_text_mark") + " .modify_form").addClass("invisible");
        $("#help_content" + $(this).attr("wrong_text_mark") + " .modify_form").prev().removeClass("invisible");

        // var help_content_length = $(".help_content").length;

        // alert(mark_length);
        if (parseInt($(this).attr("wrong_text_mark")) >= mark_length) {
            // alert($(this).attr("wrong_text_mark"));
            // alert(content_length);
            var this_content = $("#help_content" + $(this).attr("wrong_text_mark"));
            var this_sentence_id = this_content.find("input[name='sentence_id']").val();
            var this_wrong_start = parseInt(this_content.find("input[name='wrong_start']").val());
            var this_wrong_end = parseInt(this_content.find("input[name='wrong_end']").val());

            for (var i = this_wrong_start; i <= this_wrong_end; i++) {
                $("#form_text span[sentence_id='" + this_sentence_id + "'][sentence_index='" + i + "']").removeClass("red_font").find("div").remove();
            }

            $(this).parents(".help_content").remove();
            mark_length++;
        }
    });

    // side_help coplete
    $("body").on("click", ".modify_complete_btn", function () {
        helper.modifyAndCancel(this);
        var modify_arr = new Array();
        var help_content_id = $("#help_content" + $(this).attr("wrong_text_mark"));
        var modify_class = help_content_id.find(".modify_form");
        var modify_prev_class = modify_class.prev();

        modify_prev_class.each(function () {

            modify_arr.push($(this).text());
        });

        modify_arr.length = 0;

        modify_class.each(function (i) {
            if ($(this).is("select")) {
                modify_arr.push($(this).find("option:selected").text());

            } else if ($(this).is("input[name='subtract_point']")) {

                if ($(this).val() == "") {
                    $(this).val(0);
                }

                modify_arr.push(parseInt($(this).val()));
            } else {
                modify_arr.push($(this).val());
            }

            modify_prev_class.eq(i).text(modify_arr[i]);
        });

        var help_text_split = (modify_arr[4]).replace(/\n/g, "<br>");
        // console.log(help_text_split);
        $(this).parents(".help_content").find("textarea").prev("span").html(help_text_split);
        // help_content input values
        // console.log(modify_arr);

        modify_class.prev().removeClass("invisible");
        modify_class.addClass("invisible");

        // 점수 수정 func
        point_func();

        // #form_text 수정
        var this_sentence_id = help_content_id.find("input[name='sentence_id']").val();
        var this_subtract_point = parseInt(help_content_id.find("input[name='subtract_point']").val());
        var this_wrong_end = parseInt(help_content_id.find("input[name='wrong_end']").val());
        var this_wrong_start = parseInt(help_content_id.find("input[name='wrong_start']").val());
        var this_wrong_str = help_content_id.find("input[name='wrong_str']").val();


        for (var i = this_wrong_start + 1; i <= this_wrong_end; i++) {
            $("#form_text span[sentence_id='" + this_sentence_id + "'][sentence_index='" + i + "']").removeClass("red_font").find("div").remove();
        }

        if ((this_wrong_end - this_wrong_start) != this_wrong_str.length) {
            this_wrong_end = this_wrong_start + this_wrong_str.length;
            help_content_id.find("input[name='wrong_end']").val(this_wrong_end);
        }

        for (var i = this_wrong_start + 1; i <= this_wrong_end; i++) {
            $("#form_text span[sentence_id='" + this_sentence_id + "'][sentence_index='" + i + "']").addClass("red_font");
            if (i == this_wrong_end) {
                $("#form_text span[sentence_id='" + this_sentence_id + "'][sentence_index='" + i + "']").append("<div></div>");
            }
        }

        $("#form_text span[sentence_id='" + this_sentence_id + "'][sentence_index='" + this_wrong_end + "']").find("div").attr("data-name", "-" + this_subtract_point);

    });

    // help_content input click select event
    $("body").on("click", ".modify_form", function () {
        $(this).select();
    });

    // 감점추가 버튼 이벤트
    var lastSelectedText = "";
    var start_span_sentence_id;
    var sentence_id_index;

    $("#content2 #form_text").bind("mouseup", function (e) {
        // console.log(e)
        var selectedText = x.Selector.getSelected();
        if (selectedText != "") {
            $(".select_text_popup").css({
                "left": pageX + 5,
                "top": pageY - 55
            }).fadeIn(200);
            lastSelectedText = selectedText;
            var target = e.target;

            if (!$(target).is('#content2 #form_text, .select_text_popup') && !$(target).parents().is('#content2 #form_text, .select_text_popup')) {
                $(".select_text_popup").fadeOut(200);
            }

            if ($(target).is("#form_text span")) {
                start_span_sentence_id = $(target).attr("sentence_id");
                sentence_id_index = $(target).index(this);
            }
            // console.log(lastSelectedText);
        } else {
            $(".select_text_popup").fadeOut(200);
        }
    });
    $("#content2 #form_text").on("mousedown", function (e) {
        pageX = e.pageX;
        pageY = e.pageY;
    });

    $("body").on("click", ".select_text_popup", function () {
        // alert(lastSelectedText);
        var help_contents_length = $(".style2 .help_content").length;
        // var help_contents_length = content.length;
        var last_span_attr_id = $("#form_text span").last().attr("sentence_id");
        var error_check = false;

        if (help_contents_length < 1) {
            $(".style2").text("");
        }

        // alert(start_span_sentence_id);
        // alert(last_span_attr_id);

        for (var i = parseInt(start_span_sentence_id); i <= parseInt(last_span_attr_id); i++) {
            // alert(error_check);
            var content_form_text_value = $.trim($("span[sentence_id=" + i + "]").text());
            content_form_text_index = content_form_text_value.indexOf(lastSelectedText);

            // console.log(content_form_text_index);
            if (content_form_text_index > -1) {

                var user_select_check = $("span[sentence_id='" + i + "']");
                var red_font_check = $(".red_font[sentence_id='" + i + "']");

                if (red_font_check.length >= 1) {

                    var red_font_sentence_index = parseInt(red_font_check.attr("sentence_index")) - 1;
                    var red_font_help_mark = $("input[name='wrong_start'][value='" + red_font_sentence_index + "']").parents(".help_content").find("button").attr("wrong_text_mark");
                    var red_font_help_content = $("#help_content" + red_font_help_mark);
                    var red_font_point = red_font_help_content.find("input[name='subtract_point']").val();
                    var red_font_start = red_font_help_content.find("input[name='wrong_start']").val();
                    var red_font_end = red_font_help_content.find("input[name='wrong_end']").val();

                    var e, f;

                    for (e = parseInt(red_font_start) + 1; e <= parseInt(red_font_end); e++) {
                        // console.log(e);
                        for (f = parseInt(content_form_text_index); f <= parseInt(content_form_text_index) + lastSelectedText.length; f++) {
                            // console.log(f);
                            if (e == f) {

                                alert("이미 오류 체크된 부분이 있습니다. 삭제 후 진행해주세요.");
                                $(this).fadeOut(200);

                                return false;

                            }
                        }

                    }

                }

                var no_nbsp_str = lastSelectedText.replace(/\s/g, " ");

                var val = {
                    wrong_str: no_nbsp_str,
                    subtract_point: 0,
                    correct_method: 0,
                    help_text: "",
                    replace_word: "",
                    wrong_start: parseInt(content_form_text_index),
                    wrong_end: parseInt(content_form_text_index) + lastSelectedText.length,
                    sentence_id: i,
                    lecture_link: "http://www.test.com",
                    lecture_name: "강의 4"
                };

                help_content_maker(new_help_index, val, ".sidebar2");

                $(".help_content .modify_complete_btn").click();
                $("#help_content" + new_help_index + " .help_modify_btn").click();

                new_help_index++;

                var key2 = {
                    wrong_start: parseInt(content_form_text_index),
                    wrong_end: parseInt(content_form_text_index) + lastSelectedText.length
                };
                var elem1 = $("span[sentence_id=" + i + "]");

                helper.wrong_str_addClass(key2, elem1, val.subtract_point, true);

                // console.log(lastSelectedText.length);
                // ($("#form_text span[sentence_id='"+i+"']").eq(content_form_text_index+1)).addClass("red_font");

                error_check = true;
                //찾았니 트루
                break;
            }

        }

        $(this).fadeOut(200);

        if (error_check == false) alert("한 문장 내에서만 선택해주세요.");
    });


    // drag to scroll event
    /*
    var curDown = false,
        curYPos = 0,
        curXPos = 0;

    $("body").on("mousemove", ".style2", function(m){
        if (curDown === true) {
            $(".style2").scrollTop($(".style2").scrollTop() + (curYPos - (m.pageY)));
            $(".style2").scrollLeft($(".style2").scrollLeft() + (curXPos - m.pageX));
        }
    });

    $("body").on("mousedown", ".style2", function(m){
        curDown = true;
        curYPos = m.pageY;
        curXPos = m.pageX;
    });

    $("body").on("mouseup", ".style2", function(){
        curDown = false;
    });
    */

    // 완료 및 확인 버튼
    $("body").on("click", ".complete_check_btn", function () {

        $("input[name='replace_word']").each(function (k, elem) {

            var replace_words = (($(elem).val()).split(","));
            var first_word = replace_words.length > 1 ? replace_words[0] : replace_words;
            $(elem).val(first_word);
        });

        $("input[name='wrong_start']").each(function () {

            $(this).val(parseNum_func($(this)));
        });

        $("input[name='wrong_end']").each(function () {

            $(this).val(parseNum_func($(this)));
        });

        $("input[name='sentence_id']").each(function () {
            sentence_id_arr.push($(this).val());
        });

        $.each(sentence_id_arr, function (m, el) {

            if ($.inArray(el, result_arr) == -1) {
                result_arr.push(el);
            }
        });

        $(".help_content .modify_complete_btn").click();
        $(".result_content").submit();
        $(".content2_inner_wrap").addClass("invisible");
        $(".content3_inner_wrap").removeClass("invisible");

    });

    // console.log(corrections);

    //뒤로가기
    $("body").on("click", ".back_btn", function () {
        $(".content2_inner_wrap").removeClass("invisible");
        $(".content3_inner_wrap").addClass("invisible");

        $(".content3_inner_wrap .style2, .content3_table .content3 #form_text").empty();

        // console.log(corrections);
        // console.log(corrections_item);
    });

    $("body").on("submit", ".result_content", function (e) {
        e.preventDefault();

        corrections = [];
        $(".help_content").each(function (i, item) {
            corrections_item = {};
            $(item).find('input, textarea, select').each(function (j, item2) {
                // if($(item2).prop("name")=='wrong_start'){

                // }
                corrections_item[$(item2).attr('name')] = $(item2).val();
            });
            corrections.push(corrections_item);
        });

        var data = {
            question_type: question_type,
            content: content,
            corrections: corrections
        }
        // console.log(corrections);

        helper.changeHelpCommentComplete(data);

        return false;
    });

    $("body").on("click", ".print_btn", function () {
        // $(".content3 .textarea_wrap, .content3 #form_text").css({
        //     "max-height": "none"
        // });

        window.print();

        // $(".content3 .textarea_wrap, .content3 #form_text").css("max-height", "380px");
    });

});

function Helper() {
    this.form_textarea = null;
}

// 초기화
Helper.prototype.Init = function () {
    this.form_textarea = $("#form_text");

    this.add_option();
    this.setStanardScore();
}

// option append event
Helper.prototype.add_option = function () {
    for (var i = 0; i < keyArray.length; i++) {

        var score_text = "(" + valueArray[i] + "점)";
        if (valueArray[i] === 0) {
            score_text = "";
        }

        $("#form_type").append("<input type='radio' value='" + valueArray[i] + "' name='question_type' id='question_" + i + "'/>" + "<label for ='question_" + i + "'>" + keyArray[i] + score_text + "</label>");
    }

    $("input[type='radio']").eq(0).click();
}

// option value return
Helper.prototype.get_select_val = function (opt) {
    var opt_val = opt;
    opt_val = parseInt(opt_val.attr("value"));

    return opt_val;
}

// option별 기본점수 set
Helper.prototype.setStanardScore = function () {
    // var selected_opt = Helper.prototype.get_select_val($("#form_type option:selected"));
    var selected_opt = Helper.prototype.get_select_val($("#form_type input[type='radio']:checked"));
    var standard = $(".standard_score");
    var total = $(".total_score");
    var form_textarea = $("#form_text");

    // form_textarea.val("");

    total.removeClass("red_font").text(0);
    $("#form_text").keyup();

    for (var i = 0; i < valueArray.length; i++) {

        if (selected_opt === valueArray[i]) {

            var standard_text = "/ " + limitArray[i] + "자";
            if (limitArray[i] === 0) {
                standard_text = "";
            }

            standard.text(standard_text);
            break;
        }

    }
}

// 채점결과 페이지 보이기
Helper.prototype.showResultPage = function (index) {

    $("#content2_wrap").removeClass("invisible");
    $("#content").addClass("invisible");

    var result_text = $(".result_text > span").last();

    switch (index) {
        case 1:
        case 2:
        case 3:
            result_text.find("b").last().text(valueArray[index]);
            break;

        default:
            result_text.find("b").text(0);
            break;
    }

    // this.addHelpContents();
}

// side 오답체크부분
Helper.prototype.addHelpContents = function (arr) {
    // var help_div = "<div class='help_content'><ul></ul></div>";

    $.each(arr, function (key, val) {
        help_content_maker(key, val, true);
    });

}

var help_content_maker = function (key, val, style_parent, is_append) {
    // console.log(val);
    var replace_word_type = typeof (val.replace_word) == "string" ? val.replace_word : val.replace_word[0];

    // (val.wrong_str).replace(/&nbsp;/g, " ");
    // (val.replace_word).replace(/&nbsp;/g, " ");

    var html = "<div class='help_content' id='help_content" + key + "'>" +
        "<ul>" +
        "<li>" +
        "<span>입력내용</span>" +
        "<span>" +
        "<span>" + val.wrong_str + "</span>" +
        "<input type='text' name='wrong_str' value='" + val.wrong_str + "' class='invisible modify_form'>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<span>모범답안</span>" +
        "<span>" +
        "<span>" + val.replace_word + "</span>" +
        "<input type='text' name='replace_word' value='" + val.replace_word + "' class='invisible modify_form'>" +
        // "<select class = 'invisible modify_form' name = 'replace_word'>" +
        // "</select>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<span>감점정도</span>" +
        "<span>- " +
        "<span>" + val.subtract_point + "</span>" +
        "<input type='number' name='subtract_point' value='" + val.subtract_point + "' style='width:50%;' class='invisible modify_form' min='0'>" +
        "점</span>" +
        "</li>" +
        "<li>" +
        "<span>감점유형</span>" +
        "<span>" +
        "<span correct_method='" + val.correct_method + "'>" + err_type[parseInt(val.correct_method)] + "</span>" +
        "<select class='invisible modify_form' name='correct_method'>" +
        "</select>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<span>도움말</span>" +
        "<span>" +
        "<span class='help_text_wrap'>" + val.help_text + "</span>" +
        "<textarea class='invisible modify_form' name='help_text' rows='6'>" + val.help_text + "</textarea>" +
        "</span>" +
        "</li>" +
        "<li>" +
        "<span>연관강의</span>" +
        "<span>" +
        "<a href='http://www." + "#" + "' target='_blank' >" +
        "<span>" + "연관강의" + "</span>" +
        "</a>" +
        "<input type='text' name='lecture_link' value='" + "연관강의" + "' class='invisible modify_form'>" +
        "<input type='hidden' name='lecture_name' value='" + "강의 4" + "'>" +
        "</span>" +
        "</li>" +
        "<li class='has_btn'>" +
        "<button type='button' class='help_modify_btn' wrong_text_mark='" + key + "'>수정</button>" +
        "<button type='button' class='help_delete_btn' wrong_text_mark='" + key + "'>삭제</button>" +
        "</li>" +
        "<li class='has_btn invisible'>" +
        "<button type='button' class='modify_cancel_btn' wrong_text_mark='" + key + "'>취소</button>" +
        "<button type='button' class='modify_complete_btn' wrong_text_mark='" + key + "'>완료</button>" +
        "</li>" +
        "</ul>" +
        "<input type='hidden' name='sentence_id' value='" + val.sentence_id + "'>" +
        "<input type='hidden' name='wrong_start' value='" + parseInt(val.wrong_start) + "'>" +
        "<input type='hidden' name='wrong_end' value='" + parseInt(val.wrong_end) + "'>" +
        "</div>"

    if (is_append) {

        $(style_parent + " .style2").append(html);
    } else {
        $(style_parent + " .style2").prepend(html);
    }

    $(style_parent + " .style2").html($(style_parent + " .help_content").sort(function (a, b) {

        return $(a).find('input[name=sentence_id]').val() == $(b).find('input[name=sentence_id]').val() ? $(a).find('input[name=wrong_start]').val() - $(b).find('input[name=wrong_start]').val() : $(a).find('input[name=sentence_id]').val() - $(b).find('input[name=sentence_id]').val();
    }));

    /*
    if(typeof(val.replace_word)=="string"){
        $("#help_content" + key + " select[name='replace_word']").append("<option value = '" + val.replace_word + "'>" + val.replace_word + "</option>");
    } else {

        for (var i = 0; i < val.replace_word.length; i++) {
            $("#help_content" + key + " select[name='replace_word']").append("<option value = '" + val.replace_word + "'>" + val.replace_word[i] + "</option>");
        }
    }
    */

    $("#help_content" + key + " select[name='correct_method']").empty();

    for (var j = 0; j < err_type.length; j++) {
        $("#help_content" + key + " select[name='correct_method']").append("<option value = '" + j + "'>" + err_type[j] + "</option>");
    }
};

var print_help_content = function (key, val) {

    // console.log(val.sentence_id);

    // sentence_id_arr.push(val.sentence_id);

    // console.log(sentence_id_arr);
    var sentence_table =
        "<table class='print_sentence' sentence_id='" + val.sentence_id + "' style='page-break-after:always;'>" +
        "<tr>" +
        "<td>오답문장</td>" +
        "<td class='wrong_sentence'></td>" +
        "</tr>" +
        "<tr>" +
        "<td>수정문장</td>" +
        "<td class='replace_sentence'></td>" +
        "</tr>" +
        "</table>";

    var html =
        sentence_table +
        "<table class='print_help_content' id='print_help_content" + key + "' style='page-break-after:always;'>" +
        "<tr>" +
        "<th><span>모범답안</span>" +
        "<span>" +
        val.replace_word +
        "</span>" +
        "<input type='text' name='replace_word' value='" + val.replace_word + "' class='invisible modify_form'>" +
        "</th>" +

        "<td><span>입력내용</span>" +
        "<span>" +
        val.wrong_str +
        " (-" + val.subtract_point + ")" +
        "<input type='text' name='wrong_str' value='" + val.wrong_str + "' class='invisible modify_form'>" +
        "</span>" +
        "<input type='number' name='subtract_point' value='" + val.subtract_point + "' class='invisible modify_form'>" +
        "</td>" +
        "</tr>" +

        "<tr>" +
        "<td>감점유형</td>" +
        "<td>" +
        "<span correct_method='" + val.correct_method + "'>" +
        err_type[parseInt(val.correct_method)] +
        "</span>" +
        "<select class='invisible modify_form' name='correct_method'></select>" +
        "</td>" +
        "</tr>" +

        "<tr>" +
        "<td>도움말</td>" +
        "<td>" +
        "<span>" + val.help_text + "</span>" +
        "<textarea class='invisible modify_form' name='help_text' rows='6'>" + val.help_text + "</textarea>" +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<td>연관강의</td>" +
        "<td>" +
        "<a href='http://www." + "#" + "' target='_blank'>" +
        "강의 2" +
        "</a>" +
        "<input type='text' name='lecture_link' value='" + "연관강의" + "' class='invisible modify_form'>" +
        "<input type='hidden' name='lecture_name' value='" + "강의 4" + "'>" +
        "</td>" +
        "</tr>" +
        "<tr>" +
        "<input type='hidden' name='sentence_id' value='" + val.sentence_id + "'>" +
        "<input type='hidden' name='wrong_start' value='" + parseInt(val.wrong_start) + "'>" +
        "<input type='hidden' name='wrong_end' value='" + parseInt(val.wrong_end) + "'>" +
        "</tr>" +
        "</table>";

    $(".sidebar3 .style2").append(html);

    $("#print_help_content" + key + " select[name='correct_method']").empty();

    for (var j = 0; j < err_type.length; j++) {
        $("#print_help_content" + key + " select[name='correct_method']").append("<option value = '" + j + "'>" + err_type[j] + "</option>");
    }

};

// 수정, 취소 버튼 이벤트
Helper.prototype.modifyAndCancel = function (obj) {
    var invisible_class = "invisible";

    $(obj).parent(".has_btn").addClass(invisible_class);
    $(obj).parent(".has_btn").siblings(".has_btn").removeClass(invisible_class);

}

// topik_mark api 연결
Helper.prototype.topik_mark = function (val) {

    $.ajax({
        url: "/api/v1/topik/mark",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: val
    }).done(function (data) {

        mark_length = data.marks.length;

        if (question_type === 3 && data.content.length > 1) {

            var type3_confirm = confirm("1문장 이상입니다. 진행하시겠습니까?");

            if (type3_confirm) {

                // quiz type 결과페이지 넘기기
                Helper.prototype.showResultPage(question_type);
                var select_type = $("#form_type input[type='radio']:checked + label").text();
                var question_type_span = $("span.question_type");
                question_type_span.text(select_type);

                $(".quiz_type_val").attr("value", question_type);

                var form_textarea = $("#content2 #form_text");
                var k_index;
                // var split_sentence = data.content.split("");
                // split_sentence.unshift(" ");

                // 문장 1개씩 나눠서 span 처리
                $.each(data.content, function (k, val) {
                    k_index = k;
                    var split_sentence = val.split("");
                    split_sentence.unshift(" ");

                    for (var i = 0; i < split_sentence.length; i++) {
                        split_sentence[i] = split_sentence[i] == ' ' ? "&nbsp;" : split_sentence[i];
                        form_textarea.append("<span sentence_id='" + k + "' sentence_index='" + i + "'>" + split_sentence[i] + "</span>");
                    }

                    content.push(val);
                });

                var form_text_span_leng = form_textarea.find("span").length;
                var form_text_row = Math.floor(form_textarea.width() / 30);
                // console.log(form_text_span_leng%form_text_row);
                if (form_text_span_leng % form_text_row != 0) {
                    var remain_span = form_text_row - (form_text_span_leng % form_text_row);

                    for (var e = 0; e < remain_span; e++) {
                        form_textarea.append("<span sentence_id='" + (k_index + 1) + "'>&nbsp;</span>");
                    }
                }

                // help content 채우기
                var correct_method_val;
                if (data.marks.length <= 0) {
                    // $(".style2").text("1문장 이상입니다. 문법 오류는 없으나 감점이 있습니다.");
                    var key = 0;
                    var val = {
                        wrong_str: "오류 없음",
                        subtract_point: 10,
                        correct_method: 0,
                        help_text: "1문장 이상입니다.",
                        replace_word: "",
                        wrong_start: "",
                        wrong_end: "",
                        sentence_id: "",
                        lecture_link: "http://www.test.com",
                        lecture_name: "강의 4"
                    };

                    help_content_maker(new_help_index, val, ".sidebar2");

                    var help_text_split = (val.help_text).replace(/\n/g, "<br>");
                    $("#help_content" + new_help_index + " textarea").prev("span").html(help_text_split);
                    new_help_index++;
                } else {

                    $.each(data.marks, function (key, val) {

                        correct_method_val = val.correct_method;
                        val.subtract_point = 2;
                        help_content_maker(new_help_index, val, ".sidebar2");

                        var help_text_split = (val.help_text).replace(/\n/g, "<br>");
                        $("#help_content" + new_help_index + " textarea").prev("span").html(help_text_split);

                        new_help_index++;

                        var sentence_id_text = $.trim($("#form_text span[sentence_id='" + val.sentence_id + "']").text());
                        var sentence_id_span = $("#form_text span[sentence_id='" + val.sentence_id + "']");

                        Helper.prototype.wrong_str_addClass(val, sentence_id_span, val.subtract_point, true);

                    });

                }

                $(".help_content select option[value='" + correct_method_val + "']").attr("selected", "selected");

                // 점수 수정 func
                point_func();
            }
        } else {

            // quiz type 결과페이지 넘기기
            Helper.prototype.showResultPage(question_type);
            var select_type = $("#form_type input[type='radio']:checked + label").text();
            var question_type_span = $("span.question_type");
            question_type_span.text(select_type);

            $(".quiz_type_val").attr("value", question_type);

            var form_textarea = $("#content2 #form_text");
            var k_index;

            // 문장 1개씩 나눠서 span 처리
            $.each(data.content, function (k, val) {
                k_index = k;
                var split_sentence = val.split("");
                split_sentence.unshift(" ");

                for (var i = 0; i < split_sentence.length; i++) {
                    split_sentence[i] = split_sentence[i] == ' ' ? "&nbsp;" : split_sentence[i];
                    form_textarea.append("<span sentence_id='" + k + "' sentence_index='" + i + "'>" + split_sentence[i] + "</span>");
                }

                content.push(val);
            });

            // console.log(content);

            var form_text_span_leng = form_textarea.find("span").length;
            var form_text_row = Math.floor(form_textarea.width() / 30);
            // console.log(form_text_span_leng%form_text_row);
            if (form_text_span_leng % form_text_row != 0) {
                var remain_span = form_text_row - (form_text_span_leng % form_text_row);

                for (var e = 0; e < remain_span; e++) {
                    form_textarea.append("<span sentence_id='" + (k_index + 1) + "'> </span>");
                }
            }

            // help content 채우기
            var correct_method_val;
            if (data.marks.length <= 0) {
                $(".style2").text("오류가 없습니다.");
            } else {

                $.each(data.marks, function (key, val) {

                    correct_method_val = val.correct_method;
                    val.subtract_point = 2;
                    help_content_maker(new_help_index, val, ".sidebar2");

                    var help_text_split = (val.help_text).replace(/\n/g, "<br>");
                    $("#help_content" + new_help_index + " textarea").prev("span").html(help_text_split);
                    new_help_index++;
                    var sentence_id_text = $.trim($("#form_text span[sentence_id='" + val.sentence_id + "']").text());
                    var sentence_id_span = $("#form_text span[sentence_id='" + val.sentence_id + "']");

                    Helper.prototype.wrong_str_addClass(val, sentence_id_span, val.subtract_point, true);

                });

            }

            $(".help_content select option[value='" + correct_method_val + "']").attr("selected", "selected");

            // 점수 수정 func
            point_func();

            // console.log(data);

        }

    }).fail(function (err) {

        // console.log(err);
        alert(err);
    });
}

// back_arrow event
var back_arrow_evt = function () {
    if (!$("#content2_wrap").hasClass("invisible") && $(".content3_inner_wrap").hasClass("invisible")) {
        location.href = '/topik_tool';
    } else if ($(".content2_inner_wrap").hasClass("invisible")) {
        $(".content3_inner_wrap").addClass("invisible");
        $(".content2_inner_wrap").removeClass("invisible");

        $(".content3_inner_wrap .style2, .content3_table .content3 #form_text").empty();
    }
}

// 현 감점 상황 func
var now_point_check = function () {
    var result = Helper.prototype.get_select_val($("#form_type input[type='radio']:checked"));

    result = result - parseInt($(".minus_point_span").text());

    return result;
}

// 현재감점 계산
Helper.prototype.minus_point_add = function (val) {
    var sum = 0;

    $.each(val, function (i, elem) {
        // sum += (1 * ($(elem).val()));
        sum += parseInt($(elem).val());
    });

    return sum;
}

// parseInt event
var parseNum_func = function (item) {
    var this_val = $(item).val();
    this_val = parseInt(this_val);

    return this_val;
}

// 오답체크 변경 완료 이벤트
Helper.prototype.changeHelpCommentComplete = function (data) {

    // var formData = new FromData($(arr)[0]);
    var JSON_data = JSON.stringify(data);

    $.ajax({
        url: "/api/v1/topik/revise",
        type: "post",
        dataType: "json",
        // contentType: "application/json; charset=utf-8",
        contentType: false,
        processData: false,
        data: JSON_data
    }).done(function (data) {

        // console.log(data);
        // console.log(data.corrections);

        append_span_func($("#content_before #form_text"), data.content);
        append_span_func($("#content_after #form_text"), data.revision);

        // console.log(data.content);

        $.each(data.corrections, function (key, val) {
            // console.log(key);
            // console.log(val);

            var sidebar3_help_content = $(".sidebar3 .print_help_content");
            print_help_content(new_help_index, val);

            var br_tag = "<br><br>";
            var help_text_split = (val.help_text).replace(/\n/, br_tag);
            // console.log(help_text_split);
            $("#print_help_content" + new_help_index + " textarea").prev("span").html(help_text_split);

            new_help_index++;

            var wrong_str_arr = [];
            var sentence_id_span = $("#content_before #form_text span[sentence_id='" + val.sentence_id + "']");
            wrong_str_arr.push($.trim(sentence_id_span.text()));
            Helper.prototype.wrong_str_addClass(val, sentence_id_span, val.subtract_point, true);

            var replace_str_arr = [];
            var sentence_id_span2 = $("#content_after #form_text span[sentence_id='" + val.sentence_id + "']");
            replace_str_arr.push($.trim(sentence_id_span2.text()));
            Helper.prototype.wrong_str_addClass(val, sentence_id_span2, val.subtract_point, false);
            // console.log(val);

            var wrong_print_sentence_span = $(".print_sentence[sentence_id='"+val.sentence_id+"'] .wrong_sentence span[sentence_id='"+val.sentence_id+"']");
            var replace_print_sentence_span = $(".print_sentence[sentence_id='"+val.sentence_id+"'] .replace_sentence span[sentence_id='"+val.sentence_id+"']");
            append_span_func($(".print_sentence[sentence_id='" + val.sentence_id + "']").find(".wrong_sentence"), wrong_str_arr);
            append_span_func($(".print_sentence[sentence_id='" + val.sentence_id + "']").find(".replace_sentence"), replace_str_arr);


            // ↓↓↓ 요기부터 ↓↓↓
            
            // red_font class 추가
            Helper.prototype.wrong_str_addClass(val, wrong_print_sentence_span, val.subtract_point, true);
            Helper.prototype.wrong_str_addClass(val, replace_print_sentence_span, val.subtract_point, false);

            // 같은 sentence_id 가진 .print_sentence 제거
            if (sentence_id_arr[key] == sentence_id_arr[key + 1]) {

                var remove_print_sentence = $(".print_sentence[sentence_id='" + val.sentence_id + "']");

                // remove_print_sentence.next(".print_sentence").remove();

                // remove_print_sentence.css("border", "2px solid red");
                // remove_print_sentence.remove();

            }
        });

    }).fail(function (request, status, error) {
        console.log("code: " + request.status + "\n" + "msg: " + request.responseText + "\n" + "error: " + error);
    });

}

// form_textarea에 span 뿌리기
var append_span_func = function (append_parent, data) {
    var form_textarea = append_parent;
    var k_index;

    $.each(data, function (k, val) {
        k_index = k;
        var split_sentence = val.split("");
        split_sentence.unshift(" ");

        for (var i = 0; i < split_sentence.length; i++) {
            split_sentence[i] = split_sentence[i] == ' ' ? "&nbsp;" : split_sentence[i];
            form_textarea.append("<span sentence_id='" + k + "' sentence_index='" + i + "'>" + split_sentence[i] + "</span>");
        }
    });

    var form_text_span_leng = form_textarea.find("span").length;
    var form_text_row = Math.floor(form_textarea.width() / 30);
    // console.log(form_text_span_leng%form_text_row);
    if (form_text_span_leng % form_text_row != 0) {
        var remain_span = form_text_row - (form_text_span_leng % form_text_row);

        for (var e = 0; e < remain_span; e++) {
            form_textarea.append("<span sentence_id='" + (k_index + 1) + "'> </span>");
        }
    }
}

// 틀린 위치 addClass = "red_font"
Helper.prototype.wrong_str_addClass = function (key, elem, num, is_wrong_end) {

    var start_num;
    var end_num;

    if (is_wrong_end) {
        start_num = key.wrong_start;
        end_num = key.wrong_end;
    } else {
        start_num = parseInt(key.revise_start);
        end_num = key.revise_end;
    }

    for (var j = parseInt(start_num) + 1; j < parseInt(end_num) + 1; j++) {
        // console.log(i);
        elem.eq(j).addClass("red_font");
        if (j == parseInt(end_num)) {
            if (elem.eq(j).has("div")) {

                elem.eq(j).find("div").remove();
            }
            elem.eq(j).append("<div data-name='-" + num + "'></div>");
        }
    }
}
// topik mail send
Helper.prototype.send_mail = function (msg) {

    var formData = new FormData($(msg)[0]);

    $.ajax({
        url: "/api/v1/topik/send/mail",
        type: "post",
        dataType: "json",
        contentType: false,
        processData: false,
        data: formData
    }).done(function (data) {

        alert("전송 성공");
        // console.log(data);
    }).fail(function (err) {

        console.log(err)
    });
}

// print event
function openPrintDialogue() {
    /*
    $('<iframe>', {
      name: 'myiframe',
      class: 'printFrame'
    })
    .appendTo('body')
    .contents().find('body')
    .append(`
      <h1>Our Amazing Offer</h1>
      <img src='coupon.png' />
    `);
  
    window.frames['myiframe'].focus();
    window.frames['myiframe'].print();
  
    setTimeout(() => { $(".printFrame").remove(); }, 1000);
    */

};