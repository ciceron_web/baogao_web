var currentPage = 0;
var lastSelected = -1;
var requestList = [];
var loadingNow = false;
var currentSelectedRequestId = -1;

$(document).ready(function () {
	$("body").on("click", ".ar-view-detail-section-file", function (e) {
		$(".ar-view-detail-section-file").removeClass("selected");
		$(this).addClass("selected");
		paintFilePreview($(this).attr("ar-file"));
	});

	$("body").on("click", ".ar-logo", function (e) {
		if ($(".ar-list").hasClass("selected")) {
			$(".ar-list").removeClass("selected");
		}
		else {
			$(".ar-list").addClass("selected");
		}
	});

	$("body").on("click", ".ar-view-detail-dialog-close", function (e) {
		$(".ar-view-detail-dialog").removeClass("visible");
	});

	$("body").on("click", ".ar-view-detail-dialog", function(e){
		if($(e.target).hasClass("ar-view-detail-dialog")){
			$(".ar-view-detail-dialog-close").click();
		}
	});

	$("body").on("click", ".ar-view-detail-section-file-menu-preview", function (e) {
		e.preventDefault();
		// alert($(this).parents(".ar-view-detail-section-file").attr("ar-file"));
		paintFilePreview($(this).parents(".ar-view-detail-section-file").attr("ar-file"), true);
	});

	$("body").on("click", ".ar-view-detail-section-file-menu-download", function (e) {
	});

	$("body").on("click", ".ar-list-item", function () {
		$(".ar-list-item").removeClass("selected");
		$(this).addClass("selected");
		$(".ar-list").removeClass("selected");

		lastSelected = $(this).attr("ar-id");
		currentSelectedRequestId = $(this).attr("ar-id");
		paintFilePreview($(this).attr("ar-file"));

		$(".ar-view-detail-section").remove();
		$(".ar-view-detail").hide();
		$(".ar-view-detail-setting").removeClass("active");
		$.ajax({
			url: "/api/v1/admin/getRequest?id=" + $(this).attr("ar-id"),
			type: "GET",
			dataType: "JSON",
			contentType: false,
			processData: false
		}).done(function (data) {
			// $(".ar-view-detail").append(
			// 	'<div class="ar-view-detail-user">'
			// 	+ '<div class="ar-view-detail-user-school userSchool"><img src="/img/11.png" /></div><br />'
			// 	+ '<span class="userName"></span><br />'
			// 	+ '<span class="userEmail"></span>'
			// 	+ '</div>'
			// );
			$(".ar-view-detail").show();
			$($(".ar-view-detail-setting")[parseInt(data.status)]).addClass("active");

			$(".userName").text(data.name);
			$(".userEmail").text(data.email);
			data.payment && $(".paymentAmount").text(data.payment.amount + "원");
			$(".statusText").text(statusText[data.status]);
			$(".statusText").css("background-color", statusColor[data.status]);
			$(".userSchool").css("background-image", "url('/img/school_logo/" + data.school_url + ".png')");



			$(".ar-view-detail").append(
				'<div class="ar-view-detail-section">'
				+ '<span class="ar-view-detail-section-subject" style="">' + data.subject + '</span>'
				+ '</div>'
			);

			paintFile(data.file, data.time);
			$(".ar-view-detail-section-file").addClass("selected");

			var d = new Date(data.time);
			// d.setHours(d.getHours() + 9);
			// $(".ar-view-detail").append(
			// 	'<div class="ar-view-detail-section">'
			// 	+ '<span class="ar-view-detail-section-subject" style="">' + data.subject + '</span>'
			// 	+ '<div class="ar-view-detail-section-file selected" ar-file="' + data.file + '">'
			// 	+ '<img src="/img/mime/file.png">[한국어]'
			// 	+ data.file.split("/").pop()
			// 	+ '</div><span class="ar-view-detail-section-time">' + prettyDate(d) + '</span>'
			// 	+ '</div>'
			// );

			if (data.original.substr(-1) != '/') {
				paintFile(data.original, data.time);

				// $(".ar-view-detail").append(
				// 	'<div class="ar-view-detail-section">'
				// 	+ '<div class="ar-view-detail-section-file" ar-file="' + data.original + '">'
				// 	+ '<img src="/img/mime/file.png">[원문]'
				// 	+ data.original.split("/").pop()
				// 	+ '</div><span class="ar-view-detail-section-time">' + prettyDate(d) + '</span>'
				// 	+ '</div>'
				// 	+ '<br />'
				// 	+ '<br />'
				// 	+ '<hr />'
				// 	+ '<br />'
				// 	+ '<br />'
				// );
			}

			$(".ar-view-detail").append(
				'<div class="ar-view-detail-section">'
				+ '<span class="ar-view-detail-section-subject" style=" color: #7b7c80;">' + data.name + '</span>'
				+ '<div class="ar-view-detail-section-text">'
				+ data.context
				+ '</div><span class="ar-view-detail-section-time">' + prettyDate(d) + '</span>'
				+ '</div>'
			);




			$(".ar-view-detail").append(
				'<div class="ar-view-detail-section">'
				+ '<span class="ar-view-detail-section-subject" style=" color: #7b7c80;">' + adminEmail + '</span>'
				+ '<form class="ar-view-detail-section-comment">'
				+ '<textarea name="content" placeholder="코멘트 입력"></textarea>'
				+ '<input name="file" type="file" style="width: 80%;" /><button type="submit" style="width: 20%;">제출</button>'
				+ '<input name="request_id" type="hidden" value="' + currentSelectedRequestId + '"/>'
				+ '</form>'
				+ '</div>'
			);


			$.each(data.comment, function (index, item2) {
				paintComment(item2.id, item2.admin_email, item2.content, item2.time, item2.file);

			});

		}).fail(function (err) {
			console.log(err);
		});

	});

	setInterval(function () {
		if ($(".ar-list").prop("scrollHeight") <= $(".ar-list").height() + 100) {
			getRequestList();
		}
	}, 500);

	$(".ar-list").scroll(function () {
		console.log($(this).scrollTop());
		console.log($(this).prop("scrollHeight"));
		console.log($(this).height());
		console.log($(this).prop("scrollHeight") - $(this).height());
		if ($(this).scrollTop() >= $(this).prop("scrollHeight") - $(this).height() - 150) {
			getRequestList();
		}
	});

	$("body").on("submit", ".ar-view-detail-section-comment", function (e) {
		e.preventDefault();
		var formData = new FormData($(this)[0]);

		$.ajax({
			url: "/api/v1/admin/writeComment",
			type: "POST",
			dataType: "JSON",
			data: formData,
			contentType: false,
			processData: false
		}).done(function (data) {
			paintComment(data.id, data.admin_email, data.content, data.time, data.file);
		}).fail(function (err) {
			console.log(err);
		});
	});

	$("body").on("submit", ".ar-view-detail-dialog form", function(e){
		e.preventDefault();
		var formData = new FormData($(this)[0]);

		$.ajax({
			url: $(this).attr("action"),
			type: $(this).attr("method"),
			dataType: "JSON",
			data: formData,
			contentType: false,
			processData: false
		}).done(function (data) {
			$(".ar-list-item[ar-id=" + currentSelectedRequestId + "]").click();
			// paintComment(data.id, data.admin_email, data.content, data.time, data.file);
		}).fail(function (err) {
			console.log(err);
		});

	});

	getRequestList();
});

var statusColor = ["#f57a82", "#f5ec7a", "#7ac0f5", "#8e688f", "#7b7c80"];
var statusText = ["의뢰완료", "가격지정됨", "결제완료됨", "담당자지정됨", "교열완료"];

var prettyDate = function (time) {
	var date = new Date((time || "")),
		diff = (((new Date()).getTime() - date.getTime()) / 1000),
		day_diff = Math.floor(diff / 86400);
	// if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
	if (isNaN(day_diff))
		return;
	var v = day_diff <= 0 && (
		diff < 60 && "just now" ||
		diff < 120 && "1분 전" ||
		diff < 3600 && Math.floor(diff / 60) + "분 전" ||
		diff < 7200 && "한시간 전" ||
		diff < 86400 && Math.floor(diff / 3600) + "시간 전") ||
		day_diff == 1 && "어제" ||
		// day_diff < 7 && day_diff + " days ago" ||
		// day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago" ||
		day_diff > 1 && date.toLocaleDateString("ko-kr");
	if (!v)
		window.console && console.log(time);
	return v ? v : '';
}

var getRequestList = function () {
	if (!loadingNow) {
		loadingNow = true;
		$.ajax({
			url: "/api/v1/admin/getRequestList?page=" + ++currentPage,
			type: "GET",
			dataType: "JSON",
			contentType: false,
			processData: false
		}).done(function (data) {
			$.each(data.data, function (index, item) {
				requestList.push(item);
			});
			requestList.sort(function (a, b) {
				return b.id - a.id;
			});
			paintRequestList();
		}).fail(function (err) {
			console.log(err);
		}).always(function () {
			loadingNow = false;
		});

	}
}

var paintRequestList = function () {
	$(".ar-list").empty();
	$.each(requestList, function (index, item1) {
		var d = new Date(item1.time);
		// d.setHours(d.getHours() + 9);
		if ($(".ar-list .ar-list-item[ar-id='" + item1.id + "']").length < 1) {
			$(".ar-list").append(
				'<div class="ar-list-item" ar-id="' + item1.id + '" ar-status="' + item1.status + '" ar-file="' + item1.file + '" ar-name="' + item1.name + '" ar-email="' + item1.email + '" ar-school="' + item1.school_url + '">'
				+ '<div class="ar-list-item-status" style="background-color:' + statusColor[item1.status] + '"></div>'
				+ '<div class="ar-list-item-detail">'
				+ '<span class="ar-list-item-detail-text">' + item1.id + '. ' + item1.name + '</span>'
				+ '<span class="ar-list-item-detail-text">' + item1.file.split("/").pop() + '</span>'
				+ '</div>'
				+ '<div class="ar-list-item-num"></div>'
				+ '</div>'
			);
		}
	});
	$(".ar-list .ar-list-item[ar-id='" + lastSelected + "']").addClass("selected");
}

var paintFilePreview = function (filePath, isNewTab) {
	$(".ar-view-doc").empty();

	var officeMime = ["doc", "docx", "ppt", "pptx", "xls", "xlsx"];
	var imageMime = ["png", "jpg", "jpeg", "gif", "tiff", "bmp"];
	var textMime = ["txt", "html"];
	var zipMime = ["zip", "rar", "7z"];
	var url = "";



	if (officeMime.indexOf(filePath.split(".").pop()) >= 0) {
		url = "https://view.officeapps.live.com/op/embed.aspx?src=baogao%2Eco" + encodeURI(filePath);
	}
	else if (imageMime.indexOf(filePath.split(".").pop()) >= 0) {
		url = filePath;
	}
	else if (textMime.indexOf(filePath.split(".").pop()) >= 0) {
		url = filePath;
	}
	else {
		$(".ar-view-doc").html("미리보기 할 수 없는 파일입니다.");
	}

	if ($(window).width() > 640) {
		$(".ar-view-doc").append('<iframe src="" style="width: 100%; height: 100%; overflow: hidden;" frameborder="0"></iframe>');
		$(".ar-view-doc iframe").attr("src", url);
	}

	if (isNewTab) window.open(url);

}

var paintComment = function (id, writer, content, time, file) {
	var d2 = new Date(time);
	// console.log(d2);
	// d2.setHours(d2.getHours() + 9);
	$(".ar-view-detail").append(
		'<div class="ar-view-detail-section" id="comment_' + id + '">'
		+ '<span class="ar-view-detail-section-subject" style=" color: #7b7c80;">' + writer + '</span>'
		+ '<div class="ar-view-detail-section-text">'
		+ content
		+ '</div><span class="ar-view-detail-section-time">' + prettyDate(d2) + '</span>'
		+ (writer == adminEmail ? ('<div class="ar-view-detail-section-management">'
			+ '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'
			+ '<i class="fa fa-trash-o" aria-hidden="true" onclick="deleteComment(' + id + ');"></i>'
			+ '</div>') : "")
		+ '</div>'
	);
	if (file) {
		paintFile(file, null);
	}


	$(".ar-view-detail-section-comment").parent(".ar-view-detail-section").remove();
	$(".ar-view-detail").append(
		'<div class="ar-view-detail-section">'
		+ '<span class="ar-view-detail-section-subject" style=" color: #7b7c80;">' + adminEmail + '</span>'
		+ '<form class="ar-view-detail-section-comment">'
		+ '<textarea name="content" placeholder="코멘트 입력"></textarea>'
		+ '<input name="file" type="file" style="width: 80%;" /><button type="submit" style="width: 20%;">제출</button>'
		+ '<input name="request_id" type="hidden" value="' + currentSelectedRequestId + '"/>'
		+ '</form>'
		+ '</div>'
	);

}

var paintFile = function (file, time) {
	var d2 = null;

	if (time) {
		d2 = new Date(time);
		// d2.setHours(d2.getHours() + 9);
	}

	$(".ar-view-detail").append(
		'<div class="ar-view-detail-section">'
		+ '<div class="ar-view-detail-section-file" ar-file="' + file + '" title="' + file.split("/").pop() + '">'
		+ '<img src="/img/mime/file.png">'
		+ file.split("/").pop()
		+ '<div class="ar-view-detail-section-file-menu">'
		+ '<a class="ar-view-detail-section-file-menu-button ar-view-detail-section-file-menu-download" href="' + file + '" download>'
		+ '<i class="fa fa-download" aria-hidden="true"></i>'
		+ '</a>'
		+ '<div class="ar-view-detail-section-file-menu-button ar-view-detail-section-file-menu-preview">'
		+ '<i class="fa fa-television" aria-hidden="true"></i>'
		+ '</div>'
		+ '</div>'
		+ '</div><span class="ar-view-detail-section-time">' + prettyDate(d2) + '</span>'
		+ '</div>'
	);
}

var deleteComment = function (id) {

	var formData = new FormData();
	formData.append("request_id", id);

	$.ajax({
		url: "/api/v1/admin/deleteComment",
		type: "POST",
		dataType: "JSON",
		data: formData,
		contentType: false,
		processData: false
	}).done(function (data) {
		$("#comment_" + id).remove();
		// paintComment(data.admin_email, data.content, data.time, data.file);
	}).fail(function (err) {
		console.log(err);
	})
}

var showModal = function (name) {
	$(".ar-view-detail-dialog[value=" + name + "] form")[0].reset();
	$(".ar-view-detail-dialog[value=" + name + "] form input[name=request_id]").val(currentSelectedRequestId);
	$(".ar-view-detail-dialog[value=" + name + "]").addClass("visible");
}