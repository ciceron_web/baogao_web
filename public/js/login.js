$(document).ready(function () {
    var email, password, hash_pass, identifier;

    $("body").on("click", ".intro-logo img", function () {
        location.href = "/";
    });

    $("body").on("click", ".join_btn", function () {
        location.href = "/signup";
    });

    $("body").on("click",".search_id", function(){
        location.href = "/find_login_info";
    });

    $("body").on("submit", ".frmLogin", function (e) {
        e.preventDefault();

        email = $("input[name='email']").val();
        password = $.sha256($("input[name='password']").val()) ;

        var formData = new FormData();
        formData.append("email", email);
        formData.append("password", password);


        $.ajax({
            url: "/api2/v2/login",
            type: "POST",
            dataType: "JSON",
            contentType: false,
            processData: false,
            data: formData
        }).done(function (data) {
            console.log(data);
            location.href = "/";
        }).fail(function (err) {
            console.log("실패");
            alert("아이디 혹은 비밀번호를 확인해주세요.");
        });

    });

    $("body").on("click",".fb_btn", function(e){
        e.preventDefault();

        $.ajax({
            url:"/api2/v2/fbLogin",
            type:"get",
            contentType:false,
            processData:false
        }).done(function(data){
            console.log(data);
        }).fail(function(err){
            console.log("실패");
        })
    });

});

