$(document).ready(function () {
	$("body").on("click", "#account-logout", function () {
		$.ajax({
			url: "/api2/v2/logout",
			type: "GET",
			dataType: "text",
			contentType: false,
			processData: false
		}).done(function (data) {
			location.href = "/";
		}).fail(function (err) {

		});
	});

	$("body").on("click", ".fa-times", function (e) {
		e.preventDefault();
		$(".popup").remove();
	});

	$("body").on("keydown", function (e) {
		if (e.keyCode == 27) {
			$(".fa-times").click();
		}
	});

	var checkAuth = function () {
		if (!isAuthInProgress) {
			return;
		}
		if ($(".popup#popup_authStart").length < 1) {
			console.log("ff");
			return;
		}

		$.ajax({
			url: '/api/user/auth/check?token=' + token,
			processData: false,
			contentType: false,
			dataType: "JSON",
			type: 'GET',
			success: function (data) {
				if (data.isAuthenticated == 1) {
					location.reload();
				}
				else {
					setTimeout(function () {
						checkAuth();
					}, 2000);
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				setTimeout(function () {
					checkAuth();
				}, 2000);
			}
		});
	}


	function checkPassword() {
		password = $("#new_password1").val();
		var checkNumber = password.search(/[0-9]/g);
		var checkEnglish = password.search(/[a-z]/ig);

		if (!/^[a-zA-Z0-9]{4,16}$/.test(password)) {
			alert('숫자와 영문자 조합으로 4~16자리를 사용해야 합니다.');
			return false;
		}
		if (checkNumber < 0 || checkEnglish < 0) {
			alert("숫자와 영문자를 혼용하여야 합니다.");
			return false;
		}
		if (/(\w)\1\1\1/.test(password)) {
			alert('같은 문자를 4번 이상 사용하실 수 없습니다.');
			return false;
		}
		if($("#new_password1").val() != $("#new_password2").val()){
			alert("두 비밀번호가 일치하지 않습니다.");
			return false;
		}
		return true;
	}

	$("body").on("click", "#changePassword", function () {
		popup("changePassword", function () {
			$(".mypage-password").submit(function (e) {
				e.preventDefault();
				///api2/v2/changePassword
				var formData = new FormData();

				if (checkPassword()) {
					formData.append("email", userInfomation.email);
					formData.append("old_password", $.sha256($("#old_password").val()));
					formData.append("new_password", $.sha256($("#new_password1").val()));
					$.ajax({
						url: '/api2/v2/changePassword',
						data: formData,
						processData: false,
						contentType: false,
						dataType: "TEXT",
						type: 'POST',
						success: function (data2) {
							$(".step-now").addClass("step-left").removeClass("step-now");
							$($(".step-right").first()).addClass("step-now").removeClass("step-right");
							// alert("입력하신 학교 이메일로 인증 링크를 발송하였습니다. 인증 완료 후 교열이 시작됩니다.");
						},
						error: function (xhr, ajaxOptions, thrownError) {
							alert("알 수 없는 에러가 발생하였습니다.");
						}
					});
				}

			});
		});
	});

	$("body").on("click", "#account-auth", function () {
		popup("authStart", function () {
			isAuthInProgress = false;
			$(".auth-school_id").val(userInfomation.school_id);
			$(".auth-school_email").attr("placeholder", userInfomation.email);
			$(".auth-user_email").val(userInfomation.email);
			$(".popup-button").css("background-color", userInfomation.school_color);
			$(".popup-title").css("background-color", userInfomation.school_color);


			$(".form-auth").submit(function (e) {
				e.preventDefault();
				$(".auth-user_email").val(userInfomation.email);
				$(".auth-user_email").attr("disabled", false);
				var formData = new FormData($(this)[0]);
				$(".auth-user_email").attr("disabled", true);

				$.ajax({
					url: '/api/user/auth/start',
					data: formData,
					processData: false,
					contentType: false,
					dataType: "JSON",
					type: 'POST',
					success: function (data2) {
						token = data2.token;

						$(".step-now").addClass("step-left").removeClass("step-now");
						$($(".step-right").first()).addClass("step-now").removeClass("step-right");
						isAuthInProgress = true;

						$("#popup_authStart popup-button popup-prev").click(function () {
							isAuthInProgress = false;
						});

						checkAuth();
						// alert("입력하신 학교 이메일로 인증 링크를 발송하였습니다. 인증 완료 후 교열이 시작됩니다.");
					},
					error: function (xhr, ajaxOptions, thrownError) {
						if (xhr.status == 401) {
							alert("이메일이 잘못되었습니다.");
							// alert("학교에서 발급받은 이메일만 사용할 수 있습니다.");
						}
					}
				});
			})
		});
	});
});