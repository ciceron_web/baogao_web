$(document).ready(function () {
    var cheer_texts = [
        "不吃饭则饥，不读书则愚。",
        // 밥을 안 먹으면 배고프다. 책을 안 읽으면 어리석다.
        "用宝珠打扮自己，不如用知识充实自己。",
        // 보석으로 꾸미는 것 보다 지식으로 자기개발하는 것이 더 낫다.
        "知识是智慧的火炬。",
        // 지식은 지혜의 횃불이다.
        "星星使天空绚烂夺目, 知识使人增长才干。",
        // 별은 하늘을 반짝반짝하게 해주며 지식은 사람을 반짝반짝하게 해준다.
        "世界上三种东西最宝贵:知识、粮食和友谊。",
        // 세상에서 제일 진귀한 3 가지 : 지식, 식량, 우정
        "人贵有志，学贵有恒。",
        // 향상심이 있는 사람이 귀중하며 끈기있는 학습이 귀중하다
        "书读百遍，其义自见。",
        // 책을 백번 읽으면 그 의미를 알아볼 수 있다.
        "知识就是力量。",
        // 지식은 힘이다.
        "学而时习之，不亦说乎？",
        // 학습한 후 항상 복습하면 얼마나 기쁘겠어요?
        "求学的三个条件是：多观察、多吃苦、多研究。",
        // 학습의 3가지 조건 : 관찰을 많이 하고, 고생을 많이 하고, 연구를 많이 하는 것이다. 
        "学习从来无捷径，循序渐进登高峰。",
        // 학습은 지름길이 없다. 점차적으로 정상에 올라가야 된다.
        "经常不断地学习，你就什么都知道。你知道得越多，你就越有力量。",
        // 끊임 없이 자주 공부할 수록 많이 알 수 있다. 많이 알게 될수록 힘이 세질 것이다. 
        "学习要有三心，一信心，二决心，三恒心。",
        // 학습에 있어서, 3가지 마음이 있어야 한다. 신심, 결심, 항심 
        "读一书，增一智。",
        // 책 한 권 읽으면 지혜 한 층 증가
        "百尺竿头，更进一步。",
        // 이미 도달한 탁월한 경지에 만족하지 않고 더욱더 노력한다.
        "书山有路勤为径，学海无涯古作舟。",
        // 서적의 산은 길이 있고 근로로 도로를 만들어야 한다. 학습의 해양은 안변이 없고 고생을 배로 타야 한다.
        "温故而知新。",
        // 배운 것을 다시 복습하면 새로운 것을 알게 될 것이다. 
        "莫等闲，白了少年头，空悲切。",
        // 청춘을 낭비하지 마오. 소년의 머리가 희어지면 공허하기만 하다오. 
        "勿谓寸阴短，既过难再获。",
        // 세월이 짧다. 지나가면 다시 돌아가지 않는다.
        "风声雨声读书声声声入耳，家事国事天下事事事关心。",
        // 풍성우성독서성 소리가 귀에 들어온다. 가사국사찬하사 일일이 다 관심을 가져야 한다. 
        "虚心万事能成，自满十事九空。",
        // 겸손만사성, 자만십사구공
        "学如逆水行舟，不进则退。",
        // 학습은 역류에서 배를 타듯이 진보하지 못하면 뒤돌아질 것이다. 
        "重复是学习之母。",
        // 중복은 학습의 어머님이다. 
        "游手好闲的学习并不比学习游手好闲。"
        // 빈둥거리면서 공부하는 것이 공부하는 것 보다 더 빈둥거린다. 
    ]
    var cheer_text = shuffle(cheer_texts);
    function random_text() {
        var i = (Math.random() * cheer_text.length) | 0;
        $(".cheer_text p").text(cheer_text[i]);
    }
    var $quiz_content = $("#quiz_main .content");
    var $quiz_contentY = $quiz_content.offset().top;
    function quiz_position() {
        $("html,body").animate({ scrollTop: $quiz_contentY + 90 }, 10);
    }
    // quiz_position();

    /*******************************************
     
        팝업창
    
    ********************************************/

    // alert 창 닫기
    /*$("body").on("click", ".fa-times-circle", function () {
        $(".alertwrap").css("display", "none");
    });*/


    // 퀴즈 시작
    $("body").on("click", "#startBtn", function () {
        $.ajax({
            url: "/api2/v2/quiz/start",
            type: "get",
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $start_txt = "<h2>퀴즈를<br />시작하지.</h2>";
                $('.start_text h1').fadeOut(100, function () {
                    $(".start_text").html($start_txt).fadeIn(100);
                });
            },
            contentType: false,
            processData: false
        }).done(function (data) {
            quiz_position();
            $(".grid").load("quiz/questions", function () {
                $(".content").css({ "background": "#eaeaea", "min-height": "auto" });
                questions();
            });
            return false;
        }).fail(function (err) {
            console.log(err);
        });
    });

    // 퀴즈 받아오기.
    var Qnum = 0;
    var correctNum = 0;
    var correct_sentence = "";
    var wrong_sentence = "";

    function questions() {
        Qnum++;
        $(".alertwrap").css("display", "none");
        $.ajax({
            url: "/api2/v2/quiz/" + Qnum,
            type: "get",
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $(".loading").css("display", "block");
            },
            contentType: false,
            processData: false
        }).done(function (data) {
            quiz_position();
            $(".loading").css("display", "none");
            // console.log(data.data);
            random_text();
            $(".cheer_text p").show();
            $("#question").empty();
            $("#question").html(data.data.quiz).attr("value", data.data.id);
            $(".buttons button").removeClass("active");
            $(".submit_btn button").removeClass("active");
            $(".short-answer").css("display", "none");

            if (Qnum < 4) {
                var $buttons = $("div.buttons");
                $buttons.html(shuffle($buttons.children().get()));
                var choice0 = $("#choice0").html(data.data.correct_answer).attr("value", "correct_answer");
                var choice1 = $("#choice1").html(data.data.wrong_answer).attr("value", "wrong_answer");
                $buttons.find("button").on("click", function () {
                    $(this).siblings("button").removeClass("active");
                    $(this).toggleClass("active");
                    $(".submit_btn button").removeClass("active");
                    if ($buttons.find(".active").length == 1) {
                        $(".submit_btn button").addClass("active");
                    }
                });
            } else {
                // 문장 카드 뿌리기                
                $(".short-answer").css({ "display": "block", "border": "4px solid #00824c" });
                $(".buttons").empty();
                $(".short-answer").empty();
                var $arrow = "<i class='fa fa-long-arrow-left' aria-hidden='true'></i>"
                var $buttons = $("div.buttons");
                $(".short-answer").append($arrow);
                correct_sentence = '';
                for (i = 0; i < data.data.correct_answer.length; i++) {
                    correct_sentence += data.data.correct_answer[i] + " ";
                }
                wrong_sentence = '';
                for (i = 0; i < data.data.wrong_answer.length; i++) {
                    wrong_sentence += data.data.wrong_answer[i] + " ";
                }
                $.each(data.data.correct_answer, function (i1, val1) {
                    // console.log(i +" : "+ val);
                    var spans1 = "<span id='choice0_" + i1 + "' value='" + i1 + "'>" + val1 + "</span>";
                    $buttons.append(spans1);
                });
                $.each(data.data.wrong_answer, function (i2, val2) {
                    var spans2 = "<span id='choice1_" + i2 + "' value='wrong_answer'>" + val2 + "</span>";
                    $buttons.append(spans2);
                });
                $buttons.html(shuffle($buttons.children().get()));
            }
            progress();
        }).fail(function (err) {
            console.log(err);
            if (err.status == 500) {
                // var para = location.href.split("?");
                // console.log(para);
                location.replace();
            }
        });

    }

    // 정답, 오답 섞기
    function shuffle(o) {
        for (var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    };

    // 퀴즈 끝
    function quizend() {
        $.ajax({
            url: "/api2/v2/quiz/end",
            type: "get",
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $(".loading").css("display", "block");
            },
            contentType: false,
            processData: false
        }).done(function (data) {
            $(".loading").css("display", "none");
            $question = $("#question");
            $correct = Qnum + "个问题中对了" + correctNum + "个!"
            $question.html($correct).css("color", "#ff2020");
            $question.css("padding-bottom", "0");
            $(".content").css({ "padding-top": "50px", "padding-bottom": "70px" });
            $(".short-answer").remove();
            $(".progress_bar").remove();
            $(".cheer_text").remove();
            $(".buttons").empty();
            $email = "<div class='cheer_text'><p class='coupon_text'>将您的邮箱告诉我们，我们会为您发送 baogao 的优惠券哦</p></div><form class='coupon_issue' onsubmit='return false'><input type='email' class='mail_address' name='email' placeholder='baogao@ciceron.me' required/><input type='submit' class='mailsend' value='send' /></form>";
            // 메일을 적어주시면 바오가오 쿠폰을 드려요.
            $(".buttons").append($email);
            $finish = "";
            if (correctNum == 0) {
                $finish += "讲真，你真的很需要 baogao！";
                // 당신은 바오가오가 반드시 필요하군요!
            } else if (correctNum == 1) {
                $finish += "恩，没错就这样，继续保持~";
                // 음, 이렇게 지속하세요~
            } else if (correctNum == 2) {
                $finish += "第二个也对啦！继续继续！你可以的！";
                // 2번째 거도 맞췄네요! go on! go on! you can do it!
            } else if (correctNum == 3) {
                $finish += "不错不错，你已经比一半的人还要优秀啦！";
                // not bad~ 반 이상의 사람보다 우수합니다!
            } else if (correctNum == 4) {
                $finish += "对了四个，离天下第一就差一步啦！";
                // 4개나 맞췄네요, 천하제일까지 한 걸음만 남았어요!
            } else {
                $finish += "天呐！韩语真的好棒~！使用 baogao 话会变得更完美呦！";
                // 와우!  한국어 잘하네요~! 바오가오를 쓰면 더 완벽해지겠어요!
            }
            $question.append("<p>" + $finish + "</p>");
            return false;
        }).fail(function (err) {
            console.log(err);
        });
    }

    // 다음 문제 불러오기.
    $("body").on("click", ".next_btn button", function () {
        quiz_position();
        $(".progress_bar").show();
        $(".fix_window").children().show();
        if (Qnum < 5) {
            questions();
            $(".next_btn").addClass("submit_btn");
            $(".next_btn").find("button").html("submit");
            $(".submit_btn").removeClass("next_btn");
        } else {
            quizend();
            $(".alertwrap").remove();
            $(".next_btn").remove();
        }
    });

    // 4~5번 문제 enter event
    $("body").on("keydown", function (e) {
        var keycode = e.keyCode;
        if (Qnum <= 5) {
            if (keycode == 13) {
                $(".submit_btn button").click();
            }
            if (keycode == 8) {
                $(".short-answer .fa").click();
            }
        }
    });

    // 주관식 답
    $("body").on("click", ".buttons span", function () {
        var answerText = $(".short-answer");
        var span = $(this).text();
        var spanId = $(this).attr("id");
        answerText.append("<span value='" + spanId + "'>" + span + " </span>");
        $(this).css("visibility", "hidden");
        if (answerText.find("span").length >= 1) {
            $(".submit_btn button").addClass("active");
        }
        /*else {
            $(".submit_btn button").removeClass("active");
        }*/
    });

    $("body").on("click", ".short-answer .fa", function () {
        var last_span = $(".short-answer").children("span").last();
        // console.log($(".short-answer").children().last("span").text());
        $("#" + last_span.attr('value')).css("visibility", "visible");
        last_span.remove();
        if ($(".short-answer").find("span").length == 0) {
            $(".submit_btn button").removeClass("active");
        }
    });

    // 퀴즈 정, 오답
    var result = "";
    $("body").on("click", ".submit_btn button", function () {
        // $(".errorPop").css("display", "none");
        var sentence_answer = $(".short-answer").text();
        // 객관식
        if (Qnum <= 3) {
            if ($(".active").text() == '') {
                alert("请答题。")
            } else {
                $(".cheer_text p").hide();
                $(".progress_bar").hide();
                $(".fix_window").children().hide();
                $(".submit_btn").addClass("next_btn");
                $(".next_btn").find("button").html("next");
                $(".submit_btn").removeClass("submit_btn");

                var dataId = $("#question").attr("value");
                var choice0 = $("#choice0").text();
                var correct = "<p class='correct'>" + choice0 + "</p>";

                if ($(".active").attr("value") == "correct_answer") {
                    result = "pass";
                } else {
                    result = "fail";
                }
                var data = "result=" + result + "&dataId=" + dataId;

                $.ajax({
                    url: "/api2/v2/quiz/" + Qnum + "/submit?" + data,
                    type: "post",
                    beforeSend: function () {
                        $(".loading").css("display", "block");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    quiz_position();
                    $(".loading").css("display", "none");
                    if (result == "pass") {
                        $(".alertPop h2").html("正确"); // 정답입니다.
                        $(".alertPop h1").html("O");
                        correctNum++;
                    } else {
                        $(".alertPop h2").html("错误"); // 오답입니다.
                        $(".alertPop h1").html("X");
                        $(".alertPop h1").append(correct);
                    };

                    $(".alertwrap").css("display", "block");
                }).fail(function (err) {
                    console.log(err);
                });
            }
        } else {
            if (sentence_answer == "") {
                alert("请答题。");
                // 문제를 풀어주세요.
            } else {
                $(".cheer_text p").hide();
                $(".progress_bar").hide();
                $(".fix_window").children().hide();
                $(".submit_btn").addClass("next_btn");
                $(".next_btn").find("button").html("next");
                $(".submit_btn").removeClass("submit_btn");
                var dataId = $("#question").attr("value");
                // var sentence_answer = $(".short-answer").text();
                // var result = "";
                var correct = "<p class='correct'>" + correct_sentence + "</p>";
                // 정답알림창
                $(".alertwrap").css("display", "block");

                if (correct_sentence.trim() == sentence_answer.trim()) {
                    result = "pass";
                } else {
                    result = "fail";
                }
                var data = "result=" + result + "&dataId=" + dataId;
                $.ajax({
                    url: "/api2/v2/quiz/" + Qnum + "/submit?" + data,
                    type: "post",
                    beforeSend: function () {
                        $(".loading").css("display", "block");
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function (data) {
                    quiz_position();
                    $(".loading").css("display", "none");
                    if (result == "pass") {
                        $(".alertPop h2").html("正确"); //정답입니다.
                        $(".alertPop h1").html("O");
                        correctNum++;
                    } else {
                        $(".alertPop h2").html("错误"); //오답입니다.
                        $(".alertPop h1").html("X");
                        $(".alertPop h1").append(correct);
                    };
                }).fail(function (err) {
                    console.log(err);
                });
            }
        }
    });

    // 주관식 답 클릭 취소
    $("body").on("click", ".short-answer span", function () {
        var spanVal = $(this).attr("value");
        $(this).remove();
        $("#" + spanVal).css("visibility", "visible");
    });

    // 오류 신고 창
    $("body").on("click", ".sendError", function () {
        $(".backbtn").remove();
        $(".quiz_board").hide();
        $(".next_btn").addClass("error_btn");
        $(".next_btn").find("button").html("send").addClass("sendbtn");
        $(".error_btn").removeClass("next_btn");
        $(".errorPop textarea").val("");
        $(".errorPop").css("display", "block");
        $backbtn = "<button class='backbtn'>back</button>"
        $(".error_btn").prepend($backbtn);
    });

    back_event = function () {
        $(".backbtn").remove();
        $(".quiz_board").show();
        $(".sendbtn").removeClass("sendbtn");
        $(".errorPop").css("display", "none");
        $(".error_btn").addClass("next_btn");
        $(".next_btn").find("button").html("next");
        $(".next_btn").removeClass("error_btn");
    }

    $("body").on("click", ".backbtn", function () {
        back_event();
    });

    // 오류 신고
    $("body").on("click", ".sendbtn", function () {
        var quiz = $("#question").text();
        var msg = $(".errorPop textarea").val();
        var formdata = new FormData();

        if (Qnum <= 3) {
            var correct_answer = $("#choice0").text();
            var wrong_answer = $("#choice1").text();
            formdata.append("quiz", quiz);
            formdata.append("correct_answer", correct_answer);
            formdata.append("wrong_answer", wrong_answer);
            formdata.append("msg", msg);
        } else {
            var correct_answer = correct_sentence;
            var wrong_answer = wrong_sentence;
            formdata.append("quiz", quiz);
            formdata.append("correct_answer", correct_answer);
            formdata.append("wrong_answer", wrong_answer);
            formdata.append("msg", msg);
        }

        $.ajax({
            url: "/api2/v2/quiz/sendError",
            type: "post",
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $(".loading").css("display", "block");
            },
            data: formdata,
            contentType: false,
            processData: false
        }).done(function (data) {
            $(".loading").css("display", "none");
            alert("谢谢您宝贵的意见。"); //소중한 의견 감사합니다.
            back_event();
        }).fail(function (err) {
            console.log(err);
        });
    });

    // 쿠폰 발행
    $("body").on("submit", ".coupon_issue", function () {
        var mail_address = $(".mail_address").val();
        var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
        var formdata = new FormData($(this)[0]);

        // formdata.append("user_mail", mail_address);

        if (mail_address == "") {
            alert("请写下您的邮件地址。"); // 메일을 적어주세요.
        } else if (!regExp.test(mail_address)) {
            alert("邮件格式错误。"); // 메일 형식이 잘못되었습니다.
        } else {
            console.log(mail_address);
            $.ajax({
                url: "/api/v1/user/quiz/coupon",
                type: "post",
                cache: false,
                dataType: "json",
                beforeSend: function () {
                    $(".loading").css("display", "block");
                },
                data: formdata,
                contentType: false,
                processData: false
            }).done(function (data) {
                $(".coupon_text").text("已向您发送了优惠券。查看你的电子邮件。");
                $(".coupon_issue *").attr("disabled", "disabled");
                alert("已向您发送了优惠券。");
                // 쿠폰이 발행되었습니다.
            }).fail(function (err) {
                console.log(err);
            }).always(function () {
                $(".loading").css("display", "none");
            });
        }
    });

    // progress bar
    function progress() {
        var progress = $("#progress");
        progress.css("width", Qnum * 20 + "%");
    }
});