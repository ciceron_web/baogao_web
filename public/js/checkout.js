$(document).ready(function () {
	var coupon_code = "";

	$(window).bind("pageshow", function () {
		$("form").clear();
		// update hidden input field
	});


	$(".checkout-coupon-form").submit(function (e) {
		e.preventDefault();
		loading(true);

		var formData = new FormData($(this)[0]);
		$.ajax({
			url: '/api/user/coupon/check',
			data: formData,
			processData: false,
			contentType: false,
			dataType: "JSON",
			type: 'POST'
		}).done(function (data) {
			try {
				$(".checkout-coupon-discounted").text(data.request.discounted);
				$(".checkout-coupon-amount").text(data.request.amount);
				$(".checkout-amount").text(data.request.amount);
				coupon_code = $("input[name=code]").val();
				alert("쿠폰이 적용되었습니다.");
				// $(".checkout-amount").css("text-decoration-line", "line-through");
				// location.href = data.link;
			}
			catch (err) {

				try {
					if (data.code == 4001) {
						alert("이미 사용된 쿠폰입니다.");
					}
					else if(data.code == 4002){
						alert("존재하지 않는 쿠폰입니다.");
					}
				}
				catch (err) {
					alert("오류가 발생하였습니다.");
				}

				$(".checkout-coupon-discounted").text("0");
				$(".checkout-coupon-amount").text(payment_amount);
				$(".checkout-amount").text(payment_amount);
				coupon_code = "";

				// alert("哦! 发生了错误。 请再试一次。");
			}
		}).fail(function (xhr, ajaxOptions, thrownError) {
			alert("오류가 발생하였습니다.");
			$(".checkout-coupon-discounted").text("0");
			$(".checkout-coupon-amount").text(payment_amount);
			$(".checkout-amount").text(payment_amount);
			coupon_code = "";
			// alert("哦! 发生了错误。 请再试一次。");
		}).always(function () {
			loading(false);
		});

	});

	var checkout = function (payBy) {
		if ($("#agree").prop('checked') != true) {
			alert("请接受条款。");
			return;
		}

		loading(true);
		var formData = new FormData();
		formData.append("payBy", payBy);
		formData.append("request_id", request_id);
		formData.append("coupon_code", coupon_code);

		$.ajax({
			url: '/api/user/pay/start',
			data: formData,
			processData: false,
			contentType: false,
			dataType: "JSON",
			type: 'POST'
		}).done(function (data) {
			try {
				location.href = data.link;
			}
			catch (err) {
				alert("哦! 发生了错误。 请再试一次。");
			}
		}).fail(function (xhr, ajaxOptions, thrownError) {
			alert("哦! 发生了错误。 请再试一次。");
		}).always(function () {
			loading(false);
		});

	}

	$("body").on("click", ".checkout-item", function (e) {
		checkout($(this).attr("payment"));
	});


});