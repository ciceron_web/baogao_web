$(document).ready(function () {


	if ($.cookie("pop-day") != "Y") {
		popup("howToUse");
	}

	if ($.cookie("alert-day") == "Y") {
		$(".alertWrap").hide();
	};

	$("body").on("click", ".intro-logo img", function () {
		location.href = "/";
	});

	$("body").on("click", "#gotoSchool", function (e) {
		e.preventDefault();
		var targetOffset = $(".school2").offset().top - 100;
		$('html,body').stop().animate({ scrollTop: targetOffset }, 500);
		$(".school2").effect("highlight", {}, 850);
	});

	$("body").on("click", "a", function (e) {
		if ($(this).attr("href").substr(0, 1) == "#") {
			e.preventDefault();
			var targetOffset = $($(this).attr("href")).offset().top - 100;
			$('html,body').stop().animate({ scrollTop: targetOffset }, 500);
		}
	});

	$("body").on("click", "li.menu2, .gotoSchool", function (e) {
		e.preventDefault();
		$("#gotoSchool").click();
	});

	// alert 창
	$("body").on("click", "#alert-day", function () {
		// if ($("#alert-day").is(":checked")) {
		$.cookie("alert-day", "Y", {
			expires: 1
		});
		$(".alertWrap").css("height", "0%");
		// } else {
		// 	$(this).css("height", "0%");
		// }
	});

	// 팝업 창 닫기
	$("body").on("click", ".popup-button", function (e) {
		e.preventDefault();

		if ($("#pop-day").is(":checked")) {
			// 쿠키값을 "Y"로 하여 하루 저장
			$.cookie("pop-day", "Y", {
				expires: 1
			});
		}
		$(".popup").remove();
	});

	$("body").on("click", ".close", function (e) {
		e.preventDefault();
		$(".popup").remove();
	});

	// about us alert
	/*$("body").on("click", ".section_us a", function(){
		alert("준비 중입니다.");
	});
	
	$("body").on("click", ".section_writer a", function(){
		alert("준비 중입니다.");
	});*/

});

